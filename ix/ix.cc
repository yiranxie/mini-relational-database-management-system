#include "ix.h"
#include <cmath>

unsigned reverse(register unsigned int x) {
	x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
	x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
	x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
	x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
	return ((x >> 16) | (x << 16));

}

// get the most significant N bits of an unsigned value
unsigned getMostSignificantBits(unsigned value, unsigned N) {
	return (reverse(value) << (32 - N)) >> (32 - N);
}

unsigned hashInt(unsigned a) {
	//Designed by Thomas Wang
	unsigned result = a;
	result += ~(result << 15);
	result ^= (result >> 10);
	result += (result << 3);
	result ^= (result >> 6);
	result += ~(result << 11);
	result ^= (result >> 16);
	return result;
}

unsigned hashFloat(float a){
	std::hash<float> hashFloat;
	return hashFloat(a);

	//TODO test new hash function
//	unsigned temp;
//	memcpy(&temp, &a, sizeof(unsigned));
//	return hashInt(temp);
}

unsigned hashString(string & a) {
	std::hash<string> hashString;
	return hashString(a);

	//TODO test new hash function

	//JS (Justin Sobel) Hash Function
//	unsigned charLength = a.size();
//	if (charLength == 0){
//		return 0;
//	}
//	unsigned result = 1315423911;
//	const char * pChar = a.c_str();
//	for (unsigned i=0; i<charLength; ++i){
//		unsigned thisChar = *(pChar + i);
//		result ^= ((result << 5) + thisChar + (result >> 2));
//	}
//	return result;

	//TODO SBox Hash algorithm
	//Get from http://floodyberry.com/noncryptohashzoo/SBox.html
//	unsigned seed = 1313131313;
//	unsigned charLength = a.size();
//	if (charLength == 0) {
//		return 0;
//	}
//	unsigned result = charLength + seed;
//	char * pChar = a.c_str();
//	for ( ; charLength & ~1; charLength -= 2, pChar += 2 ){
//		result = (((result ^ SBoxTable[pChar[0]]) * 3) ^ SBoxTable[pChar[1]]) * 3;
//	}
//	if (charLength & 1){
//		result = ( result ^ SBoxTable[pChar[0]] ) * 3;
//	}
//	result += ( result >> 22 ) ^ ( result << 4 );
//	return result;
}

unsigned hashInt_2(unsigned a){
	unsigned result = a;
	result = (result + 0x7ed55d16) + (result << 12);
	result = (result ^ 0xc761c23c) ^ (result >> 19);
	result = (result + 0x165667b1) + (result << 5);
	result = (result + 0xd3a2646c) ^ (result << 9);
	result = (result + 0xfd7046c5) + (result << 3);
	result = (result ^ 0xb55a4f09) ^ (result >> 16);
	return result;
}

unsigned hashFloat_2(unsigned a){
	unsigned temp;
	memcpy(&temp, &a, sizeof(unsigned));
	return hashInt_2(temp);
}

unsigned hashString_2(string & a){
	unsigned charLength = a.size();
	if (charLength == 0) {
		return 0;
	}
	unsigned result = FNV_OFFSET_32;
	const char * pChar = a.c_str();
	for(unsigned i = 0; i < charLength; ++i){
		result = result ^ (pChar[i]); // xor next byte into the bottom of the result
		result = result * FNV_PRIME_32; // Multiply by prime number found to work well
	}
	return result;;
}


IndexManager* IndexManager::_index_manager = 0;

IndexManager* IndexManager::instance() {
	if (!_index_manager)
		_index_manager = new IndexManager();

	return _index_manager;
}

IndexManager::IndexManager() {
}

IndexManager::~IndexManager() {
}

RC IndexManager::createFile(const string &fileName,
		const unsigned &numberOfPages) {
	string primaryFilename = fileName + "_primary"; // convert string to char *
	string metadataFilename = fileName + "_metadata";
	RC createPrimary = PagedFileManager::instance()->createFile(
			primaryFilename.c_str());
	RC createMetadata = PagedFileManager::instance()->createFile(
			metadataFilename.c_str());
	if (createPrimary | createMetadata) { // if createFile unsuccessful, existing file(s)
		return CREATE_FILE_ERROR;   // createFile error
	}
	IXFileHandle ixfileHandle;
	openFile(fileName, ixfileHandle);
	// append N empty primary pages
	for (unsigned i = 0; i < numberOfPages; i++) {
		char empty[PAGE_SIZE];
		memset(empty, 0, PAGE_SIZE);
		ixfileHandle.primaryHandle.appendPage(empty);
	}
	// initialize metadata page
	char page[PAGE_SIZE];
	memset(page, 0, PAGE_SIZE);
	*((unsigned *) page) = 0; //initialize Level
	*((unsigned *) (page + sizeof(unsigned))) = numberOfPages; //initialize N
	*((unsigned *) (page + sizeof(unsigned) + sizeof(unsigned))) = 0; //initialize Next
	ixfileHandle.Level = 0;
	ixfileHandle.N = numberOfPages;
	ixfileHandle.Next = 0;
	ixfileHandle.metadataHandle.appendPage(page); // write metadata to metadataFile
	closeFile(ixfileHandle);
	return 0;
}

RC IndexManager::destroyFile(const string &fileName) {
	string primaryFilename = fileName + "_primary"; // convert string to char *
	string metadataFilename = fileName + "_metadata";
	RC destroyPrimary = PagedFileManager::instance()->destroyFile(
			primaryFilename.c_str());
	RC destroyMetadata = PagedFileManager::instance()->destroyFile(
			metadataFilename.c_str());
	if (destroyPrimary | destroyMetadata) { // if destroyFile unsuccessful, non-existing file(s)
		return DESTROY_FILE_ERROR; // destroyFile error
	}
	return 0;
}

RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle) {
	if (ixfileHandle.valid) {
		return 12;
	}
	string primaryFilename = fileName + "_primary"; // convert string to char *
	string metadataFilename = fileName + "_metadata";
	RC openPrimary = PagedFileManager::instance()->openFile(
			primaryFilename.c_str(), ixfileHandle.primaryHandle);
	RC openMetadata = PagedFileManager::instance()->openFile(
			metadataFilename.c_str(), ixfileHandle.metadataHandle);
	if (openPrimary | openMetadata) { // if openFile unsuccessful, non-existing file(s)
		ixfileHandle.valid = false;
		return OPEN_FILE_ERROR; // openFile error
	}
	char temp[PAGE_SIZE];
	RC readMetaFirstPage = ixfileHandle.metadataHandle.readPage(0, temp);
	if (readMetaFirstPage) {
		return 12;
	}
	ixfileHandle.Level = *((unsigned *) temp);
	ixfileHandle.N = *((unsigned *) (temp + sizeof(unsigned)));
	ixfileHandle.Next = *((unsigned *) (temp + sizeof(unsigned)
			+ sizeof(unsigned)));
	unsigned reusablePageNumber = *((unsigned *) (temp + sizeof(unsigned) * 3));
	for (unsigned i = 0; i < reusablePageNumber; ++i) {
		unsigned thisPageID =
				*((unsigned *) (temp + sizeof(unsigned) * (4 + i)));
		ixfileHandle.reusableOverflowPages.push_back(thisPageID);
	}
	ixfileHandle.valid = true;
	return 0;
}

RC IndexManager::closeFile(IXFileHandle &ixfileHandle) {
	if (!ixfileHandle.valid) {
		return 16;
	}
	char temp[PAGE_SIZE];
	memset(temp, 0, PAGE_SIZE);
	memcpy(temp, &(ixfileHandle.Level), sizeof(unsigned));
	memcpy(temp + sizeof(unsigned), &(ixfileHandle.N), sizeof(unsigned));
	memcpy(temp + sizeof(unsigned) * 2, &(ixfileHandle.Next), sizeof(unsigned));

	unsigned maxNum = (PAGE_SIZE - 4 * sizeof(unsigned)) / sizeof(unsigned);
	unsigned reuseNum =
			maxNum < ixfileHandle.reusableOverflowPages.size() ?
					maxNum : ixfileHandle.reusableOverflowPages.size();
	memcpy(temp + sizeof(unsigned) * 3, &reuseNum, sizeof(unsigned));
	for (unsigned i = 0; i < reuseNum; ++i) {
		unsigned thisPageID = ixfileHandle.reusableOverflowPages.front();
		memcpy(temp + sizeof(unsigned) * (4 + i), &thisPageID,
				sizeof(unsigned));
		ixfileHandle.reusableOverflowPages.pop_front();
	}
	RC writeMetaFirstPage = ixfileHandle.metadataHandle.writePage(0, temp);
	RC closePrimary = PagedFileManager::instance()->closeFile(
			ixfileHandle.primaryHandle);
	RC closeMetadata = PagedFileManager::instance()->closeFile(
			ixfileHandle.metadataHandle);
	if (writeMetaFirstPage | closePrimary | closeMetadata) // if closeFile unsuccessful, non-existing file(s)
		return CLOSE_FILE_ERROR; // closeFile error
	return 0;
}

/************************************************************
 * Main logic:
 *  1. Check whether RID already exists
 * 	2. Identify the page number:
 *  3. Construct the in-memory extendble hash table
 * 	   3.1 if the in-page bucket has enough space, just insert, and update the bucket header
 * 	   3.2 if the in-page bucket does not has enough space, split the bucket until
 * 	       3.2.1 the bucket has enough space to insert the record
 *         3.2.2 the global depth reaches the limit, there is no enough space to insert this record,
 * 						try to insert this to the next overflow page of current page, and repeat 2
 ************************************************************/
RC IndexManager::insertEntry(IXFileHandle &ixfileHandle,
		const Attribute &attribute, const void *key, const RID &rid) {

	char page[PAGE_SIZE];

	//1. Check whether RID already exists
	IX_ScanIterator scanItor;
	scan(ixfileHandle, attribute, key, key, true, true, scanItor);
	RID tempRID;
	while (scanItor.getNextEntry(tempRID, page) != IX_EOF) {
		if (tempRID.pageNum == rid.pageNum && tempRID.slotNum == rid.slotNum) {
			return INSERT_RID_ALREADY_EXIST;
		}
	}

	//2. Identify the page number:
	unsigned hashValue = hash(attribute, key);

	// read metadata
	unsigned Level = ixfileHandle.Level;
	unsigned N = ixfileHandle.N;
	unsigned Next = ixfileHandle.Next;
	// find out the linear hashing bucket number
	unsigned bucketNum = hashValue % N;
	if (bucketNum < Next) {
		// if the bucket has been split
		bucketNum = hashValue % (N << 1);
	}

	// read primary page according to bucket number
	ixfileHandle.primaryHandle.readPage(bucketNum, page);

	//3. Construct the in-memory extendible hash table
	ExtendibleHash thisEH(page, attribute, true, bucketNum);
	int insertResult = thisEH.insertPair(key, rid);

	if (insertResult == 0) {
		thisEH.writeInDiskFormat(page);
		ixfileHandle.primaryHandle.writePage(bucketNum, page);
		return 0;
	} else if (insertResult == INSERT_INDEX_SIZE_EXCEEDS_LIMITS) {
		//This pair is too large to store
		return INSERT_INDEX_SIZE_EXCEEDS_LIMITS;
	} else if (insertResult == INSERT_PAGE_HAS_NO_ENOUGH_SPACE
			|| insertResult == INSERT_EH_HAS_NO_ENOUGH_SLOT) {
		//This needs to be stored in the overflow page
		unsigned overFlowPageID = thisEH.overflowPageID;
		unsigned validLastOverFlowPageID = overFlowPageID;
		while (overFlowPageID != 0) {
			validLastOverFlowPageID = overFlowPageID;
			ixfileHandle.metadataHandle.readPage(overFlowPageID, page);
			ExtendibleHash tempEH(page, attribute, false, overFlowPageID);
			insertResult = tempEH.insertPair(key, rid);
			if (insertResult == 0) {
				tempEH.writeInDiskFormat(page);
				break;
			}
			overFlowPageID = tempEH.overflowPageID;
		}
		if (insertResult == 0) {
			//Successfully insert the index into overflow page
			ixfileHandle.metadataHandle.writePage(validLastOverFlowPageID,
					page);
			return 0;

		} else if (insertResult == INSERT_PAGE_HAS_NO_ENOUGH_SPACE
				|| insertResult == INSERT_EH_HAS_NO_ENOUGH_SLOT) {
			//No overflowPage, need to add a new one, and split one page in LH
			unsigned newOverflowPageID;
			bool reusePage = true;
			if (ixfileHandle.reusableOverflowPages.size() > 0) {
				newOverflowPageID = ixfileHandle.reusableOverflowPages.front();
				ixfileHandle.reusableOverflowPages.pop_front();
			} else {
				newOverflowPageID =
						ixfileHandle.metadataHandle.getNumberOfPages();
				reusePage = false;
			}

			memcpy(page + PAGE_SIZE - sizeof(unsigned), &newOverflowPageID,
					sizeof(unsigned));
			if (validLastOverFlowPageID == 0) {
				ixfileHandle.primaryHandle.writePage(bucketNum, page);
			} else {
				ixfileHandle.metadataHandle.writePage(validLastOverFlowPageID,
						page);
			}

			memset(page, 0, PAGE_SIZE);
			ExtendibleHash tempEH(page, attribute, false, newOverflowPageID);
			insertResult = tempEH.insertPair(key, rid);
			if (insertResult == 0) {
				tempEH.writeInDiskFormat(page);
				if (reusePage) {
					ixfileHandle.metadataHandle.writePage(newOverflowPageID,
							page);
				} else {
					ixfileHandle.metadataHandle.appendPage(page);
				}

				//split in LH
				unsigned splitBucketNumber = Next;

				ixfileHandle.primaryHandle.readPage(splitBucketNumber, page);

				ExtendibleHash *pOriginHash = new ExtendibleHash(page,
						attribute, true, splitBucketNumber);
				memset(page, 0, PAGE_SIZE);
				ExtendibleHash *pNewHash_1 = new ExtendibleHash(page, attribute,
						true, splitBucketNumber);
				ExtendibleHash *pNewHash_2 = new ExtendibleHash(page, attribute,
						true, splitBucketNumber + N);
				bool primary1_full = false;
				bool primary2_full = false;
				vector<unsigned> pageChain_1;
				vector<unsigned> pageChain_2;

				pageChain_1.push_back(splitBucketNumber);
				pageChain_2.push_back(splitBucketNumber + N);

				char tempChar[PAGE_SIZE];
				std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator itor;
				for (itor = pOriginHash->content.begin();
						itor != pOriginHash->content.end(); ++itor) {
					std::list<ValueRIDPair> & thisList = itor->second;
					while (thisList.size() > 0) {
						ValueRIDPair & thisPair = thisList.front();
						if (attribute.type == TypeInt) {
							memcpy(tempChar, &(thisPair.intValue), sizeof(int));
						} else if (attribute.type == TypeReal) {
							memcpy(tempChar, &(thisPair.realValue),
									sizeof(float));
						} else {
							unsigned varCharLength = thisPair.varChar.size();
							memcpy(tempChar, &(varCharLength),
									sizeof(unsigned));
							memcpy(tempChar + sizeof(unsigned),
									thisPair.varChar.c_str(), varCharLength);
						}
						unsigned thisHashValue = hash(attribute, tempChar);
						unsigned thisBucketNum = thisHashValue % (N << 1);

						//split one page to two pages, we should not have space problem.
						if (thisBucketNum == splitBucketNumber) {
							if (pNewHash_1->insertPair(tempChar, thisPair.rid)
									!= 0) {
								if (!primary1_full) {
									primary1_full = true;
									pNewHash_1->overflowPageID =
											ixfileHandle.metadataHandle.getNumberOfPages();
									if (ixfileHandle.reusableOverflowPages.size()
											> 0) {
										pNewHash_1->overflowPageID =
												ixfileHandle.reusableOverflowPages.front();
										ixfileHandle.reusableOverflowPages.pop_front();
									}else {
										memset(page, 0, PAGE_SIZE);
										ixfileHandle.metadataHandle.appendPage(page);
									}
									pNewHash_1->writeInDiskFormat(page);
									ixfileHandle.primaryHandle.writePage(
											pNewHash_1->pageID, page);
								} else {
									pNewHash_1->overflowPageID =
											ixfileHandle.metadataHandle.getNumberOfPages();
									if (ixfileHandle.reusableOverflowPages.size()
											> 0) {
										pNewHash_1->overflowPageID =
												ixfileHandle.reusableOverflowPages.front();
										ixfileHandle.reusableOverflowPages.pop_front();
									}else {
										memset(page, 0, PAGE_SIZE);
										ixfileHandle.metadataHandle.appendPage(page);
									}
									pNewHash_1->writeInDiskFormat(page);
									ixfileHandle.metadataHandle.writePage(
											pNewHash_1->pageID, page);
								}
								pageChain_1.push_back(
										pNewHash_1->overflowPageID);
								unsigned tempID = pNewHash_1->overflowPageID;
								delete pNewHash_1;
								memset(page, 0, PAGE_SIZE);
								pNewHash_1 = new ExtendibleHash(page, attribute,
										false, tempID);
								pNewHash_1->insertPair(tempChar, thisPair.rid);
							}

						} else {
							if (pNewHash_2->insertPair(tempChar, thisPair.rid)
									!= 0) {
								if (!primary2_full) {
									primary2_full = true;
									pNewHash_2->overflowPageID =
											ixfileHandle.metadataHandle.getNumberOfPages();
									if (ixfileHandle.reusableOverflowPages.size()
											> 0) {
										pNewHash_2->overflowPageID =
												ixfileHandle.reusableOverflowPages.front();
										ixfileHandle.reusableOverflowPages.pop_front();
									} else {
										memset(page, 0, PAGE_SIZE);
										ixfileHandle.metadataHandle.appendPage(
												page);
									}
									pNewHash_2->writeInDiskFormat(page);
									if (ixfileHandle.primaryHandle.getNumberOfPages()
											<= pNewHash_2->pageID) {
										ixfileHandle.primaryHandle.appendPage(
												page);
									} else {
										ixfileHandle.primaryHandle.writePage(
												pNewHash_2->pageID, page);
									}

								} else {
									pNewHash_2->overflowPageID =
											ixfileHandle.metadataHandle.getNumberOfPages();
									if (ixfileHandle.reusableOverflowPages.size()
											> 0) {
										pNewHash_2->overflowPageID =
												ixfileHandle.reusableOverflowPages.front();
										ixfileHandle.reusableOverflowPages.pop_front();
									} else {
										memset(page, 0, PAGE_SIZE);
										ixfileHandle.metadataHandle.appendPage(
												page);
									}
									pNewHash_2->writeInDiskFormat(page);
									ixfileHandle.metadataHandle.writePage(
											pageChain_2.back(), page);
								}
								pageChain_2.push_back(
										pNewHash_2->overflowPageID);
								unsigned tempID = pNewHash_2->overflowPageID;
								delete pNewHash_2;
								memset(page, 0, PAGE_SIZE);
								pNewHash_2 = new ExtendibleHash(page, attribute,
										false, tempID);
								pNewHash_2->insertPair(tempChar, thisPair.rid);
							}
						}
						thisList.pop_front();
					}
				}
				unsigned thisOverflowPage = pOriginHash->overflowPageID;
				delete pOriginHash;
				while (thisOverflowPage != 0) {
					ixfileHandle.metadataHandle.readPage(thisOverflowPage,
							page);
					ExtendibleHash *pOriginHash = new ExtendibleHash(page,
							attribute, false, thisOverflowPage);
					ixfileHandle.reusableOverflowPages.push_back(
							thisOverflowPage);

					for (itor = pOriginHash->content.begin();
							itor != pOriginHash->content.end(); ++itor) {
						std::list<ValueRIDPair> & thisList = itor->second;
						while (thisList.size() > 0) {
							ValueRIDPair & thisPair = thisList.front();
							if (attribute.type == TypeInt) {
								memcpy(tempChar, &(thisPair.intValue),
										sizeof(int));
							} else if (attribute.type == TypeReal) {
								memcpy(tempChar, &(thisPair.realValue),
										sizeof(float));
							} else {
								unsigned varCharLength =
										thisPair.varChar.size();
								memcpy(tempChar, &(varCharLength),
										sizeof(unsigned));
								memcpy(tempChar + sizeof(unsigned),
										thisPair.varChar.c_str(),
										varCharLength);
							}
							unsigned thisHashValue = hash(attribute, tempChar);
							unsigned thisBucketNum = thisHashValue % (N << 1);

							if (thisBucketNum == splitBucketNumber) {
								if (pNewHash_1->insertPair(tempChar,
										thisPair.rid) != 0) {
									if (!primary1_full) {
										primary1_full = true;
										pNewHash_1->overflowPageID =
												ixfileHandle.metadataHandle.getNumberOfPages();
										if (ixfileHandle.reusableOverflowPages.size()
												> 0) {
											pNewHash_1->overflowPageID =
													ixfileHandle.reusableOverflowPages.front();
											ixfileHandle.reusableOverflowPages.pop_front();
										}else {
											memset(page, 0, PAGE_SIZE);
											ixfileHandle.metadataHandle.appendPage(page);
										}
										pNewHash_1->writeInDiskFormat(page);
										ixfileHandle.primaryHandle.writePage(
												pNewHash_1->pageID, page);
									} else {
										pNewHash_1->overflowPageID =
												ixfileHandle.metadataHandle.getNumberOfPages();
										if (ixfileHandle.reusableOverflowPages.size()
												> 0) {
											pNewHash_1->overflowPageID =
													ixfileHandle.reusableOverflowPages.front();
											ixfileHandle.reusableOverflowPages.pop_front();
										}else {
											memset(page, 0, PAGE_SIZE);
											ixfileHandle.metadataHandle.appendPage(page);
										}
										pNewHash_1->writeInDiskFormat(page);
										ixfileHandle.metadataHandle.writePage(
												pNewHash_1->pageID, page);
									}
									pageChain_1.push_back(
											pNewHash_1->overflowPageID);
									unsigned tempID = pNewHash_1->overflowPageID;
									delete pNewHash_1;
									memset(page, 0, PAGE_SIZE);
									pNewHash_1 = new ExtendibleHash(page,
											attribute, false, tempID);
									pNewHash_1->insertPair(tempChar,
											thisPair.rid);
								}

							} else {
								if (pNewHash_2->insertPair(tempChar,
										thisPair.rid) != 0) {
									if (!primary2_full) {
										primary2_full = true;
										pNewHash_2->overflowPageID =
												ixfileHandle.metadataHandle.getNumberOfPages();
										if (ixfileHandle.reusableOverflowPages.size()
												> 0) {
											pNewHash_2->overflowPageID =
													ixfileHandle.reusableOverflowPages.front();
											ixfileHandle.reusableOverflowPages.pop_front();
										} else {
											memset(page, 0, PAGE_SIZE);
											ixfileHandle.metadataHandle.appendPage(
													page);
										}
										pNewHash_2->writeInDiskFormat(page);
										if (ixfileHandle.primaryHandle.getNumberOfPages()
												<= pNewHash_2->pageID) {
											ixfileHandle.primaryHandle.appendPage(
													page);
										} else {
											ixfileHandle.primaryHandle.writePage(
													pNewHash_2->pageID, page);
										}

									} else {
										pNewHash_2->overflowPageID =
												ixfileHandle.metadataHandle.getNumberOfPages();
										if (ixfileHandle.reusableOverflowPages.size()
												> 0) {
											pNewHash_2->overflowPageID =
													ixfileHandle.reusableOverflowPages.front();
											ixfileHandle.reusableOverflowPages.pop_front();
										} else {
											memset(page, 0, PAGE_SIZE);
											ixfileHandle.metadataHandle.appendPage(
													page);
										}
										pNewHash_2->writeInDiskFormat(page);
										ixfileHandle.metadataHandle.writePage(
												pageChain_2.back(), page);
									}
									pageChain_2.push_back(
											pNewHash_2->overflowPageID);
									unsigned tempID = pNewHash_2->overflowPageID;
									delete pNewHash_2;
									memset(page, 0, PAGE_SIZE);
									pNewHash_2 = new ExtendibleHash(page,
											attribute, false, tempID);
									pNewHash_2->insertPair(tempChar,
											thisPair.rid);
								}
							}
							thisList.pop_front();
						}
					}

					thisOverflowPage = pOriginHash->overflowPageID;
					delete pOriginHash;
				}

				if (!primary1_full) {
					pNewHash_1->writeInDiskFormat(page);
					ixfileHandle.primaryHandle.writePage(pNewHash_1->pageID,
							page);
				} else {
					pNewHash_1->writeInDiskFormat(page);
					if (pNewHash_1->dirty) {
						ixfileHandle.metadataHandle.writePage(
								pNewHash_1->pageID, page);
					} else {
						ixfileHandle.reusableOverflowPages.push_back(
								pageChain_1.back());
						//update the previous page
						pageChain_1.pop_back();
						if (pageChain_1.size() == 1) {
							ixfileHandle.primaryHandle.readPage(
									pageChain_1.back(), page);
							memset(page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES, 0,
							OVERFLOW_PAGEID_BYTES);
							ixfileHandle.primaryHandle.writePage(
									pageChain_1.back(), page);
						} else {
							ixfileHandle.metadataHandle.readPage(
									pageChain_1.back(), page);
							memset(page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES, 0,
							OVERFLOW_PAGEID_BYTES);
							ixfileHandle.metadataHandle.writePage(
									pageChain_1.back(), page);
						}
					}
				}
				delete pNewHash_1;

				if (!primary2_full) {
					pNewHash_2->writeInDiskFormat(page);
					if (ixfileHandle.primaryHandle.getNumberOfPages()
							<= pNewHash_2->pageID) {
						ixfileHandle.primaryHandle.appendPage(page);
					} else {
						ixfileHandle.primaryHandle.writePage(pNewHash_2->pageID,
								page);
					}

				} else {
					pNewHash_2->writeInDiskFormat(page);
					if (pNewHash_2->dirty) {
						ixfileHandle.metadataHandle.writePage(
								pNewHash_2->pageID, page);
					} else {
						ixfileHandle.reusableOverflowPages.push_back(
								pageChain_2.back());
						//update the previous page
						pageChain_2.pop_back();
						if (pageChain_2.size() == 1) {
							ixfileHandle.primaryHandle.readPage(
									pageChain_2.back(), page);
							memset(page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES, 0,
							OVERFLOW_PAGEID_BYTES);
							ixfileHandle.primaryHandle.writePage(
									pageChain_2.back(), page);
						} else {
							ixfileHandle.metadataHandle.readPage(
									pageChain_2.back(), page);
							memset(page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES, 0,
							OVERFLOW_PAGEID_BYTES);
							ixfileHandle.metadataHandle.writePage(
									pageChain_2.back(), page);
						}
					}
				}
				delete pNewHash_2;

				++Next;
				if (Next == N) {
					++Level;
					Next = 0;
					N = N << 1;
				}
				ixfileHandle.Next = Next;
				ixfileHandle.Level = Level;
				ixfileHandle.N = N;
				return 0;
			}
		}
	}

	return INSERT_UNHANDLED_ERROR;
}

/**
 * Find the entry
 * Delete it
 * And try to move one entry from the overflow page to this page
 * If the overflow page is empty, recycle it
 * If triggered, merge LH
 */
RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle,
		const Attribute &attribute, const void *key, const RID &rid) {

	int intKey = 0;
	float floatKey = 0.0;
	string strKey;
	char temp[PAGE_SIZE];
	unsigned LH_hashValue = hash(attribute, key);
	if (attribute.type == TypeInt) {
		memcpy(&intKey, key, sizeof(int));
	} else if (attribute.type == TypeReal) {
		memcpy(&floatKey, key, sizeof(float));
	} else {
		unsigned thisLength = 0;
		memcpy(&thisLength, key, sizeof(unsigned));
		memcpy(temp, (char*)key + sizeof(unsigned), thisLength);
		temp[thisLength] = '\0';
		strKey = temp;
	}

	unsigned bucketNum = LH_hashValue % ixfileHandle.N;
	if (bucketNum < ixfileHandle.Next) {
		// if the bucket has been split
		bucketNum = LH_hashValue % (ixfileHandle.N << 1);
	}

	char buffer[PAGE_SIZE];
	ixfileHandle.primaryHandle.readPage(bucketNum, buffer);
	unsigned triedPageID = 0;
//	RID thisRID;
//	unsigned thisLength;
//	unsigned lastLength;

	bool findMatch = false;
	bool primaryDone = false;
	bool isEmpty = false;
	bool LHMerge = false;

	/******************Logic*******************
	 * 1. find the entry,
	 * 2. try to steal an entry with different key from the last page
	 * 3. if the page is empty and it is overflow page, then trigger LH merge
	 *    3.1 make sure we reuse the overflow page of the deleted page (to support deletion scan)
	 ******************************************/

	triedPageID = bucketNum;
	vector<unsigned> pageIDChain;
	while (true) {
		ExtendibleHash thisEH(buffer, attribute, !primaryDone, triedPageID);
		primaryDone = true;
		pageIDChain.push_back(thisEH.pageID);
		for (std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator mapItor =
				thisEH.content.begin(); mapItor != thisEH.content.end();
				++mapItor) {
			for (std::list<ValueRIDPair>::iterator listItor =
					mapItor->second.begin(); listItor != mapItor->second.end();
					++listItor) {
				if (attribute.type == TypeInt) {
					if (intKey == listItor->intValue
							&& rid.pageNum == listItor->rid.pageNum
							&& rid.slotNum == listItor->rid.slotNum) {
						findMatch = true;
						mapItor->second.erase(listItor);
						if (thisEH.getTotalItemNumber() == 0) {
							isEmpty = true;
						}
						break;
					}
				} else if (attribute.type == TypeReal
						&& rid.pageNum == listItor->rid.pageNum
						&& rid.slotNum == listItor->rid.slotNum) {
					if (floatKey == listItor->realValue) {
						findMatch = true;
						mapItor->second.erase(listItor);
						if (thisEH.getTotalItemNumber() == 0) {
							isEmpty = true;
						}
						break;
					}
				} else {
					if (strKey.compare(listItor->varChar) == 0
							&& rid.pageNum == listItor->rid.slotNum
							&& rid.slotNum == listItor->rid.slotNum) {
						findMatch = true;
						mapItor->second.erase(listItor);
						if (thisEH.getTotalItemNumber() == 0) {
							isEmpty = true;
						}
						break;
					}
				}
			}
			if (findMatch) {
				break;
			}
		}
		if (findMatch) {

			//write back to the disk
			thisEH.writeInDiskFormat(buffer);
			if (thisEH.isPrimaryPage) {
				ixfileHandle.primaryHandle.writePage(thisEH.pageID, buffer);
			} else {
				ixfileHandle.metadataHandle.writePage(thisEH.pageID, buffer);
			}

			if (isEmpty && !thisEH.isPrimaryPage
					&& (ixfileHandle.Level != 0 || ixfileHandle.Next != 0)) {
				LHMerge = true;
			}

			if (isEmpty) {
				ixfileHandle.reusableOverflowPages.push_back(thisEH.pageID);
				pageIDChain.pop_back();
				if (pageIDChain.size() == 1) {
					unsigned tempID = pageIDChain.back();
					ixfileHandle.primaryHandle.readPage(tempID, buffer);
					memcpy(buffer + PAGE_SIZE - OVERFLOW_PAGEID_BYTES,
							&(thisEH.overflowPageID), sizeof(unsigned));
					ixfileHandle.primaryHandle.writePage(tempID, buffer);
				} else if (pageIDChain.size() > 1) {
					unsigned tempID = pageIDChain.back();
					ixfileHandle.metadataHandle.readPage(tempID, buffer);
					memcpy(buffer + PAGE_SIZE - OVERFLOW_PAGEID_BYTES,
							&(thisEH.overflowPageID), sizeof(unsigned));
					ixfileHandle.metadataHandle.writePage(tempID, buffer);
				}
			}

			if (LHMerge) {
				if (ixfileHandle.Next == 0) {
					ixfileHandle.N = ixfileHandle.N << 1;
					ixfileHandle.Next = ixfileHandle.N - 1;
					--ixfileHandle.Level;
				} else {
					--ixfileHandle.Next;
				}
				unsigned mergedPrimaryPageID_1 = ixfileHandle.Next;
				unsigned mergedPrimaryPageID_2 = ixfileHandle.Next
						+ ixfileHandle.N;

				ixfileHandle.primaryHandle.readPage(mergedPrimaryPageID_1,
						buffer);
				vector<unsigned> destPageChain;
				ExtendibleHash * destPage = new ExtendibleHash(buffer,
						attribute, true, mergedPrimaryPageID_1);
				destPageChain.push_back(mergedPrimaryPageID_1);
				unsigned nextPageID = destPage->pageID;
				unsigned appendingPageID = 0;
				bool destPageIsReused = false;
				while (nextPageID != 0) {
					if (nextPageID == thisEH.overflowPageID) {
						appendingPageID = nextPageID;
						break;
					}
					ixfileHandle.metadataHandle.readPage(nextPageID, buffer);
					ExtendibleHash * srcPage = new ExtendibleHash(buffer,
							attribute, false, nextPageID);
					ixfileHandle.reusableOverflowPages.push_back(nextPageID);
					for (std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator mapItor =
							srcPage->content.begin();
							mapItor != srcPage->content.end(); ++mapItor) {
						for (std::list<ValueRIDPair>::iterator listItor =
								mapItor->second.begin();
								listItor != mapItor->second.end(); ++listItor) {
							if (attribute.type == TypeInt) {
								memcpy(temp, &(listItor->intValue),
										sizeof(int));
							} else if (attribute.type == TypeReal) {
								memcpy(temp, &(listItor->realValue),
										sizeof(float));
							} else {
								unsigned tempLength = listItor->varChar.size();
								memcpy(temp, &tempLength, sizeof(unsigned));
								memcpy(temp + sizeof(unsigned),
										listItor->varChar.c_str(), tempLength);
							}
							if (destPage->insertPair(temp, listItor->rid)
									!= 0) {
								bool newPageIsReused = false;
								unsigned newDestPageID = 0;
								if (ixfileHandle.reusableOverflowPages.size()
										> 0) {
									newDestPageID =
											ixfileHandle.reusableOverflowPages.front();
									ixfileHandle.reusableOverflowPages.pop_front();
									newPageIsReused = true;
								} else {
									newDestPageID =
											ixfileHandle.metadataHandle.getNumberOfPages();
									newPageIsReused = false;
								}
								destPage->overflowPageID = newDestPageID;
								destPage->writeInDiskFormat(buffer);
								if (destPage->isPrimaryPage) {
									ixfileHandle.primaryHandle.writePage(
											destPage->pageID, buffer);
								} else if (destPageIsReused) {
									ixfileHandle.metadataHandle.writePage(
											destPage->pageID, buffer);
								} else {
									ixfileHandle.metadataHandle.appendPage(
											buffer);
								}
								delete destPage;
								memset(buffer, 0, PAGE_SIZE);
								destPage = new ExtendibleHash(buffer, attribute,
										false, newDestPageID);
								destPageChain.push_back(newDestPageID);
								destPage->insertPair(temp, listItor->rid);
								destPageIsReused = newPageIsReused;
							}
						}
					}
					nextPageID = srcPage->overflowPageID;
					delete srcPage;
				}

				ixfileHandle.primaryHandle.readPage(mergedPrimaryPageID_2,
						buffer);
				ExtendibleHash * srcPage = new ExtendibleHash(buffer, attribute,
						true, mergedPrimaryPageID_2);
				ixfileHandle.reusableOverflowPages.push_back(nextPageID);
				for (std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator mapItor =
						srcPage->content.begin();
						mapItor != srcPage->content.end(); ++mapItor) {
					for (std::list<ValueRIDPair>::iterator listItor =
							mapItor->second.begin();
							listItor != mapItor->second.end(); ++listItor) {
						if (attribute.type == TypeInt) {
							memcpy(temp, &(listItor->intValue), sizeof(int));
						} else if (attribute.type == TypeReal) {
							memcpy(temp, &(listItor->realValue), sizeof(float));
						} else {
							unsigned tempLength = listItor->varChar.size();
							memcpy(temp, &tempLength, sizeof(unsigned));
							memcpy(temp + sizeof(unsigned),
									listItor->varChar.c_str(), tempLength);
						}
						if (destPage->insertPair(temp, listItor->rid) != 0) {
							bool newPageIsReused = false;
							unsigned newDestPageID = 0;
							if (ixfileHandle.reusableOverflowPages.size() > 0) {
								newDestPageID =
										ixfileHandle.reusableOverflowPages.front();
								ixfileHandle.reusableOverflowPages.pop_front();
								newPageIsReused = true;
							} else {
								newDestPageID =
										ixfileHandle.metadataHandle.getNumberOfPages();
								newPageIsReused = false;
							}
							destPage->overflowPageID = newDestPageID;
							destPage->writeInDiskFormat(buffer);
							if (destPage->isPrimaryPage) {
								ixfileHandle.primaryHandle.writePage(
										destPage->pageID, buffer);
							} else if (destPageIsReused) {
								ixfileHandle.metadataHandle.writePage(
										destPage->pageID, buffer);
							} else {
								ixfileHandle.metadataHandle.appendPage(buffer);
							}
							delete destPage;
							memset(buffer, 0, PAGE_SIZE);
							destPage = new ExtendibleHash(buffer, attribute,
									false, newDestPageID);
							destPageChain.push_back(newDestPageID);
							destPage->insertPair(temp, listItor->rid);
							destPageIsReused = newPageIsReused;
						}
					}
				}
				nextPageID = srcPage->overflowPageID;
				delete srcPage;

				while (nextPageID != 0) {
					if (nextPageID == thisEH.overflowPageID) {
						appendingPageID = nextPageID;
						break;
					}
					ixfileHandle.metadataHandle.readPage(nextPageID, buffer);
					ExtendibleHash * srcPage = new ExtendibleHash(buffer,
							attribute, false, nextPageID);
					ixfileHandle.reusableOverflowPages.push_back(nextPageID);
					for (std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator mapItor =
							srcPage->content.begin();
							mapItor != srcPage->content.end(); ++mapItor) {
						for (std::list<ValueRIDPair>::iterator listItor =
								mapItor->second.begin();
								listItor != mapItor->second.end(); ++listItor) {
							if (attribute.type == TypeInt) {
								memcpy(temp, &(listItor->intValue),
										sizeof(int));
							} else if (attribute.type == TypeReal) {
								memcpy(temp, &(listItor->realValue),
										sizeof(float));
							} else {
								unsigned tempLength = listItor->varChar.size();
								memcpy(temp, &tempLength, sizeof(unsigned));
								memcpy(temp + sizeof(unsigned),
										listItor->varChar.c_str(), tempLength);
							}
							if (destPage->insertPair(temp, listItor->rid)
									!= 0) {
								bool newPageIsReused = false;
								unsigned newDestPageID = 0;
								if (ixfileHandle.reusableOverflowPages.size()
										> 0) {
									newDestPageID =
											ixfileHandle.reusableOverflowPages.front();
									ixfileHandle.reusableOverflowPages.pop_front();
									newPageIsReused = true;
								} else {
									newDestPageID =
											ixfileHandle.metadataHandle.getNumberOfPages();
									newPageIsReused = false;
								}
								destPage->overflowPageID = newDestPageID;
								destPage->writeInDiskFormat(buffer);
								if (destPage->isPrimaryPage) {
									ixfileHandle.primaryHandle.writePage(
											destPage->pageID, buffer);
								} else if (destPageIsReused) {
									ixfileHandle.metadataHandle.writePage(
											destPage->pageID, buffer);
								} else {
									ixfileHandle.metadataHandle.appendPage(
											buffer);
								}
								delete destPage;
								memset(buffer, 0, PAGE_SIZE);
								destPage = new ExtendibleHash(buffer, attribute,
										false, newDestPageID);
								destPageChain.push_back(newDestPageID);
								destPage->insertPair(temp, listItor->rid);
								destPageIsReused = newPageIsReused;
							}
						}
					}
					nextPageID = srcPage->overflowPageID;
					delete srcPage;
				}
				if (appendingPageID != 0) {
					destPage->overflowPageID = appendingPageID;
				}

				if (destPage->getTotalItemNumber() == 0) {
					if (destPageIsReused) {
						ixfileHandle.reusableOverflowPages.push_back(
								destPage->pageID);
					} else {
						//No need to append
					}
					destPageChain.pop_back();
					if (destPageChain.size() == 1) {
						ixfileHandle.primaryHandle.readPage(
								destPageChain.back(), buffer);
						memcpy(buffer + PAGE_SIZE - OVERFLOW_PAGEID_BYTES,
								&(destPage->overflowPageID),
								OVERFLOW_PAGEID_BYTES);
						ixfileHandle.primaryHandle.writePage(
								destPageChain.back(), buffer);
					} else if (destPageChain.size() > 1) {
						ixfileHandle.metadataHandle.readPage(
								destPageChain.back(), buffer);
						memcpy(buffer + PAGE_SIZE - OVERFLOW_PAGEID_BYTES,
								&(destPage->overflowPageID),
								OVERFLOW_PAGEID_BYTES);
						ixfileHandle.metadataHandle.writePage(
								destPageChain.back(), buffer);
					}
				} else {
					destPage->writeInDiskFormat(buffer);
					if (destPage->isPrimaryPage) {
						ixfileHandle.primaryHandle.writePage(destPage->pageID,
								buffer);
					} else if (destPageIsReused) {
						ixfileHandle.metadataHandle.writePage(destPage->pageID,
								buffer);
					} else {
						ixfileHandle.metadataHandle.appendPage(buffer);
					}
				}
				delete destPage;
			}
			break;
		}
		if (thisEH.overflowPageID == 0) {
			break;
		} else {
			triedPageID = thisEH.overflowPageID;
			ixfileHandle.metadataHandle.readPage(triedPageID, buffer);
		}
	}

	if (findMatch) {
		return 0;
	}

	return DELETE_NOT_EXIST;
}

unsigned IndexManager::hash(const Attribute &attribute, const void *key) {
	if (attribute.type == TypeInt) {
		int intKey = *(int *) key;
		return hashInt(intKey);
	} else if (attribute.type == TypeReal) {
		float realKey = *(float *) key;
		return hashFloat(realKey);
	} else {
		unsigned varCharLength = *(unsigned *) key;
		char varChar[PAGE_SIZE];
		memcpy(varChar, (char*)key + sizeof(unsigned), varCharLength);
		varChar[varCharLength] = '\0';
		string tempString = varChar;
		return hashString(tempString);
	}
}

unsigned IndexManager::indexEntriesInAPage(IXFileHandle &ixfileHandle,
		const Attribute &attribute, const unsigned &primaryPageNumber) {
	unsigned numOfTotalEntry = 0;
	char page[PAGE_SIZE];
	ixfileHandle.primaryHandle.readPage(primaryPageNumber, page);
	ExtendibleHash * EH = new ExtendibleHash(page, attribute, 1,
			primaryPageNumber);
	unordered_map<unsigned, std::list<ValueRIDPair>>::iterator itor;
	for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
		std::list<ValueRIDPair> & bucketList = itor->second;
		numOfTotalEntry += bucketList.size();
	}
	while (EH->overflowPageID) { // has subsequent overflow page
		ixfileHandle.metadataHandle.readPage(EH->overflowPageID, page);
		unsigned overflowPage = EH->overflowPageID;
		delete EH;
		EH = new ExtendibleHash(page, attribute, 0, overflowPage);
		for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
			std::list<ValueRIDPair> & bucketList = itor->second;
			numOfTotalEntry += bucketList.size();
		}
	}
	return numOfTotalEntry;
}

unsigned IndexManager::totalEntries(IXFileHandle &ixfileHandle,
		const Attribute &attribute) {
	unsigned total = 0;
	for (unsigned i = 0; i < ixfileHandle.N + ixfileHandle.Next; ++i) {
		total += indexEntriesInAPage(ixfileHandle, attribute, i);
	}
	return total;
}

RC IndexManager::printIndexEntriesInAPage(IXFileHandle &ixfileHandle,
		const Attribute &attribute, const unsigned &primaryPageNumber) {
	unsigned numOfTotalEntry = 0;
	char page[PAGE_SIZE];
	ixfileHandle.primaryHandle.readPage(primaryPageNumber, page);
	ExtendibleHash * EH = new ExtendibleHash(page, attribute, 1,
			primaryPageNumber);
	unordered_map<unsigned, std::list<ValueRIDPair>>::iterator itor;
	for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
		std::list<ValueRIDPair> & bucketList = itor->second;
		numOfTotalEntry += bucketList.size();
	}
	while (EH->overflowPageID) { // has subsequent overflow page
		ixfileHandle.metadataHandle.readPage(EH->overflowPageID, page);
		unsigned overflowPage = EH->overflowPageID;
		delete EH;
		EH = new ExtendibleHash(page, attribute, 0, overflowPage);
		for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
			std::list<ValueRIDPair> & bucketList = itor->second;
			numOfTotalEntry += bucketList.size();
		}
	}

	cout << "Number of total entries in the page (+ overflow pages) : "
			<< numOfTotalEntry << endl;

	ixfileHandle.primaryHandle.readPage(primaryPageNumber, page);
	delete EH;
	EH = new ExtendibleHash(page, attribute, 1, primaryPageNumber);
	cout << "primary Page No." << primaryPageNumber << endl << endl;
	unsigned numOfPrimaryEntry = 0;
	for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
		std::list<ValueRIDPair> & bucketList = itor->second;
		numOfPrimaryEntry += bucketList.size();
	}
	cout << "a. # of entries : " << numOfPrimaryEntry << endl;
	cout << "b. entries: ";
	for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
		std::list<ValueRIDPair> & bucketList = itor->second;
		std::list<ValueRIDPair>::iterator listItor;
		for (listItor = bucketList.begin(); listItor != bucketList.end();
				++listItor) {
			if (attribute.type == TypeInt) {
				cout << "[" << listItor->intValue << "/"
						<< listItor->rid.pageNum << "," << listItor->rid.slotNum
						<< "] ";
			} else if (attribute.type == TypeReal) {
				cout << "[" << listItor->realValue << "/"
						<< listItor->rid.pageNum << "," << listItor->rid.slotNum
						<< "] ";
			} else {
				cout << "[" << listItor->varChar << "/" << listItor->rid.pageNum
						<< "," << listItor->rid.slotNum << "] ";
			}
		}
	}
	cout << endl << endl;

	while (EH->overflowPageID) { // has subsequent overflow page
		cout << "overflow Page No." << EH->overflowPageID << " linked to ";
		if (EH->isPrimaryPage)
			cout << "primary page " << EH->pageID << endl << endl;
		else
			cout << "overflow page" << EH->pageID << endl << endl;
		ixfileHandle.metadataHandle.readPage(EH->overflowPageID, page);
		unsigned overflowPage = EH->overflowPageID;
		delete EH;
		EH = new ExtendibleHash(page, attribute, 0, overflowPage);
		unsigned numOfOverflowEntry = 0;
		for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
			std::list<ValueRIDPair> & bucketList = itor->second;
			numOfOverflowEntry += bucketList.size();
		}
		cout << "a. # of entries : " << numOfOverflowEntry << endl;
		cout << "b. entries: ";
		for (itor = EH->content.begin(); itor != EH->content.end(); ++itor) {
			std::list<ValueRIDPair> & bucketList = itor->second;
			std::list<ValueRIDPair>::iterator listItor;
			for (listItor = bucketList.begin(); listItor != bucketList.end();
					++listItor) {
				if (attribute.type == TypeInt) {
					cout << "[" << listItor->intValue << "/"
							<< listItor->rid.pageNum << ","
							<< listItor->rid.slotNum << "] ";
				} else if (attribute.type == TypeReal) {
					cout << "[" << listItor->realValue << "/"
							<< listItor->rid.pageNum << ","
							<< listItor->rid.slotNum << "] ";
				} else {
					cout << "[" << listItor->varChar << "/"
							<< listItor->rid.pageNum << ","
							<< listItor->rid.slotNum << "] ";
				}
			}
		}
		cout << endl << endl;
	}
	delete EH;
	return 0;
}

RC IndexManager::getNumberOfPrimaryPages(IXFileHandle &ixfileHandle,
		unsigned &numberOfPrimaryPages) {
	numberOfPrimaryPages = ixfileHandle.N + ixfileHandle.Next;
	return 0;
}

RC IndexManager::getNumberOfAllPages(IXFileHandle &ixfileHandle,
		unsigned &numberOfAllPages) {
	numberOfAllPages = ixfileHandle.N + ixfileHandle.Next
			+ ixfileHandle.metadataHandle.getNumberOfPages()
			- ixfileHandle.reusableOverflowPages.size();
	return 0;
}

RC IndexManager::scan(IXFileHandle &ixfileHandle, const Attribute &attribute,
		const void *lowKey, const void *highKey, bool lowKeyInclusive,
		bool highKeyInclusive, IX_ScanIterator &ix_ScanIterator) {
	if (!ixfileHandle.valid) {
		return -1;
	}

	ix_ScanIterator.ptr_ixfileHandle = &ixfileHandle;
	ix_ScanIterator.lowKeyInclusive = lowKeyInclusive;
	ix_ScanIterator.highKeyInclusive = highKeyInclusive;
	ix_ScanIterator.attribute = attribute;

	ix_ScanIterator.exactScan = true;
	if (attribute.type == TypeInt) {
		ix_ScanIterator.lowKey.type = TypeInt;
		ix_ScanIterator.highKey.type = TypeInt;
		if (lowKey == 0) {
			ix_ScanIterator.negativeInf = true;
			ix_ScanIterator.exactScan = false;
		} else {
			ix_ScanIterator.lowKey.intValue = *((int *) lowKey);
		}
		if (highKey == 0) {
			ix_ScanIterator.positiveInf = true;
			ix_ScanIterator.exactScan = false;
		} else {
			ix_ScanIterator.highKey.intValue = *((int *) highKey);
		}
		if (ix_ScanIterator.exactScan) {
			if (ix_ScanIterator.lowKey.intValue
					!= ix_ScanIterator.highKey.intValue) {
				ix_ScanIterator.exactScan = false;
			}
		}
	} else if (attribute.type == TypeReal) {
		ix_ScanIterator.lowKey.type = TypeReal;
		ix_ScanIterator.highKey.type = TypeReal;
		if (lowKey == 0) {
			ix_ScanIterator.negativeInf = true;
			ix_ScanIterator.exactScan = false;
		} else {
			ix_ScanIterator.lowKey.floatValue = *((float *) lowKey);
		}
		if (highKey == 0) {
			ix_ScanIterator.positiveInf = true;
			ix_ScanIterator.exactScan = false;
		} else {
			ix_ScanIterator.highKey.floatValue = *((float *) highKey);
		}
		if (ix_ScanIterator.exactScan) {
			if (ix_ScanIterator.lowKey.floatValue
					!= ix_ScanIterator.highKey.floatValue) {
				ix_ScanIterator.exactScan = false;
			}
		}
	} else {
		ix_ScanIterator.lowKey.type = TypeVarChar;
		ix_ScanIterator.highKey.type = TypeVarChar;
		if (lowKey == 0) {
			ix_ScanIterator.negativeInf = true;
			ix_ScanIterator.exactScan = false;
		} else {
			unsigned length = *((unsigned *) lowKey);
			memcpy(ix_ScanIterator.buffer, (char*)lowKey + sizeof(unsigned), length);
			ix_ScanIterator.buffer[length] = '\0';
			ix_ScanIterator.lowKey.strValue = ix_ScanIterator.buffer;
		}
		if (highKey == 0) {
			ix_ScanIterator.positiveInf = true;
			ix_ScanIterator.exactScan = false;
		} else {
			unsigned length = *((unsigned *) highKey);
			memcpy(ix_ScanIterator.buffer, (char*)highKey + sizeof(unsigned), length);
			ix_ScanIterator.buffer[length] = '\0';
			ix_ScanIterator.highKey.strValue = ix_ScanIterator.buffer;
		}
		if (ix_ScanIterator.exactScan) {
			if (ix_ScanIterator.lowKey.strValue.compare(
					ix_ScanIterator.highKey.strValue) != 0) {
				ix_ScanIterator.exactScan = false;
			}
		}
	}

	ix_ScanIterator.bufferValid = false;
	if (ix_ScanIterator.exactScan) {
		if ((!ix_ScanIterator.lowKeyInclusive
				|| !ix_ScanIterator.highKeyInclusive)) {
			ix_ScanIterator.finished = true;
		}
	} else {
		if (!ix_ScanIterator.negativeInf && !ix_ScanIterator.positiveInf) {
			if (attribute.type == TypeInt) {
				if (ix_ScanIterator.lowKey.intValue
						> ix_ScanIterator.highKey.intValue) {
					ix_ScanIterator.finished = true;
				}
			} else if (attribute.type == TypeReal) {
				if (ix_ScanIterator.lowKey.floatValue
						> ix_ScanIterator.highKey.floatValue) {
					ix_ScanIterator.finished = true;
				}
			} else {
				if (ix_ScanIterator.lowKey.strValue.compare(
						ix_ScanIterator.highKey.strValue) > 0) {
					ix_ScanIterator.finished = true;
				}
			}
		}
	}

	return 0;
}

IX_ScanIterator::IX_ScanIterator() {
	exactScan = false;
	lowKeyInclusive = false;
	highKeyInclusive = false;
	negativeInf = false;
	positiveInf = false;
	ptr_ixfileHandle = 0;
	memset(buffer, 0, PAGE_SIZE);
	bufferPageID = 0;
	currentPrimaryPage = 0;
	primaryPage = false;
	bufferValid = false;
	EHBucketID = -1;
	nextEHPairOffset = -1;
	cachedEH = 0;
	finished = false;
	LH_hashValue = 0;
	EH_hashValue = 0;
	catchedInt = 0;
	catchedFloat = 0.0;
	catchedString = "";
	totalPrimaryPage = 0;
}

IX_ScanIterator::~IX_ScanIterator() {
}

RC IX_ScanIterator::getNextEntry(RID &rid, void *key) {
	if (finished) {
		return IX_EOF;
	}
	bool findMatch = false;
	if (exactScan) {
		bool skipLoadPage = false;
		if (!bufferValid) {
			if (attribute.type == TypeInt) {
				EH_hashValue = hashInt(lowKey.intValue);
			} else if (attribute.type == TypeReal) {
				EH_hashValue = hashFloat(lowKey.floatValue);
			} else {
				EH_hashValue = hashString(lowKey.strValue);
			}
			LH_hashValue = EH_hashValue;
			unsigned bucketNum = LH_hashValue % (ptr_ixfileHandle->N);
			if (bucketNum < ptr_ixfileHandle->Next) {
				// if the bucket has been split
				bucketNum = LH_hashValue % (ptr_ixfileHandle->N << 1);
			}
			bufferPageID = bucketNum;
			ptr_ixfileHandle->primaryHandle.readPage(bucketNum, buffer);
			bufferValid = true;
			nextEHPairOffset = 0;
			skipLoadPage = true;
		}
		char temp[PAGE_SIZE];
		do {
			while (nextEHPairOffset != 0) {
				unsigned thisLength = 0;
				if (attribute.type == TypeInt) {
					catchedInt = *((int *) (buffer + nextEHPairOffset));
					if (catchedInt == lowKey.intValue) {
						findMatch = true;
						memcpy(key, &catchedInt, sizeof(int));
						memcpy(&rid, buffer + nextEHPairOffset + sizeof(int),
								sizeof(RID));
						nextEHPairOffset =
								*((unsigned *) (buffer + nextEHPairOffset
										+ sizeof(int) + sizeof(RID)));
						break;
					}
					thisLength = sizeof(int) + sizeof(RID);
				} else if (attribute.type == TypeReal) {
					catchedFloat = *((float *) (buffer + nextEHPairOffset));
					if (catchedFloat == lowKey.floatValue) {
						findMatch = true;
						memcpy(key, &catchedFloat, sizeof(float));
						memcpy(&rid, buffer + nextEHPairOffset + sizeof(float),
								sizeof(RID));
						nextEHPairOffset =
								*((unsigned *) (buffer + nextEHPairOffset
										+ sizeof(float) + sizeof(RID)));
						break;
					}
					thisLength = sizeof(float) + sizeof(RID);
				} else {
					thisLength = *((unsigned *) (buffer + nextEHPairOffset));
					memcpy(temp, buffer + nextEHPairOffset + sizeof(unsigned),
							thisLength);
					temp[thisLength] = '\0';
					catchedString = temp;
					if (catchedString.compare(lowKey.strValue) == 0) {
						findMatch = true;
						memcpy(key, &thisLength, sizeof(unsigned));
						memcpy((char*)key + sizeof(unsigned), catchedString.c_str(),
								thisLength);
						memcpy(&rid,
								buffer + nextEHPairOffset + sizeof(unsigned)
										+ thisLength, sizeof(RID));
						nextEHPairOffset = *((unsigned *) (buffer
								+ nextEHPairOffset + sizeof(unsigned)
								+ thisLength + sizeof(RID)));
						break;
					}
					thisLength = sizeof(unsigned) + thisLength + sizeof(RID);
				}
				nextEHPairOffset = *((unsigned *) (buffer + nextEHPairOffset
						+ thisLength));
			}

			if (findMatch) {
				break;
			}

			if (!findMatch && !skipLoadPage) {
				unsigned overflowPageID = *((unsigned *) (buffer + PAGE_SIZE
						- OVERFLOW_PAGEID_BYTES));
				if (overflowPageID != 0) {
					ptr_ixfileHandle->metadataHandle.readPage(overflowPageID,
							buffer);
					bufferPageID = overflowPageID;
				} else {
					finished = true;
					break;
				}
			}

			unsigned globalDepth = *((unsigned *) (buffer + PAGE_SIZE
					- OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES
					- GLOBAL_DEPTH_BYTES));
			if (globalDepth < GLOBAL_DEPTH_MIN) {
				//Empty page, so just quit
				break;
			}
			EHBucketID = getMostSignificantBits(EH_hashValue, globalDepth);
			unsigned bucketHeadOffset = *((unsigned *) (buffer + PAGE_SIZE
					- OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES
					- GLOBAL_DEPTH_BYTES - (EHBucketID + 1) * sizeof(unsigned)));
			InPageBucketHead thisHead;
			memcpy(&thisHead, buffer + bucketHeadOffset,
					sizeof(InPageBucketHead));
			nextEHPairOffset = thisHead.nextPointer;
			if (thisHead.itemNumber == 0) {
				nextEHPairOffset = 0;
			}
			skipLoadPage = false;
		} while (!findMatch);

	} else {
		bool useOldItor = true;
		if (!bufferValid) {
			totalPrimaryPage = ptr_ixfileHandle->N + ptr_ixfileHandle->Next;
			if (totalPrimaryPage == 0) {
				cachedEH = 0;
				finished = true;
			} else {
				for (unsigned i = 0; i < totalPrimaryPage; ++i) {
					ptr_ixfileHandle->primaryHandle.readPage(i, buffer);
					cachedEH = new ExtendibleHash(buffer, attribute, true, i);
					bufferValid = true;
					currentPrimaryPage = i;
					bufferPageID = i;
					if (cachedEH->content.size() == 0) {
						delete cachedEH;
						continue;
					}
					contentItor = cachedEH->content.begin();
					listItor = contentItor->second.begin();
					break;
					//get valid listItor
				}
			}
		}

		for (unsigned i = currentPrimaryPage; i < totalPrimaryPage; ++i) {
			do {
				for (; contentItor != cachedEH->content.end(); ++contentItor) {
					if (!useOldItor) {
						listItor = contentItor->second.begin();
					}
					for (; listItor != contentItor->second.end(); ++listItor) {
						if (attribute.type == TypeInt) {
							if (satisfiedInt(listItor->intValue)) {
								findMatch = true;
								memcpy(key, &(listItor->intValue), sizeof(int));
								memcpy(&rid, &(listItor->rid), sizeof(RID));
								++listItor;
								break;
							}
						} else if (attribute.type == TypeReal) {
							if (satisfiedFloat(listItor->realValue)) {
								findMatch = true;
								memcpy(key, &(listItor->realValue),
										sizeof(float));
								memcpy(&rid, &(listItor->rid), sizeof(RID));
								++listItor;
								break;
							}
						} else {
							if (satisfiedString(listItor->varChar)) {
								findMatch = true;
								unsigned thisLength = listItor->varChar.size();
								memcpy(key, &thisLength, sizeof(unsigned));
								memcpy((char*)key + sizeof(unsigned),
										listItor->varChar.c_str(), thisLength);
								memcpy(&rid, &(listItor->rid), sizeof(RID));
								++listItor;
								break;
							}
						}
					}
					useOldItor = false;
					if (findMatch) {
						break;
					}
				}
				if (findMatch) {
					break;
				}
				if (cachedEH->overflowPageID != 0) {
					ptr_ixfileHandle->metadataHandle.readPage(
							cachedEH->overflowPageID, buffer);
					bufferPageID = cachedEH->overflowPageID;
					delete cachedEH;
					cachedEH = new ExtendibleHash(buffer, attribute, false,
							bufferPageID);
					contentItor = cachedEH->content.begin();
				} else {
					delete cachedEH;
					cachedEH = 0;
				}
			} while (cachedEH != 0);
			if (findMatch) {
				break;
			}
			if (i < totalPrimaryPage - 1) {
				currentPrimaryPage = i + 1;
				ptr_ixfileHandle->primaryHandle.readPage(i + 1, buffer);
				bufferPageID = i + 1;
				if (cachedEH != 0) {
					delete cachedEH;
				}
				cachedEH = new ExtendibleHash(buffer, attribute, true, i + 1);
				contentItor = cachedEH->content.begin();
				listItor = contentItor->second.begin();
			}
		}
		if (!findMatch) {
			if (cachedEH != 0) {
				delete cachedEH;
				cachedEH = 0;
			}
		}
	}
	if (findMatch) {
		return 0;
	}
	finished = true;
	return IX_EOF;
}

RC IX_ScanIterator::close() {
	finished = true;
	bufferValid = false;
	bufferPageID = 0;
	currentPrimaryPage = 0;
	ptr_ixfileHandle = 0;
	if (cachedEH != 0) {
		delete cachedEH;
		cachedEH = 0;
	}
	return 0;
}

IXFileHandle::IXFileHandle() {
	writePageCounter = 0;
	readPageCounter = 0;
	appendPageCounter = 0;
	Next = 0;
	N = 0;
	Level = 0;
	valid = false;
}

IXFileHandle::~IXFileHandle() {
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount,
		unsigned &writePageCount, unsigned &appendPageCount) {
	readPageCounter = primaryHandle.readPageCounter
			+ metadataHandle.readPageCounter;
	writePageCounter = primaryHandle.writePageCounter
			+ metadataHandle.writePageCounter;
	appendPageCounter = primaryHandle.appendPageCounter
			+ metadataHandle.appendPageCounter;
	readPageCount = readPageCounter;
	writePageCount = writePageCounter;
	appendPageCount = appendPageCounter;
	return 0;
}

void IX_PrintError(RC rc) {
	if (rc == CREATE_FILE_ERROR)
		cout << "Error in createFile" << endl;
	else if (rc == DESTROY_FILE_ERROR)
		cout << "Error in destroyFile" << endl;
	else if (rc == OPEN_FILE_ERROR)
		cout << "Error in openFile" << endl;
	else if (rc == CLOSE_FILE_ERROR)
		cout << "Error in closeFile" << endl;
	else if (rc > 16 && rc < 40)
		cout << "Error in insertEntry" << endl;
	else if (rc == DELETE_NOT_EXIST)
		cout << "Error in deleteEntry: non-existing entry" << endl;
}
