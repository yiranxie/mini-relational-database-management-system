#ifndef _ix_h_
#define _ix_h_

#include <vector>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <iostream>
#include <functional>

#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include "../rbf/rbfm.h"

#define IX_EOF (-1)  // end of the index scan

/******************************************
 * Parameters Definition
 ******************************************/
#define OVERFLOW_PAGEID_BYTES 4
#define FREE_POINTER_BYTES 4
#define GLOBAL_DEPTH_BYTES 4
#define GLOBAL_DEPTH_MAX 7
#define GLOBAL_DEPTH_MIN 2
#define INPAGE_DIR_ITEM_BYTES 4
#define INPAGE_BUCKET_SIZE 32
#define INDEX_ITEM_MAX_SIZE 2048

/******************************************
 * Error Code Definition
 ******************************************/
#define CREATE_FILE_ERROR 4
#define DESTROY_FILE_ERROR 8
#define OPEN_FILE_ERROR 12
#define CLOSE_FILE_ERROR 16
#define INSERT_INDEX_SIZE_EXCEEDS_LIMITS 20
#define INSERT_PAGE_HAS_NO_ENOUGH_SPACE 24
#define INSERT_EH_HAS_NO_ENOUGH_SLOT 28
#define INSERT_RID_ALREADY_EXIST 32
#define INSERT_UNHANDLED_ERROR 36
#define DELETE_NOT_EXIST 40

/**
 * Primary File:
 * 	(1) indices pages
 *
 * Metadata File:
 * 	(1)	page 0 metadata (including Level, N, Next)
 * 	(2)	following pages are the overflow pages
 *
 * Indices consist of pages (the structure of primary file page and metadata file page is the same),
 * which we can consider as a bucket in Linear Hashing
 * In each page, we use extendible hashing to store the <key, RID> pairs.
 *
 * ***************PAGE STRUCTURE*******************
 * |in-page bucket head/in-page bucket item|                  ...                  |
 * |                                     ...                                       |
 * |     ...     |global directory|global depth|free space pointer|overflow page ID|
 *
 * in-page bucket head: local depth, item number, next pointer
 * in-page bucket item: <key, RID>, next pointer
 */

struct InPageBucketHead {
	unsigned localDepth;
	unsigned itemNumber;
	unsigned nextPointer;
};

unsigned reverse(register unsigned int x);

// get the most significant N bits of an unsigned value
unsigned getMostSignificantBits(unsigned value, unsigned N);

/*
 * Reference: http://burtleburtle.net/bob/hash/integer.html
 */
unsigned hashInt(unsigned a);
//{
//	//Designed by Thomas Wang
//	unsigned result = a;
//	result += ~(result << 15);
//	result ^= (result >> 10);
//	result += (result << 3);
//	result ^= (result >> 6);
//	result += ~(result << 11);
//	result ^= (result >> 16);
//	return result;
//}

/*
 * Consider the float content as an integer
 */
unsigned hashFloat(float a);
//{
//	std::hash<float> hashFloat;
//	return hashFloat(a);
//
//	//TODO test new hash function
////	unsigned temp;
////	memcpy(&temp, &a, sizeof(unsigned));
////	return hashInt(temp);
//}

/*
 * Reference: http://www.strchr.com/hash_functions
 */
const unsigned SBoxTable[256] = {
	0x4660c395, 0x3baba6c5, 0x27ec605b, 0xdfc1d81a, 0xaaac4406, 0x3783e9b8, 0xa4e87c68, 0x62dc1b2a,
	0xa8830d34, 0x10a56307, 0x4ba469e3, 0x54836450, 0x1b0223d4, 0x23312e32, 0xc04e13fe, 0x3b3d61fa,
	0xdab2d0ea, 0x297286b1, 0x73dbf93f, 0x6bb1158b, 0x46867fe2, 0xb7fb5313, 0x3146f063, 0x4fd4c7cb,
	0xa59780fa, 0x9fa38c24, 0x38c63986, 0xa0bac49f, 0xd47d3386, 0x49f44707, 0xa28dea30, 0xd0f30e6d,
	0xd5ca7704, 0x934698e3, 0x1a1ddd6d, 0xfa026c39, 0xd72f0fe6, 0x4d52eb70, 0xe99126df, 0xdfdaed86,
	0x4f649da8, 0x427212bb, 0xc728b983, 0x7ca5d563, 0x5e6164e5, 0xe41d3a24, 0x10018a23, 0x5a12e111,
	0x999ebc05, 0xf1383400, 0x50b92a7c, 0xa37f7577, 0x2c126291, 0x9daf79b2, 0xdea086b1, 0x85b1f03d,
	0x598ce687, 0xf3f5f6b9, 0xe55c5c74, 0x791733af, 0x39954ea8, 0xafcff761, 0x5fea64f1, 0x216d43b4,
	0xd039f8c1, 0xa6cf1125, 0xc14b7939, 0xb6ac7001, 0x138a2eff, 0x2f7875d6, 0xfe298e40, 0x4a3fad3b,
	0x066207fd, 0x8d4dd630, 0x96998973, 0xe656ac56, 0xbb2df109, 0x0ee1ec32, 0x03673d6c, 0xd20fb97d,
	0x2c09423c, 0x093eb555, 0xab77c1e2, 0x64607bf2, 0x945204bd, 0xe8819613, 0xb59de0e3, 0x5df7fc9a,
	0x82542258, 0xfb0ee357, 0xda2a4356, 0x5c97ab61, 0x8076e10d, 0x48e4b3cc, 0x7c28ec12, 0xb17986e1,
	0x01735836, 0x1b826322, 0x6602a990, 0x7c1cef68, 0xe102458e, 0xa5564a67, 0x1136b393, 0x98dc0ea1,
	0x3b6f59e5, 0x9efe981d, 0x35fafbe0, 0xc9949ec2, 0x62c765f9, 0x510cab26, 0xbe071300, 0x7ee1d449,
	0xcc71beef, 0xfbb4284e, 0xbfc02ce7, 0xdf734c93, 0x2f8cebcd, 0xfeedc6ab, 0x5476ee54, 0xbd2b5ff9,
	0xf4fd0352, 0x67f9d6ea, 0x7b70db05, 0x5a5f5310, 0x482dd7aa, 0xa0a66735, 0x321ae71f, 0x8e8ad56c,
	0x27a509c3, 0x1690b261, 0x4494b132, 0xc43a42a7, 0x3f60a7a6, 0xd63779ff, 0xe69c1659, 0xd15972c8,
	0x5f6cdb0c, 0xb9415af2, 0x1261ad8d, 0xb70a6135, 0x52ceda5e, 0xd4591dc3, 0x442b793c, 0xe50e2dee,
	0x6f90fc79, 0xd9ecc8f9, 0x063dd233, 0x6cf2e985, 0xe62cfbe9, 0x3466e821, 0x2c8377a2, 0x00b9f14e,
	0x237c4751, 0x40d4a33b, 0x919df7e8, 0xa16991a4, 0xc5295033, 0x5c507944, 0x89510e2b, 0xb5f7d902,
	0xd2d439a6, 0xc23e5216, 0xd52d9de3, 0x534a5e05, 0x762e73d4, 0x3c147760, 0x2d189706, 0x20aa0564,
	0xb07bbc3b, 0x8183e2de, 0xebc28889, 0xf839ed29, 0x532278f7, 0x41f8b31b, 0x762e89c1, 0xa1e71830,
	0xac049bfc, 0x9b7f839c, 0x8fd9208d, 0x2d2402ed, 0xf1f06670, 0x2711d695, 0x5b9e8fe4, 0xdc935762,
	0xa56b794f, 0xd8666b88, 0x6872c274, 0xbc603be2, 0x2196689b, 0x5b2b5f7a, 0x00c77076, 0x16bfa292,
	0xc2f86524, 0xdd92e83e, 0xab60a3d4, 0x92daf8bd, 0x1fe14c62, 0xf0ff82cc, 0xc0ed8d0a, 0x64356e4d,
	0x7e996b28, 0x81aad3e8, 0x05a22d56, 0xc4b25d4f, 0x5e3683e5, 0x811c2881, 0x124b1041, 0xdb1b4f02,
	0x5a72b5cc, 0x07f8d94e, 0xe5740463, 0x498632ad, 0x7357ffb1, 0x0dddd380, 0x3d095486, 0x2569b0a9,
	0xd6e054ae, 0x14a47e22, 0x73ec8dcc, 0x004968cf, 0xe0c3a853, 0xc9b50a03, 0xe1b0eb17, 0x57c6f281,
	0xc9f9377d, 0x43e03612, 0x9a0c4554, 0xbb2d83ff, 0xa818ffee, 0xf407db87, 0x175e3847, 0x5597168f,
	0xd3d547a7, 0x78f3157c, 0xfc750f20, 0x9880a1c6, 0x1af41571, 0x95d01dfc, 0xa3968d62, 0xeae03cf8,
	0x02ee4662, 0x5f1943ff, 0x252d9d1c, 0x6b718887, 0xe052f724, 0x4cefa30b, 0xdcc31a00, 0xe4d0024d,
	0xdbb4534a, 0xce01f5c8, 0x0c072b61, 0x5d59736a, 0x60291da4, 0x1fbe2c71, 0x2f11d09c, 0x9dce266a,
};

unsigned hashString(string & a);
//{
//	std::hash<string> hashString;
//	return hashString(a);
//
//	//TODO test new hash function
//
//	//JS (Justin Sobel) Hash Function
////	unsigned charLength = a.size();
////	if (charLength == 0){
////		return 0;
////	}
////	unsigned result = 1315423911;
////	const char * pChar = a.c_str();
////	for (unsigned i=0; i<charLength; ++i){
////		unsigned thisChar = *(pChar + i);
////		result ^= ((result << 5) + thisChar + (result >> 2));
////	}
////	return result;
//
//	//TODO SBox Hash algorithm
//	//Get from http://floodyberry.com/noncryptohashzoo/SBox.html
////	unsigned seed = 1313131313;
////	unsigned charLength = a.size();
////	if (charLength == 0) {
////		return 0;
////	}
////	unsigned result = charLength + seed;
////	char * pChar = a.c_str();
////	for ( ; charLength & ~1; charLength -= 2, pChar += 2 ){
////		result = (((result ^ SBoxTable[pChar[0]]) * 3) ^ SBoxTable[pChar[1]]) * 3;
////	}
////	if (charLength & 1){
////		result = ( result ^ SBoxTable[pChar[0]] ) * 3;
////	}
////	result += ( result >> 22 ) ^ ( result << 4 );
////	return result;
//}

/*
 * Reference: http://burtleburtle.net/bob/hash/integer.html
 */
unsigned hashInt_2(unsigned a);
//{
//	unsigned result = a;
//	result = (result + 0x7ed55d16) + (result << 12);
//	result = (result ^ 0xc761c23c) ^ (result >> 19);
//	result = (result + 0x165667b1) + (result << 5);
//	result = (result + 0xd3a2646c) ^ (result << 9);
//	result = (result + 0xfd7046c5) + (result << 3);
//	result = (result ^ 0xb55a4f09) ^ (result >> 16);
//	return result;
//}

unsigned hashFloat_2(unsigned a);
//{
//	unsigned temp;
//	memcpy(&temp, &a, sizeof(unsigned));
//	return hashInt_2(temp);
//}

/*
 * Reference: http://programmers.stackexchange.com/questions/49550/which-hashing-algorithm-is-best-for-uniqueness-and-speed
 * CRC32, http://www.opensource.apple.com/source/xnu/xnu-1456.1.26/bsd/libkern/crc32.c
 * FVN-1a, http://ctips.pbworks.com/w/page/7277591/FNV%20Hash
 * Murmur
 */
#define FNV_PRIME_32 16777619
#define FNV_OFFSET_32 2166136261U

unsigned hashString_2(string & a);
//{
//	unsigned charLength = a.size();
//	if (charLength == 0) {
//		return 0;
//	}
//	unsigned result = FNV_OFFSET_32;
//	const char * pChar = a.c_str();
//	for(unsigned i = 0; i < charLength; ++i){
//		result = result ^ (pChar[i]); // xor next byte into the bottom of the result
//		result = result * FNV_PRIME_32; // Multiply by prime number found to work well
//	}
//	return result;;
//}

class IX_ScanIterator;
class IXFileHandle;

struct ValueRIDPair {
	RID rid;
	int intValue;
	float realValue;
	string varChar;
};

class ExtendibleHash {
public:
	unsigned globalDepth;
	Attribute attribute;
	unsigned overflowPageID;
	unsigned freePointer;
	std::vector<unsigned> dir;
	std::unordered_map<unsigned, std::list<ValueRIDPair>> content;
	bool dirty;
	bool isPrimaryPage;
	unsigned pageID;

	ExtendibleHash(const char *page, const Attribute &attribute,
			bool isPrimaryPage, unsigned pageID) {
		dirty = false;
		this->isPrimaryPage = isPrimaryPage;
		this->pageID = pageID;
		this->attribute = attribute;
		overflowPageID = *((unsigned int *) (page + PAGE_SIZE
				- OVERFLOW_PAGEID_BYTES));
		freePointer = *((unsigned int *) (page + PAGE_SIZE
				- OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES));
		globalDepth = *((unsigned int *) (page + PAGE_SIZE
				- OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES
				- GLOBAL_DEPTH_BYTES));

		unsigned bucketNumber = 1 << globalDepth;
		//The page is empty, initial the global directory
		if (globalDepth < GLOBAL_DEPTH_MIN) {
			globalDepth = GLOBAL_DEPTH_MIN;
			freePointer = 0;
			bucketNumber = 1 << globalDepth;
			for (unsigned i = 0; i < bucketNumber; ++i) {
				dir.push_back(globalDepth);
				freePointer += sizeof(InPageBucketHead);
				content[i];
			}
		} else {
			char temp[PAGE_SIZE];
			for (unsigned i = 0; i < bucketNumber; ++i) {
				unsigned bucketOffset = PAGE_SIZE - OVERFLOW_PAGEID_BYTES
						- FREE_POINTER_BYTES - GLOBAL_DEPTH_BYTES
						- INPAGE_DIR_ITEM_BYTES * (i + 1);
				unsigned headOffset = *((unsigned *) (page + bucketOffset));
				InPageBucketHead thisHead;
				memcpy(&thisHead, page + headOffset, sizeof(InPageBucketHead));
				dir.push_back(thisHead.localDepth);
				unsigned thisKey = (~(0xFFFFFFFF >> thisHead.localDepth
						<< thisHead.localDepth)) & i;
				if (content.find(thisKey) == content.end()) {
					std::list<ValueRIDPair> & thisList = content[thisKey];
					unsigned nextPointer = thisHead.nextPointer;
					for (unsigned j = 0; j < thisHead.itemNumber; ++j) {
						ValueRIDPair thisPair;
						unsigned thisValueLength = 0;
						if (attribute.type == TypeInt) {
							thisPair.intValue = *((int *) (page + nextPointer));
							thisValueLength = sizeof(int);
						} else if (attribute.type == TypeReal) {
							thisPair.realValue =
									*((float *) (page + nextPointer));
							thisValueLength = sizeof(float);
						} else {
							unsigned varCharLength = *((unsigned *) (page
									+ nextPointer));
							memcpy(temp, page + nextPointer + sizeof(unsigned),
									varCharLength);
							temp[varCharLength] = '\0';
							thisPair.varChar = temp;
							thisValueLength = sizeof(unsigned) + varCharLength;
						}
						memcpy(&(thisPair.rid),
								page + nextPointer + thisValueLength,
								sizeof(RID));
						thisList.push_back(thisPair);
						memcpy(&nextPointer,
								page + nextPointer + thisValueLength
										+ sizeof(RID), sizeof(unsigned));
					}
				}
			}
		}
	}

	unsigned getTotalItemNumber() {
		unsigned total = 0;
		std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator itor;
		for (itor = content.begin(); itor != content.end(); ++itor) {
			total += itor->second.size();
		}
		return total;
	}

	unsigned unusedSpaceSize() {
		return PAGE_SIZE - OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES
				- GLOBAL_DEPTH_BYTES
				- INPAGE_DIR_ITEM_BYTES * (1 << GLOBAL_DEPTH_MAX) - freePointer;
	}

	int insertPair(const void *key, const RID &rid) {

		unsigned requiredSize = sizeof(unsigned) + sizeof(RID); //the space required by the next pointer + RID
		if (attribute.type == TypeInt || attribute.type == TypeReal) {
			requiredSize += 4;
		} else {
			unsigned varCharLength = *((unsigned *) key);
			requiredSize += sizeof(unsigned) + varCharLength;
		}
		if (requiredSize > INDEX_ITEM_MAX_SIZE) {
			//Cannot insert this record
			return INSERT_INDEX_SIZE_EXCEEDS_LIMITS;
		}

		ValueRIDPair thisPair;
		thisPair.rid = rid;
		unsigned hashValue = 0;
		if (attribute.type == TypeInt) {
			thisPair.intValue = *((int *) key);
			hashValue = hashInt(thisPair.intValue);
		} else if (attribute.type == TypeReal) {
			thisPair.realValue = *((float *) key);
			hashValue = hashFloat(thisPair.realValue);
		} else {
			char temp[PAGE_SIZE];
			unsigned length = *((unsigned *) key);
			memcpy(temp, (char*)key + sizeof(unsigned), length);
			temp[length] = '\0';
			thisPair.varChar = temp;
			hashValue = hashString(thisPair.varChar);
		}

		unsigned thisBucketID = getMostSignificantBits(hashValue, globalDepth);
		unsigned thisLocalDepth = dir[thisBucketID];
		unsigned thisValidKey = (~(0xFFFFFFFF >> thisLocalDepth
				<< thisLocalDepth)) & thisBucketID;
//		unsigned thisListSize = content[thisValidKey].size();
		if (content[thisValidKey].size() < INPAGE_BUCKET_SIZE
				&& requiredSize < unusedSpaceSize()) {
			//is able to insert
			dirty = true;
			content[thisValidKey].push_back(thisPair);
			freePointer += requiredSize;
			return 0;
		} else if (requiredSize >= unusedSpaceSize()) {
			//the page doesn't have enough space to store this <key, RID> pair
			return INSERT_PAGE_HAS_NO_ENOUGH_SPACE;
		} else {
			//the extensible hash table needs to extend
			while (globalDepth <= GLOBAL_DEPTH_MAX
					&& content[thisValidKey].size() == INPAGE_BUCKET_SIZE) {
				while (thisLocalDepth < globalDepth
						&& content[thisValidKey].size() == INPAGE_BUCKET_SIZE) {
					unsigned total = dir.size();
					freePointer -= sizeof(InPageBucketHead);
					for (unsigned i = 0; i < total; ++i) {
						unsigned tempLocalDepth = dir[i];
						unsigned tempValidKey = (~(0xFFFFFFFF >> tempLocalDepth
								<< tempLocalDepth)) & i;
						if (thisValidKey == tempValidKey) {
							dir[i] = thisLocalDepth + 1;
							freePointer += sizeof(InPageBucketHead);
							content[(~(0xFFFFFFFF >> (thisLocalDepth + 1)
									<< (thisLocalDepth + 1))) & i];
						}
					}
					++thisLocalDepth;
					std::list<ValueRIDPair> originList = content[thisValidKey];
					content[thisValidKey].clear();
					for (std::list<ValueRIDPair>::iterator it =
							originList.begin(); it != originList.end(); ++it) {
						unsigned tempHashValue = 0;
						if (attribute.type == TypeInt) {
							tempHashValue = hashInt(it->intValue);
						} else if (attribute.type == TypeReal) {
							tempHashValue = hashFloat(it->realValue);
						} else {
							tempHashValue = hashString(it->varChar);
						}
						unsigned tempValidKey = getMostSignificantBits(
								tempHashValue, thisLocalDepth);
						std::list<ValueRIDPair> & tempList =
								content[tempValidKey];
						tempList.push_back(*it);
					}
				}
				thisBucketID = getMostSignificantBits(hashValue, globalDepth);
				thisLocalDepth = dir[thisBucketID];
				thisValidKey =
						(~(0xFFFFFFFF >> thisLocalDepth << thisLocalDepth))
								& thisBucketID;
				if (content[thisValidKey].size() < INPAGE_BUCKET_SIZE) {
					if (requiredSize < unusedSpaceSize()) {
						//is able to insert
						dirty = true;
						content[thisValidKey].push_back(thisPair);
						freePointer += requiredSize;
						return 0;
					} else {
						//the page doesn't have enough space to store this <key, RID> pair
						return INSERT_PAGE_HAS_NO_ENOUGH_SPACE;
					}
				} else {
					if (globalDepth < GLOBAL_DEPTH_MAX) {
						unsigned end = dir.size();
						for (unsigned i = 0; i < end; ++i) {
							dir.push_back(dir[i]);
						}
						++globalDepth;
					} else {
						break;
					}
				}
			}
			return INSERT_EH_HAS_NO_ENOUGH_SLOT;
		}
	}

	int writeInDiskFormat(char * page) {

		memset(page, 0, PAGE_SIZE);
		memcpy(page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES, &overflowPageID,
		OVERFLOW_PAGEID_BYTES);
		memcpy(page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES,
				&freePointer, FREE_POINTER_BYTES);
		memcpy(
				page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES - FREE_POINTER_BYTES
						- GLOBAL_DEPTH_BYTES, &globalDepth, GLOBAL_DEPTH_BYTES);

		std::unordered_map<unsigned, unsigned> key2Offset;

		char temp[PAGE_SIZE];
		unsigned thisFreePointer = 0;
		std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator itor;
		for (itor = content.begin(); itor != content.end(); ++itor) {
			InPageBucketHead thisHead;
			key2Offset[itor->first] = thisFreePointer;
			thisHead.localDepth = dir[itor->first];
			std::list<ValueRIDPair> & thisList = itor->second;
			thisHead.itemNumber = thisList.size();
			thisHead.nextPointer = 0;
			if (thisHead.itemNumber != 0) {
				thisHead.nextPointer = thisFreePointer
						+ sizeof(InPageBucketHead);
			}
			memcpy(page + thisFreePointer, &thisHead, sizeof(InPageBucketHead));
			thisFreePointer += sizeof(InPageBucketHead);

			while (thisList.size() > 0) {
				ValueRIDPair & thisPair = thisList.front();
				unsigned thisLength = 0;
				if (attribute.type == TypeInt) {
					memcpy(temp, &(thisPair.intValue), sizeof(unsigned));
					memcpy(temp + sizeof(unsigned), &(thisPair.rid),
							sizeof(RID));
					thisLength = sizeof(unsigned) + sizeof(RID);
				} else if (attribute.type == TypeReal) {
					memcpy(temp, &(thisPair.realValue), sizeof(float));
					memcpy(temp + sizeof(float), &(thisPair.rid), sizeof(RID));
					thisLength = sizeof(float) + sizeof(RID);
				} else {
					unsigned varCharLength = thisPair.varChar.length();
					memcpy(temp, &varCharLength, sizeof(unsigned));
					memcpy(temp + sizeof(unsigned), thisPair.varChar.c_str(),
							varCharLength);
					memcpy(temp + sizeof(unsigned) + varCharLength,
							&(thisPair.rid), sizeof(RID));
					thisLength = sizeof(unsigned) + varCharLength + sizeof(RID);
				}

				thisFreePointer += thisLength + sizeof(unsigned);
				if (thisList.size() > 1) {
					memcpy(temp + thisLength, &thisFreePointer,
							sizeof(unsigned));
				} else {
					memset(temp + thisLength, 0, sizeof(unsigned));
				}
				thisLength += sizeof(unsigned);
				memcpy(page + thisFreePointer - thisLength, temp, thisLength);

				thisList.pop_front();
			}
		}

		if (thisFreePointer > freePointer) {
			cout << "Real thisFreePointer: " << thisFreePointer
					<< "/ Write free pointer: " << freePointer << endl;
		}

		for (unsigned i = 0; i < dir.size(); ++i) {
			unsigned validKey = (~(0xFFFFFFFF >> dir[i] << dir[i])) & i;
			unsigned offset = key2Offset[validKey];
			memcpy(
					page + PAGE_SIZE - OVERFLOW_PAGEID_BYTES
							- FREE_POINTER_BYTES - GLOBAL_DEPTH_BYTES
							- (i + 1) * sizeof(unsigned), &offset,
					sizeof(unsigned));
		}
		return 0;
	}
};

class IndexManager {
public:
	static IndexManager* instance();

	bool detectIndex(const string & fileName){
		string primaryFilename = fileName + "_primary";
		string metadataFilename = fileName + "_metadata";
		ifstream fp(primaryFilename);
		ifstream fm(metadataFilename);
		if (!fp.good() || !fm.good()) {
			fp.close();
			fm.close();
			return false;
		} else {
			fp.close();
			fm.close();
			return true;
		}
	}

	// Create index file(s) to manage an index
	RC createFile(const string &fileName, const unsigned &numberOfPages);

	// Delete index file(s)
	RC destroyFile(const string &fileName);

	// Open an index and returns an IXFileHandle
	RC openFile(const string &fileName, IXFileHandle &ixFileHandle);

	// Close an IXFileHandle.
	RC closeFile(IXFileHandle &ixfileHandle);

	// The following functions are using the following format for the passed key value.
	//  1) data is a concatenation of values of the attributes
	//  2) For INT and REAL: use 4 bytes to store the value;
	//     For VarChar: use 4 bytes to store the length of characters, then store the actual characters.

	// Insert an entry to the given index that is indicated by the given IXFileHandle
	RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute,
			const void *key, const RID &rid);

	// Delete an entry from the given index that is indicated by the given IXFileHandle
	RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute,
			const void *key, const RID &rid);

	// scan() returns an iterator to allow the caller to go through the results
	// one by one in the range(lowKey, highKey).
	// For the format of "lowKey" and "highKey", please see insertEntry()
	// If lowKeyInclusive (or highKeyInclusive) is true, then lowKey (or highKey)
	// should be included in the scan
	// If lowKey is null, then the range is -infinity to highKey
	// If highKey is null, then the range is lowKey to +infinity

	// Initialize and IX_ScanIterator to supports a range search
	RC scan(IXFileHandle &ixfileHandle, const Attribute &attribute,
			const void *lowKey, const void *highKey, bool lowKeyInclusive,
			bool highKeyInclusive, IX_ScanIterator &ix_ScanIterator);

	// Generate and return the hash value (unsigned) for the given key
	unsigned hash(const Attribute &attribute, const void *key);

	// Return the total number of index entries in a primary page including associated overflow pages
	unsigned indexEntriesInAPage(IXFileHandle &ixfileHandle,
			const Attribute &attribute, const unsigned &primaryPageNumber);

	unsigned totalEntries(IXFileHandle &ixfileHandle,
			const Attribute &attribute);

	// Print all index entries in a primary page including associated overflow pages
	// Format should be:
	// Number of total entries in the page (+ overflow pages) : ??
	// primary Page No.??
	// # of entries : ??
	// entries: [xx] [xx] [xx] [xx] [xx] [xx]
	// overflow Page No.?? liked to [primary | overflow] page No.??
	// # of entries : ??
	// entries: [xx] [xx] [xx] [xx] [xx]
	// where [xx] shows each entry.
	RC printIndexEntriesInAPage(IXFileHandle &ixfileHandle,
			const Attribute &attribute, const unsigned &primaryPageNumber);

	// Get the number of primary pages
	RC getNumberOfPrimaryPages(IXFileHandle &ixfileHandle,
			unsigned &numberOfPrimaryPages);

	// Get the number of all pages (primary + overflow)
	RC getNumberOfAllPages(IXFileHandle &ixfileHandle,
			unsigned &numberOfAllPages);

protected:
	IndexManager();                            // Constructor
	~IndexManager();                            // Destructor

private:
	static IndexManager *_index_manager;
};

class IX_ScanIterator {
public:
	Attribute attribute;
	MultiTypeValue lowKey;
	MultiTypeValue highKey;
	bool lowKeyInclusive;
	bool highKeyInclusive;
	bool negativeInf;
	bool positiveInf;
	IXFileHandle * ptr_ixfileHandle;
	bool exactScan;
	char buffer[PAGE_SIZE];
	unsigned currentPrimaryPage;
	unsigned bufferPageID;
	bool primaryPage;	//Only used in iterate scan
	bool bufferValid;
	int EHBucketID;	// The bucket id
	unsigned nextEHPairOffset;	//Next EH Pair Offset
	bool finished;	//mark whether the iteration is over
	ExtendibleHash * cachedEH;	//Only used in iterate scan
	std::unordered_map<unsigned, std::list<ValueRIDPair>>::iterator contentItor;
	std::list<ValueRIDPair>::iterator listItor;
	unsigned LH_hashValue;	//Only used in exact scan
	unsigned EH_hashValue;	//Only used in exact scan
	int catchedInt;
	float catchedFloat;
	string catchedString;
	unsigned totalPrimaryPage;

	IX_ScanIterator();  							// Constructor
	~IX_ScanIterator(); 							// Destructor

	RC getNextEntry(RID &rid, void *key);  		// Get next matching entry
	RC close();             						// Terminate index scan

	bool satisfiedInt(int target) {
		if (!negativeInf && target < lowKey.intValue) {
			return false;
		}
		if (!positiveInf && target > highKey.intValue) {
			return false;
		}
		if (!negativeInf && target == lowKey.intValue && !lowKeyInclusive) {
			return false;
		}
		if (!positiveInf && target == highKey.intValue && !highKeyInclusive) {
			return false;
		}
		return true;
	}

	bool satisfiedFloat(float target) {
		if (!negativeInf && target < lowKey.floatValue) {
			return false;
		}
		if (!positiveInf && target > highKey.floatValue) {
			return false;
		}
		if (!negativeInf && target == lowKey.floatValue && !lowKeyInclusive) {
			return false;
		}
		if (!positiveInf && target == highKey.floatValue && !highKeyInclusive) {
			return false;
		}
		return true;
	}

	bool satisfiedString(string & target) {
		if (!negativeInf && target.compare(lowKey.strValue) < 0) {
			return false;
		}
		if (!positiveInf && target.compare(highKey.strValue) > 0) {
			return false;
		}
		if (!negativeInf && target.compare(lowKey.strValue) == 0
				&& !lowKeyInclusive) {
			return false;
		}
		if (!positiveInf && target.compare(highKey.strValue) == 0
				&& !highKeyInclusive) {
			return false;
		}
		return true;
	}

};

class IXFileHandle {
public:
	FileHandle primaryHandle, metadataHandle; // 2 FileHandles associated with two files

	// Put the current counter values of associated PF FileHandles into variables
	RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount,
			unsigned &appendPageCount);
	std::list<unsigned> reusableOverflowPages;
	unsigned Level;	// Initial value is 0, level number, when all the buckets are split, increas 1
	unsigned N;		// Initial size
	unsigned Next;	// Next bucket to be split

	bool valid;

	IXFileHandle();  							// Constructor
	~IXFileHandle(); 							// Destructor

private:
	unsigned readPageCounter;
	unsigned writePageCounter;
	unsigned appendPageCounter;
};

// print out the error message for a given return code
void IX_PrintError(RC rc);

#endif
