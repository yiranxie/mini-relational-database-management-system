
#include "qe.h"

Condition * createAndCopyCondition(const Condition & condition){
	Condition * pCondition = new Condition;
	pCondition->op = condition.op;
	pCondition->lhsAttr = condition.lhsAttr;
	pCondition->bRhsIsAttr = condition.bRhsIsAttr;
	if (pCondition->bRhsIsAttr){
		pCondition->rhsAttr = condition.rhsAttr;
	}else{
		pCondition->rhsAttr = "";
		pCondition->rhsValue.type = condition.rhsValue.type;
		pCondition->rhsValue.data = new char[PAGE_SIZE];
		if (condition.rhsValue.type == TypeInt){
			memcpy(pCondition->rhsValue.data, condition.rhsValue.data, sizeof(int));
		}else if(condition.rhsValue.type == TypeReal){
			memcpy(pCondition->rhsValue.data, condition.rhsValue.data, sizeof(float));
		}else{
			unsigned charLength;
			memcpy(&charLength, condition.rhsValue.data, sizeof(unsigned));
			memcpy(pCondition->rhsValue.data, condition.rhsValue.data, sizeof(unsigned)+charLength);
		}
	}
	return pCondition;
}

void deleteCondition(Condition * pCondition){
	if (pCondition != 0){
		if (!pCondition->bRhsIsAttr){
			delete (char*)pCondition->rhsValue.data;
		}
		delete pCondition;
	}
}

bool checkCondition(const MultiTypeValue &leftValue, const MultiTypeValue &rightValue, Condition * pCondition){
	bool result = false;
	if (leftValue.type == TypeInt){
		if (pCondition->op == EQ_OP){
			if (leftValue.intValue == rightValue.intValue){
				result = true;
			}
		}else if (pCondition->op == LT_OP){
			if (leftValue.intValue < rightValue.intValue){
				result = true;
			}
		}else if (pCondition->op == GT_OP){
			if (leftValue.intValue > rightValue.intValue){
				result = true;
			}
		}else if (pCondition->op == LE_OP){
			if (leftValue.intValue <= rightValue.intValue){
				result = true;
			}
		}else if (pCondition->op == GE_OP){
			if (leftValue.intValue >= rightValue.intValue){
				result = true;
			}
		}else if (pCondition->op == NE_OP){
			if (leftValue.intValue != rightValue.intValue){
				result = true;
			}
		}

	}else if (leftValue.type == TypeReal){
		if (pCondition->op == EQ_OP){
			if (leftValue.floatValue == rightValue.floatValue){
				result = true;
			}
		}else if (pCondition->op == LT_OP){
			if (leftValue.floatValue < rightValue.floatValue){
				result = true;
			}
		}else if (pCondition->op == GT_OP){
			if (leftValue.floatValue > rightValue.floatValue){
				result = true;
			}
		}else if (pCondition->op == LE_OP){
			if (leftValue.floatValue <= rightValue.floatValue){
				result = true;
			}
		}else if (pCondition->op == GE_OP){
			if (leftValue.floatValue >= rightValue.floatValue){
				result = true;
			}
		}else if (pCondition->op == NE_OP){
			if (leftValue.floatValue != rightValue.floatValue){
				result = true;
			}
		}
	}else{
		if (pCondition->op == EQ_OP){
			if (leftValue.strValue == rightValue.strValue){
				result = true;
			}
		}else if (pCondition->op == LT_OP){
			if (leftValue.strValue < rightValue.strValue){
				result = true;
			}
		}else if (pCondition->op == GT_OP){
			if (leftValue.strValue > rightValue.strValue){
				result = true;
			}
		}else if (pCondition->op == LE_OP){
			if (leftValue.strValue <= rightValue.strValue){
				result = true;
			}
		}else if (pCondition->op == GE_OP){
			if (leftValue.strValue >= rightValue.strValue){
				result = true;
			}
		}else if (pCondition->op == NE_OP){
			if (leftValue.strValue != rightValue.strValue){
				result = true;
			}
		}
	}
	return result;
}

/*
 * Implementation of Filter
 */
Filter::Filter(Iterator* input, const Condition &condition) {

	pItor = input;
	pCondition = createAndCopyCondition(condition);

	/*
	 * Assume the condition is correct
	 */
	input->getAttributes(attrs);
	conditionType = TypeInt;
	if (condition.op != NO_OP){
		for (unsigned i=0; i<attrs.size(); ++i){
			if (attrs[i].name == pCondition->lhsAttr){
				conditionType = attrs[i].type;
				break;
			}
		}
	}
}

RC Filter::getNextTuple(void *data) {
	char buffer[PAGE_SIZE];			//Buffer to store the data
	char temp[PAGE_SIZE];			//Temp space for string
	unsigned totalLength = 0;
//	bool findLeft = false, findRight = false;

	int rc;
	MultiTypeValue leftValue;
	MultiTypeValue rightValue;
	leftValue.type = conditionType;
	if (!pCondition->bRhsIsAttr){
		rightValue.type = pCondition->rhsValue.type;
		if (conditionType == TypeInt){
			memcpy(&rightValue.intValue, pCondition->rhsValue.data, sizeof(int));
		}else if (conditionType == TypeReal){
			memcpy(&rightValue.floatValue, pCondition->rhsValue.data, sizeof(float));
		}else{
			unsigned charLength;
			memcpy(&charLength, pCondition->rhsValue.data, sizeof(unsigned));
			memcpy(temp, (char*)pCondition->rhsValue.data+sizeof(unsigned), charLength);
			temp[charLength] = '\0';
			rightValue.strValue = temp;
		}
	}

	while (true){
		totalLength = 0;
		rc = pItor->getNextTuple(buffer);
		if (rc != 0){
			break;
		}

		//Get the value from the data
		for (unsigned i=0; i<attrs.size(); ++i){
			Attribute & thisAttribute = attrs[i];
			if (thisAttribute.name == pCondition->lhsAttr){
				if (conditionType == TypeInt){
					memcpy(&leftValue.intValue, buffer+totalLength, sizeof(int));
					totalLength += sizeof(int);
				}else if (conditionType == TypeReal){
					memcpy(&leftValue.floatValue, buffer+totalLength, sizeof(float));
					totalLength += sizeof(float);
				}else if (conditionType == TypeVarChar){
					unsigned charLength;
					memcpy(&charLength, buffer+totalLength, sizeof(unsigned));
					totalLength += sizeof(unsigned);
					memcpy(temp, buffer+totalLength, charLength);
					temp[charLength] = '\0';
					leftValue.strValue = temp;
					totalLength += charLength;
				}
				if (pCondition->bRhsIsAttr && (attrs[i].name == pCondition->rhsAttr)){
					rightValue = leftValue;
				}
			}else if (pCondition->bRhsIsAttr && thisAttribute.name == pCondition->rhsAttr){
				if (conditionType == TypeInt){
					memcpy(&rightValue.intValue, buffer+totalLength, sizeof(int));
					totalLength += sizeof(int);
				}else if (conditionType == TypeReal){
					memcpy(&rightValue.floatValue, buffer+totalLength, sizeof(float));
					totalLength += sizeof(float);
				}else if (conditionType == TypeVarChar){
					unsigned charLength;
					memcpy(&charLength, buffer+totalLength, sizeof(unsigned));
					totalLength += sizeof(unsigned);
					memcpy(temp, buffer+totalLength, charLength);
					temp[charLength] = '\0';
					rightValue.strValue = temp;
					totalLength += charLength;
				}
			}else{
				if (thisAttribute.type == TypeInt){
					totalLength += sizeof(int);
				}else if (thisAttribute.type == TypeReal){
					totalLength += sizeof(float);
				}else if (thisAttribute.type == TypeVarChar){
					unsigned charLength;
					memcpy(&charLength, buffer+totalLength, sizeof(unsigned));
					totalLength += sizeof(unsigned);
					totalLength += charLength;
				}
			}
		}

		/*
		 * Check whether the condition is satisfied
		 */
		if (pCondition->op == NO_OP || checkCondition(leftValue, rightValue, pCondition)){
			memcpy(data, buffer, totalLength);
			break;
		}
	}
	return rc;
}

/*
 * Implementation of Project
 */
Project::Project(Iterator *input,			// Iterator of input R
		const vector<string> &attrNames){	// vector containing attribute names
	pItor = input;
	input->getAttributes(allAttrs);
	for (unsigned i=0; i<attrNames.size(); ++i){
		for (unsigned j=0; j<allAttrs.size(); ++j){
			if (attrNames[i] == allAttrs[j].name){
				outputAttrs.push_back(allAttrs[j]);
				originOrders.push_back(j);
			}
		}
	}
}

RC Project::getNextTuple(void *data) {
	int rc;
	char buffer[PAGE_SIZE];
//	char temp[PAGE_SIZE];
	vector<unsigned> offsets;
	rc = pItor->getNextTuple(buffer);

	if (rc == 0){
		unsigned totalLength = 0;
		for (unsigned i=0; i<allAttrs.size(); ++i){
			offsets.push_back(totalLength);
			if (allAttrs[i].type == TypeInt){
				totalLength += sizeof(int);
			}else if (allAttrs[i].type == TypeReal){
				totalLength += sizeof(float);
			}else{
				unsigned strLength = 0;
				memcpy(&strLength, buffer+totalLength, sizeof(unsigned));
				totalLength += sizeof(unsigned) + strLength;
			}
		}

		totalLength = 0;
		char * pdata = (char *)data;
		for (unsigned i=0; i<outputAttrs.size(); ++i){
			unsigned thisOrder = originOrders[i];
			unsigned thisOffset = offsets[thisOrder];
			Attribute & thisAttr = outputAttrs[i];
			if (thisAttr.type == TypeInt){
				memcpy(pdata+totalLength, buffer+thisOffset, sizeof(int));
				totalLength += sizeof(int);
			}else if (thisAttr.type == TypeReal){
				memcpy(pdata+totalLength, buffer+thisOffset, sizeof(float));
				totalLength += sizeof(float);
			}else{
				unsigned strLength = 0;
				memcpy(&strLength, buffer+thisOffset, sizeof(unsigned));
				memcpy(pdata+totalLength, buffer+thisOffset, sizeof(unsigned) + strLength);
				totalLength += sizeof(unsigned) + strLength;
			}
		}
	}

	return rc;
}

/*
 * Implementation of Grace Hash Join
 */
GHJoin::GHJoin(Iterator *leftIn,		// Iterator of input R
      Iterator *rightIn,				// Iterator of input S
      const Condition &condition,		// Join condition (CompOp is always EQ)
      const unsigned numPartitions){	// # of partitions for each relation (decided by the optimizer)

	this->numPartitions = numPartitions;
	pCondition = createAndCopyCondition(condition);

	static unsigned nextID = 1;
	GHJoin_ID = nextID++;
	currentPartitionID = 0;
	leftIn->getAttributes(R_attrs);
	rightIn->getAttributes(S_attrs);
	getPartitionName(condition.lhsAttr, R_name);
	getPartitionName(condition.rhsAttr, S_name);
	for (unsigned i=0; i<R_attrs.size(); ++i){
		if (R_attrs[i].name == condition.lhsAttr){
			R_matchIndex = i;
		}
		R_attrNames.push_back(R_attrs[i].name);
		Output_attrs.push_back(R_attrs[i]);
	}
	for (unsigned i=0; i<S_attrs.size(); ++i){
		if (S_attrs[i].name == condition.rhsAttr){
			S_matchIndex = i;
		}
		S_attrNames.push_back(S_attrs[i].name);
		Output_attrs.push_back(S_attrs[i]);
	}

	/*
	 * Phase 1: Partition
	 * In the partitioning phase, create numPartitions partitions for each relation where each partition is an rbfm file.
	 * The name of the outer relation partitions must start with the word "left" while the name of the inner relation partitions must start with the word "right".
	 * In order to avoid conflicts in the file names (in the case of multiple GHJoins in the query tree) you will have to add a suffix that uniquely identify your partitions.
	 * For example, you can have something like left_join1_XX and right_join1_XX for the first join and left_join2_XX and right_join2_XX for the second join.
	 * Note: it is NOT acceptable to load the entire relation into memory while building the partitions.
	 */
	char buffer[PAGE_SIZE];
	char temp[PAGE_SIZE];
	FileHandle * fileHandles = new FileHandle[numPartitions];
	string * fileNames = new string[numPartitions];
	for (unsigned i=0; i<numPartitions; ++i){
		getPartitionFileName(GRACE_HJ_LEFT, i, R_name, fileNames[i]);
		RecordBasedFileManager::instance()->createFile(fileNames[i]);
		RecordBasedFileManager::instance()->openFile(fileNames[i], fileHandles[i]);
	}
	while (true){
		RC rc = leftIn->getNextTuple(buffer);
		if (rc != 0){
			break;
		}
		unsigned totalLength = 0;
		unsigned hashValue = 0;
		for (unsigned i=0; i<R_attrs.size(); ++i){
			Attribute & thisAttr = R_attrs[i];
			if (thisAttr.type == TypeInt){
				if (i == R_matchIndex){
					int intValue;
					memcpy(&intValue, buffer+totalLength, sizeof(int));
					hashValue = hashInt(intValue);
				}
				totalLength += sizeof(int);
			}else if (thisAttr.type == TypeReal){
				if (i == R_matchIndex){
					float floatValue;
					memcpy(&floatValue, buffer+totalLength, sizeof(float));
					hashValue = hashFloat(floatValue);
				}
				totalLength += sizeof(float);
			}else{
				unsigned charLength = 0;
				memcpy(&charLength, buffer+totalLength, sizeof(unsigned));
				totalLength += sizeof(unsigned);
				if (i == R_matchIndex){
					memcpy(temp, buffer+totalLength, charLength);
					temp[charLength] = '\0';
					string tempString = temp;
					hashValue = hashString(tempString);
				}
				totalLength += charLength;
			}
		}
		unsigned fileID = hashValue % numPartitions;
		RID thisRID;
		rc = RecordBasedFileManager::instance()->insertRecord(fileHandles[fileID], R_attrs, buffer, thisRID);
		assert (rc == 0);		//TODO when we submit the code, this code should be deleted!!!
		if (rc != 0){
			break;
		}
	}
	for (unsigned i=0; i<numPartitions; ++i){
		RecordBasedFileManager::instance()->closeFile(fileHandles[i]);
	}
	delete [] fileHandles;
	delete [] fileNames;

	fileHandles = new FileHandle[numPartitions];
	fileNames = new string[numPartitions];

	for (unsigned i=0; i<numPartitions; ++i){
		getPartitionFileName(GRACE_HJ_RIGHT, i, S_name, fileNames[i]);
		RecordBasedFileManager::instance()->createFile(fileNames[i]);
		RecordBasedFileManager::instance()->openFile(fileNames[i], fileHandles[i]);
	}
	while (true){
		RC rc = rightIn->getNextTuple(buffer);
		if (rc != 0){
			break;
		}
		unsigned totalLength = 0;
		unsigned hashValue = 0;
		for (unsigned i=0; i<S_attrs.size(); ++i){
			Attribute & thisAttr = S_attrs[i];
			if (thisAttr.type == TypeInt){
				if (i == S_matchIndex){
					int intValue;
					memcpy(&intValue, buffer+totalLength, sizeof(int));
					hashValue = hashInt(intValue);
				}
				totalLength += sizeof(int);
			}else if (thisAttr.type == TypeReal){
				if (i == S_matchIndex){
					float floatValue;
					memcpy(&floatValue, buffer+totalLength, sizeof(float));
					hashValue = hashFloat(floatValue);
				}
				totalLength += sizeof(float);
			}else{
				unsigned charLength = 0;
				memcpy(&charLength, buffer+totalLength, sizeof(unsigned));
				totalLength += sizeof(unsigned);
				if (i == S_matchIndex){
					memcpy(temp, buffer+totalLength, charLength);
					temp[charLength] = '\0';
					string tempString = temp;
					hashValue = hashString(tempString);
				}
				totalLength += charLength;
			}
		}
		unsigned fileID = hashValue % numPartitions;
		RID thisRID;
		rc = RecordBasedFileManager::instance()->insertRecord(fileHandles[fileID], S_attrs, buffer, thisRID);
		assert (rc == 0);		//TODO when we submit the code, this code should be deleted!!!
		if (rc != 0){
			break;
		}
	}
	for (unsigned i=0; i<numPartitions; ++i){
		RecordBasedFileManager::instance()->closeFile(fileHandles[i]);
	}
	delete [] fileHandles;
	delete [] fileNames;

	if (numPartitions > 0){
		string fileRName;
		getPartitionFileName(GRACE_HJ_LEFT, 0, R_name, fileRName);
		RecordBasedFileManager::instance()->openFile(fileRName, R_FileHandle);
		RecordBasedFileManager::instance()->scan(R_FileHandle, R_attrs, "", NO_OP, 0, R_attrNames, R_Itor);

		string fileSName;
		FileHandle tempFileHandle;
		RBFM_ScanIterator tempItor;
		getPartitionFileName(GRACE_HJ_RIGHT, 0, S_name, fileSName);
		RecordBasedFileManager::instance()->openFile(fileSName, tempFileHandle);
		RecordBasedFileManager::instance()->scan(tempFileHandle, S_attrs, "", NO_OP, 0, S_attrNames, tempItor);
		buildInMemoryHashTable(tempItor, S_matchIndex, S_HashTable);
		RecordBasedFileManager::instance()->closeFile(tempFileHandle);
	}
}

RC GHJoin::getNextTuple(void *data){
	/*
	 * Phase 2: Probe
	 * In the probing phase, load a partition of either R or S (in fact you might want to load the smaller partition*) into memory,
	 * then build an in-memory hash table for such partition. Next, probe the corresponding partition from the other relation for matching tuples.
	 */

	/*
	 * Phase 3: Output
	 * The output will be the join-tuples that must be passed to the next operator.
	 * The schema of these join-tuples should be the attributes of tuples from leftIn concatenated with the attributes of tuples from rightIn.
	 * You don't need to remove any duplicate attributes.
	 */

	if (bufferedOutput.size() > 0){
		FormattedRecord & thisRecord = bufferedOutput.front();
		generateDataInMemoryFormat((char *)data, PAGE_SIZE, thisRecord.values);
		bufferedOutput.pop_front();
		return 0;
	}

	if (currentPartitionID >= numPartitions){	//All the partitions are done!
		return QE_EOF;
	}


	char buffer[PAGE_SIZE];
	RID tempRID;
	bool findOutputItem = false;
	while (true){
		/*
		 * Get one record from R (on disk)
		 */
		RC rc = R_Itor.getNextRecord(tempRID, buffer);
		if (rc != 0){	//This partition pair is done!
			R_Itor.close();
			RecordBasedFileManager::instance()->closeFile(R_FileHandle);
			S_HashTable.clear();

			++ currentPartitionID;
			if (currentPartitionID >= numPartitions){
				break;
			}

			string fileRName;
			getPartitionFileName(GRACE_HJ_LEFT, currentPartitionID, R_name, fileRName);
			RecordBasedFileManager::instance()->openFile(fileRName, R_FileHandle);
			RecordBasedFileManager::instance()->scan(R_FileHandle, R_attrs, "", NO_OP, 0, R_attrNames, R_Itor);

			string fileSName;
			FileHandle tempFileHandle;
			RBFM_ScanIterator tempItor;
			getPartitionFileName(GRACE_HJ_RIGHT, currentPartitionID, S_name, fileSName);
			RecordBasedFileManager::instance()->openFile(fileSName, tempFileHandle);
			RecordBasedFileManager::instance()->scan(tempFileHandle, S_attrs, "", NO_OP, 0, S_attrNames, tempItor);
			buildInMemoryHashTable(tempItor, S_matchIndex, S_HashTable);
			RecordBasedFileManager::instance()->closeFile(tempFileHandle);
			continue;
		}

		FormattedRecord leftRecord;
		readRecordFromRawData(buffer, R_attrs, leftRecord);

		unsigned hashValue = 0;
		Attribute & leftAttr = R_attrs[R_matchIndex];
		if (leftAttr.type == TypeInt){
			hashValue = hashInt_2(leftRecord.values[R_matchIndex].intValue);
		}else if (leftAttr.type == TypeReal){
			hashValue = hashFloat_2(leftRecord.values[R_matchIndex].floatValue);
		}else{
			hashValue = hashString_2(leftRecord.values[R_matchIndex].strValue);
		}
		unordered_map<unsigned, vector<FormattedRecord>>::iterator mapItor = S_HashTable.find(hashValue);
		if (mapItor != S_HashTable.end()){
			vector<FormattedRecord> & records = mapItor->second;
			/*
			 * Find the match record in S
			 */
			for (unsigned i=0; i<records.size(); ++i){
				bool leftMatchRight = false;
				FormattedRecord & rightRecord = records[i];
				if (leftAttr.type == TypeInt){
					if (leftRecord.values[R_matchIndex].intValue == rightRecord.values[S_matchIndex].intValue){
						leftMatchRight = true;
					}
				}else if (leftAttr.type == TypeReal){
					if (leftRecord.values[R_matchIndex].floatValue == rightRecord.values[S_matchIndex].floatValue){
						leftMatchRight = true;
					}
				}else{
					if (leftRecord.values[R_matchIndex].strValue == rightRecord.values[S_matchIndex].strValue){
						leftMatchRight = true;
					}
				}
				if (leftMatchRight){
					bufferedOutput.resize(bufferedOutput.size() + 1);
					FormattedRecord & newJoinResultItem = bufferedOutput.back();
					newJoinResultItem.values.insert(newJoinResultItem.values.begin(), leftRecord.values.begin(), leftRecord.values.end());
					newJoinResultItem.values.insert(newJoinResultItem.values.end(), rightRecord.values.begin(), rightRecord.values.end());
				}
			}
			if (bufferedOutput.size() > 0){
				FormattedRecord & thisRecord = bufferedOutput.front();
				generateDataInMemoryFormat((char *)data, PAGE_SIZE, thisRecord.values);
				bufferedOutput.pop_front();
				findOutputItem = true;
				break;
			}
		}

	}

	if (findOutputItem){
		return 0;
	}

	string R_filename;
	string S_filename;
	for (unsigned i = 0; i < numPartitions; ++i) {
		getPartitionFileName(GRACE_HJ_LEFT, i, R_name, R_filename);
		RecordBasedFileManager::instance()->destroyFile(R_filename);
		getPartitionFileName(GRACE_HJ_RIGHT, i, S_name, S_filename);
		RecordBasedFileManager::instance()->destroyFile(S_filename);
	}

	return QE_EOF;
}

/*
 * Implementation of BNLJoin
 */
BNLJoin::BNLJoin(Iterator *leftIn,            // Iterator of input R
               TableScan *rightIn,           // TableScan Iterator of input S
               const Condition &condition,   // Join condition
               const unsigned numRecords     // # of records can be loaded into memory, i.e., memory block size (decided by the optimizer)
){
	R_Itor = leftIn;
	S_Itor = rightIn;
	R_finished = false;
	pCondition = createAndCopyCondition(condition);
	this->numRecords = numRecords;
	leftIn->getAttributes(R_attrs);
	rightIn->getAttributes(S_attrs);
	for (unsigned i=0; i<R_attrs.size(); ++i){
		if (R_attrs[i].name == condition.lhsAttr){
			R_matchIndex = i;
		}
		Output_attrs.push_back(R_attrs[i]);
	}
	for (unsigned i=0; i<S_attrs.size(); ++i){
		if (S_attrs[i].name == condition.rhsAttr){
			S_matchIndex = i;
		}
		Output_attrs.push_back(S_attrs[i]);
	}

	//initialize the inMemHashTable
	char buffer[PAGE_SIZE];
	Attribute & R_matchAttr = R_attrs[R_matchIndex];
	for (unsigned i=0; i<numRecords; ++i){
		RC rc = leftIn->getNextTuple(buffer);
		if (rc != 0){
			R_finished = true;
			break;
		}
		FormattedRecord thisTuple;
		readRecordFromRawData(buffer, R_attrs, thisTuple);
		if (R_matchAttr.type == TypeInt){
			unsigned hashValue = hashInt(thisTuple.values[R_matchIndex].intValue);
			inMemHashTable[hashValue] = thisTuple;
		}else if (R_matchAttr.type == TypeReal){
			unsigned hashValue = hashFloat(thisTuple.values[R_matchIndex].floatValue);
			inMemHashTable[hashValue] = thisTuple;
		}else{
			unsigned hashValue = hashString(thisTuple.values[R_matchIndex].strValue);
			inMemHashTable[hashValue] = thisTuple;
		}
	}

	RC rc = S_Itor->getNextTuple(buffer);
	if (rc != 0){	//Corner case, if S is empty.
		cached_HashTable_Itor = inMemHashTable.end();
		R_finished = true;
	}else{
		readRecordFromRawData(buffer, S_attrs, cached_S_Tuple);
		Attribute & S_matchAttr = S_attrs[S_matchIndex];
		unsigned hashValue = 0;
		if (S_matchAttr.type == TypeInt){
			hashValue = hashInt(cached_S_Tuple.values[S_matchIndex].intValue);
		}else if (S_matchAttr.type == TypeReal){
			hashValue = hashFloat(cached_S_Tuple.values[S_matchIndex].floatValue);
		}else{
			hashValue = hashString(cached_S_Tuple.values[S_matchIndex].strValue);
		}
		cached_HashTable_Itor = inMemHashTable.find(hashValue);
	}
};

RC BNLJoin::getNextTuple(void *data){
	char buffer[PAGE_SIZE];
	Attribute & R_matchAttr = R_attrs[R_matchIndex];
	Attribute & S_matchAttr = S_attrs[S_matchIndex];
	bool findMatch = false;
	while (true){
		//Try to find match
		for ( ;cached_HashTable_Itor != inMemHashTable.end(); ++cached_HashTable_Itor){
			FormattedRecord & R_Tuple = cached_HashTable_Itor->second;
			if (R_matchAttr.type == TypeInt){
				if (R_Tuple.values[R_matchIndex].intValue == cached_S_Tuple.values[S_matchIndex].intValue){
					//Find match
					findMatch = true;
					vector<MultiTypeValue> values = R_Tuple.values;
					values.insert(values.end(), cached_S_Tuple.values.begin(), cached_S_Tuple.values.end());
					generateDataInMemoryFormat((char*)data, PAGE_SIZE, values);
					++cached_HashTable_Itor;
					break;
				}
			}else if (R_matchAttr.type == TypeReal){
				if (R_Tuple.values[R_matchIndex].floatValue == cached_S_Tuple.values[S_matchIndex].floatValue){
					//Find match
					findMatch = true;
					vector<MultiTypeValue> values = R_Tuple.values;
					values.insert(values.end(), cached_S_Tuple.values.begin(), cached_S_Tuple.values.end());
					generateDataInMemoryFormat((char*)data, PAGE_SIZE, values);
					++cached_HashTable_Itor;
					break;
				}
			}else{
				if (R_Tuple.values[R_matchIndex].strValue == cached_S_Tuple.values[S_matchIndex].strValue){
					//Find match
					findMatch = true;
					vector<MultiTypeValue> values = R_Tuple.values;
					values.insert(values.end(), cached_S_Tuple.values.begin(), cached_S_Tuple.values.end());
					generateDataInMemoryFormat((char*)data, PAGE_SIZE, values);
					++cached_HashTable_Itor;
					break;
				}
			}
		}
		if (findMatch){
			break;
		}

		//If the cached S_Tuple cannot find any match, process the next Tuple in S
		RC rc = S_Itor->getNextTuple(buffer);
		if (rc != 0){				//We finish one scan of S
			inMemHashTable.clear();
			cached_HashTable_Itor = inMemHashTable.end();
			if (R_finished){		//We finish the scan of R
				return QE_EOF;
			}else{
				//reset the S iterator
				S_Itor->setIterator();
				//update the inMemHashTable
				for (unsigned i=0; i<numRecords; ++i){
					rc = R_Itor->getNextTuple(buffer);
					if (rc != 0){
						R_finished = true;
						break;
					}
					FormattedRecord R_Tuple;
					readRecordFromRawData(buffer, R_attrs, R_Tuple);
					if (R_matchAttr.type == TypeInt){
						unsigned hashValue = hashInt(R_Tuple.values[R_matchIndex].intValue);
						inMemHashTable[hashValue] = R_Tuple;
					}else if (R_matchAttr.type == TypeReal){
						unsigned hashValue = hashFloat(R_Tuple.values[R_matchIndex].floatValue);
						inMemHashTable[hashValue] = R_Tuple;
					}else{
						unsigned hashValue = hashString(R_Tuple.values[R_matchIndex].strValue);
						inMemHashTable[hashValue] = R_Tuple;
					}
				}

			}
			//to reload the S_Tuple
			continue;
		}
		readRecordFromRawData(buffer, S_attrs, cached_S_Tuple);
		unsigned hashValue = 0;
		if (S_matchAttr.type == TypeInt){
			hashValue = hashInt(cached_S_Tuple.values[S_matchIndex].intValue);
		}else if (S_matchAttr.type == TypeReal){
			hashValue = hashFloat(cached_S_Tuple.values[S_matchIndex].floatValue);
		}else{
			hashValue = hashString(cached_S_Tuple.values[S_matchIndex].strValue);
		}
		cached_HashTable_Itor = inMemHashTable.find(hashValue);
	}

	if (findMatch){
		return 0;
	}
	return QE_EOF;
};

/*
 * Implementation of INLJoin
 */
INLJoin::INLJoin(Iterator *leftIn,           // Iterator of input R
       IndexScan *rightIn,          // IndexScan Iterator of input S
       const Condition &condition   // Join condition
){
	R_Itor = leftIn;
	S_Itor = rightIn;
	pCondition = createAndCopyCondition(condition);
	R_finished = false;
	leftIn->getAttributes(R_attrs);
	rightIn->getAttributes(S_attrs);
	for (unsigned i=0; i<R_attrs.size(); ++i){
		if (R_attrs[i].name == condition.lhsAttr){
			R_matchIndex = i;
		}
		Output_attrs.push_back(R_attrs[i]);
	}
	for (unsigned i=0; i<S_attrs.size(); ++i){
		if (S_attrs[i].name == condition.rhsAttr){
			S_matchIndex = i;
		}
		Output_attrs.push_back(S_attrs[i]);
	}

	//Initialize cached R tuple
	char buffer[PAGE_SIZE];
	RC rc = R_Itor->getNextTuple(buffer);
	if (rc != 0){
		R_finished = true;
		//Initialize S_Itor
		S_valid = false;
	}else{
		//update the cached R Tuple
		readRecordFromRawData(buffer, R_attrs, cached_R_Tuple);
		MultiTypeValue & R_matchValue = cached_R_Tuple.values[R_matchIndex];
		if (R_matchValue.type == TypeInt){
			memcpy(key, &R_matchValue.intValue, sizeof(int));
		}else if (R_matchValue.type == TypeReal){
			memcpy(key, &R_matchValue.floatValue, sizeof(float));
		}else {
			unsigned charLength = R_matchValue.strValue.size();
			memcpy(key, &charLength, sizeof(unsigned));
			memcpy(key+sizeof(unsigned), R_matchValue.strValue.c_str(), charLength);
		}

		//Initialize S_Itor
		S_Itor->setIterator(key, key, true, true);
		S_valid = true;
	}
};

RC INLJoin::getNextTuple(void *data){

	if (R_finished && !S_valid){
		return QE_EOF;
	}

	char buffer[PAGE_SIZE];
	while (true){
		RC rc = S_Itor->getNextTuple(buffer);
		if (rc != 0){
			//No match in S, so we need to get next R
			RC rc = R_Itor->getNextTuple(buffer);
			if (rc != 0){
				//No more tuple in R
				R_finished = true;
				S_valid = false;
				return QE_EOF;
			}
			//Update cached R Tuple, process the next one
			readRecordFromRawData(buffer, R_attrs, cached_R_Tuple);
			MultiTypeValue & R_matchValue = cached_R_Tuple.values[R_matchIndex];
			if (R_matchValue.type == TypeInt){
				memcpy(key, &R_matchValue.intValue, sizeof(int));
			}else if (R_matchValue.type == TypeReal){
				memcpy(key, &R_matchValue.floatValue, sizeof(float));
			}else {
				unsigned charLength = R_matchValue.strValue.size();
				memcpy(key, &charLength, sizeof(unsigned));
				memcpy(key+sizeof(unsigned), R_matchValue.strValue.c_str(), charLength);
			}

			//Reset the S Iterator
			S_Itor->setIterator(key, key, true, true);
			S_valid = true;
			continue;
		}
		break;
	}
	FormattedRecord S_Tuple;
	readRecordFromRawData(buffer, S_attrs, S_Tuple);
	vector<MultiTypeValue> values = cached_R_Tuple.values;
	values.insert(values.end(), S_Tuple.values.begin(), S_Tuple.values.end());
	generateDataInMemoryFormat((char*)data, PAGE_SIZE, values);
	return 0;
};

/*
 * Implementation of Aggregation
 */
// Basic aggregation
Aggregate::Aggregate(Iterator *input,          // Iterator of input R
          Attribute aggAttr,        // The attribute over which we are computing an aggregate
          AggregateOp op            // Aggregate operation
){
	basic = true;
	resultReturned = false;
	this->op = op;
	basicValue.intValue = 0;
	basicValue.floatValue = 0.0;
	basicValue.strValue = "";
	this->aggAttr = aggAttr;
	groupIndex = 0;
	numPartitions = 0;
	aggIndex = 0;
	currentPartitionID = 0;
	GroupByAggregateID = 0;

	input->getAttributes(attrs);
	for (unsigned i=0; i<attrs.size(); ++i){
		if (attrs[i].name == aggAttr.name){
			aggIndex = i;
			break;
		}
	}

	char buffer[PAGE_SIZE];
	isInitialized = false;
	FormattedRecord thisTuple;
	unsigned count = 0;
	while (true){
		RC rc = input->getNextTuple(buffer);
		if (rc != 0){	//No more results
			break;
		}
		readRecordFromRawData(buffer, attrs, thisTuple);
		++ count;
		if (!isInitialized){
			if (aggAttr.type == TypeInt){
				basicValue.type = TypeInt;
				basicValue.intValue = thisTuple.values[aggIndex].intValue;
			}else if (aggAttr.type == TypeReal){
				basicValue.type = TypeReal;
				basicValue.floatValue = thisTuple.values[aggIndex].floatValue;
			}else{
				//We can assume we do the aggregation on a numeric attribute (INT or REAL)
			}

			isInitialized = true;
			continue;
		}
		if (op == MIN){
			if (aggAttr.type == TypeInt){
				if (basicValue.intValue > thisTuple.values[aggIndex].intValue){
					basicValue.intValue = thisTuple.values[aggIndex].intValue;
				}
			}else if (aggAttr.type == TypeReal){
				if (basicValue.floatValue > thisTuple.values[aggIndex].floatValue){
					basicValue.floatValue = thisTuple.values[aggIndex].floatValue;
				}
			}else{
				//We can assume we do the aggregation on a numeric attribute (INT or REAL)
			}
		}else if (op == MAX){
			if (aggAttr.type == TypeInt){
				if (basicValue.intValue < thisTuple.values[aggIndex].intValue){
					basicValue.intValue = thisTuple.values[aggIndex].intValue;
				}
			}else if (aggAttr.type == TypeReal){
				if (basicValue.floatValue < thisTuple.values[aggIndex].floatValue){
					basicValue.floatValue = thisTuple.values[aggIndex].floatValue;
				}
			}else{
				//We can assume we do the aggregation on a numeric attribute (INT or REAL)
			}
		}else if (op == SUM){
			if (aggAttr.type == TypeInt){
				basicValue.intValue += thisTuple.values[aggIndex].intValue;
			}else if (aggAttr.type == TypeReal){
				basicValue.floatValue += thisTuple.values[aggIndex].floatValue;
			}else{
				//We can assume we do the aggregation on a numeric attribute (INT or REAL)
			}
		}else if (op == AVG){	//First sum them together
			if (aggAttr.type == TypeInt){
				basicValue.intValue += thisTuple.values[aggIndex].intValue;
			}else if (aggAttr.type == TypeReal){
				basicValue.floatValue += thisTuple.values[aggIndex].floatValue;
			}else{
				//We can assume we do the aggregation on a numeric attribute (INT or REAL)
			}
		}else if (op == COUNT){
			//Don't need to do anything
		}
	}

	if (isInitialized){
		if (op == AVG){		//We use float to store the AVG
			if (aggAttr.type == TypeInt){
				basicValue.floatValue = ((double)(basicValue.intValue)) / (double)count;
			}else if (aggAttr.type == TypeReal){
				basicValue.floatValue = thisTuple.values[aggIndex].floatValue / (float)count;
			}else{
				//We can assume we do the aggregation on a numeric attribute (INT or REAL)
			}
		}
	}

	if (op == COUNT){
		if (op == COUNT){ //We use integer to store the COUNT
			basicValue.intValue = count;
		}
	}

};

// Group-based hash aggregation
Aggregate::Aggregate(Iterator *input,             // Iterator of input R
          Attribute aggAttr,           // The attribute over which we are computing an aggregate
          Attribute groupAttr,         // The attribute over which we are grouping the tuples
          AggregateOp op,              // Aggregate operation
          const unsigned numPartitions // Number of partitions for input (decided by the optimizer)
){
	basic = false;
	resultReturned = false;
	this->op = op;
	this->numPartitions = numPartitions;
	this->aggAttr = aggAttr;
	aggIndex = 0;
	this->groupAttr = groupAttr;
	groupIndex = 0;
	partitionAttrs.push_back(groupAttr);
	partitionAttrs.push_back(aggAttr);
	partitionAttrNames.push_back(groupAttr.name);
	partitionAttrNames.push_back(aggAttr.name);
	currentPartitionID = 0;
	static unsigned nextID = 1;
	GroupByAggregateID = nextID++;


	input->getAttributes(attrs);
	for (unsigned i=0; i<attrs.size(); ++i){
		if (attrs[i].name == aggAttr.name){
			aggIndex = i;
		}
		if (attrs[i].name == groupAttr.name){
			groupIndex = i;
		}
	}

	isInitialized = false;

	/*
	 * Phase 1: Partition for Group By
	 */
	char buffer[PAGE_SIZE];
	FileHandle * fileHandles = new FileHandle[numPartitions];
	string * fileNames = new string[numPartitions];
	for (unsigned i=0; i<numPartitions; ++i){
		getPartitionFileName(i, groupAttr.name, aggAttr.name, fileNames[i]);
		RecordBasedFileManager::instance()->createFile(fileNames[i]);
		RecordBasedFileManager::instance()->openFile(fileNames[i], fileHandles[i]);
	}
	//In each partition file, we only need to keep two attributes for each record
	while (true){
		RC rc = input->getNextTuple(buffer);
		if (rc != 0){
			//All the original data is processed
			break;
		}
//		unsigned totalLength = 0;
		unsigned hashValue = 0;
		FormattedRecord thisTuple;
		readRecordFromRawData(buffer, attrs, thisTuple);
		if (groupAttr.type == TypeInt){
			hashValue = hashInt(thisTuple.values[groupIndex].intValue);
		}else if (groupAttr.type == TypeReal){
			hashValue = hashFloat(thisTuple.values[groupIndex].floatValue);
		}else {
			hashValue = hashString(thisTuple.values[groupIndex].strValue);
		}
		unsigned fileID = hashValue % numPartitions;
		RID thisRID;
		vector<MultiTypeValue> onDiskValues;
		onDiskValues.push_back(thisTuple.values[groupIndex]);
		onDiskValues.push_back(thisTuple.values[aggIndex]);
		generateDataInMemoryFormat(buffer, PAGE_SIZE, onDiskValues);
		rc = RecordBasedFileManager::instance()->insertRecord(fileHandles[fileID], partitionAttrs, buffer, thisRID);
		assert (rc == 0);		//TODO when we submit the code, this code should be deleted!!!
		if (rc != 0){
			break;
		}
	}
	for (unsigned i=0; i<numPartitions; ++i){
		RecordBasedFileManager::instance()->closeFile(fileHandles[i]);
	}
	delete [] fileHandles;
	delete [] fileNames;
};

Aggregate::~Aggregate(){
	//delete all the temp files
//	if (!basic) {
//		for (unsigned i=0; i<numPartitions; ++i){
//			string thisFilename;
//			getPartitionFileName(i, groupAttr.name, aggAttr.name, thisFilename);
//			RecordBasedFileManager::instance()->destroyFile(thisFilename);
//		}
//	}
};

RC Aggregate::getNextTuple(void *data){

	if (basic){
		//Basic version
		if (resultReturned){
			return QE_EOF;
		}

		//The returned value is just a single real value (4 bytes)
//		float result = 0.0;
		if (isInitialized){
			if (op == MIN){
				if (aggAttr.type == TypeInt){
					memcpy(data, &basicValue.intValue, sizeof(int));
				}else if (aggAttr.type == TypeReal){
					memcpy(data, &basicValue.floatValue, sizeof(float));
				}else{
					//We can assume we do the aggregation on a numeric attribute (INT or REAL)
				}
			}else if (op == MAX){
				if (aggAttr.type == TypeInt){
					memcpy(data, &basicValue.intValue, sizeof(int));
				}else if (aggAttr.type == TypeReal){
					memcpy(data, &basicValue.floatValue, sizeof(float));
				}else{
					//We can assume we do the aggregation on a numeric attribute (INT or REAL)
				}
			}else if (op == SUM){
				if (aggAttr.type == TypeInt){
					memcpy(data, &basicValue.intValue, sizeof(int));
				}else if (aggAttr.type == TypeReal){
					memcpy(data, &basicValue.floatValue, sizeof(float));
				}else{
					//We can assume we do the aggregation on a numeric attribute (INT or REAL)
				}
			}else if (op == AVG){	//First sum them together
				if (aggAttr.type == TypeInt){
					memcpy(data, &basicValue.floatValue, sizeof(float));
				}else if (aggAttr.type == TypeReal){
					memcpy(data, &basicValue.floatValue, sizeof(float));
				}else{
					//We can assume we do the aggregation on a numeric attribute (INT or REAL)
				}
			}else if (op == COUNT){
				memcpy(data, &basicValue.intValue, sizeof(int));
			}
		}
		if (!isInitialized && op != COUNT && op != SUM){
			float result = 0.0 / 0.0;
			memcpy(data, &result, sizeof(float));
		}
		resultReturned = true;
		return 0;
	}else{

		while (true){
			if (!bufferedResults.empty()){
				FormattedRecord & thisResult = bufferedResults.front();
				generateDataInMemoryFormat((char*)data, PAGE_SIZE, thisResult.values);
				bufferedResults.pop_front();
				return 0;
			}

			//Initialize the in-memory hash map
			if (currentPartitionID < numPartitions){
				string thisFileName;
				getPartitionFileName(currentPartitionID, groupAttr.name, aggAttr.name, thisFileName);
				FileHandle thisFileHandle;
				RBFM_ScanIterator rbfmItor;
				RecordBasedFileManager::instance()->openFile(thisFileName, thisFileHandle);
				RecordBasedFileManager::instance()->scan(thisFileHandle, partitionAttrs, "", NO_OP, 0, partitionAttrNames, rbfmItor);
				if (groupAttr.type == TypeInt){
					if (aggAttr.type == TypeInt){
						unordered_map<int, int> inMemHashTable;
						unordered_map<int, int> countTable;
						buildInMemHashTable(rbfmItor, inMemHashTable, countTable);
						for (unordered_map<int, int>::iterator inMemItor=inMemHashTable.begin(); inMemItor!=inMemHashTable.end(); ++inMemItor){
							if (op != AVG){
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeInt;
								newResult.values[0].intValue = inMemItor->first;
								newResult.values[1].type = TypeInt;
								newResult.values[1].intValue = inMemItor->second;
							}else{
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeInt;
								newResult.values[0].intValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = (double)inMemItor->second / countTable[inMemItor->first];
							}
						}
					}else{
						unordered_map<int, float> inMemHashTable;
						unordered_map<int, int> countTable;
						buildInMemHashTable(rbfmItor, inMemHashTable, countTable);
						for (unordered_map<int, float>::iterator inMemItor=inMemHashTable.begin(); inMemItor!=inMemHashTable.end(); ++inMemItor){
							if (op != AVG){
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeInt;
								newResult.values[0].intValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = inMemItor->second;
							}else{
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeInt;
								newResult.values[0].intValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = inMemItor->second / countTable[inMemItor->first];
							}
						}
					}

				}else if (groupAttr.type == TypeReal){
					if (aggAttr.type == TypeInt){
						unordered_map<float, int> inMemHashTable;
						unordered_map<float, int> countTable;
						buildInMemHashTable(rbfmItor, inMemHashTable, countTable);
						for (unordered_map<float, int>::iterator inMemItor=inMemHashTable.begin(); inMemItor!=inMemHashTable.end(); ++inMemItor){
						if (op != AVG){
							bufferedResults.resize(bufferedResults.size() + 1);
							FormattedRecord & newResult = bufferedResults.back();
							newResult.values.resize(2);
							newResult.values[0].type = TypeReal;
							newResult.values[0].floatValue = inMemItor->first;
							newResult.values[1].type = TypeInt;
							newResult.values[1].intValue = inMemItor->second;
						}else{
							bufferedResults.resize(bufferedResults.size() + 1);
							FormattedRecord & newResult = bufferedResults.back();
							newResult.values.resize(2);
							newResult.values[0].type = TypeReal;
							newResult.values[0].floatValue = inMemItor->first;
							newResult.values[1].type = TypeReal;
							newResult.values[1].floatValue = (double)inMemItor->second / countTable[inMemItor->first];
						}
						}
					}else{
						unordered_map<float, float> inMemHashTable;
						unordered_map<float, int> countTable;
						buildInMemHashTable(rbfmItor, inMemHashTable, countTable);
						for (unordered_map<float, float>::iterator inMemItor=inMemHashTable.begin(); inMemItor!=inMemHashTable.end(); ++inMemItor){
							if (op != AVG){
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeReal;
								newResult.values[0].floatValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = inMemItor->second;
							}else{
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeReal;
								newResult.values[0].floatValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = inMemItor->second / countTable[inMemItor->first];
							}
						}
					}
				}else if (groupAttr.type == TypeVarChar){
					if (aggAttr.type == TypeInt){
						unordered_map<string, int> inMemHashTable;
						unordered_map<string, int> countTable;
						buildInMemHashTable(rbfmItor, inMemHashTable, countTable);
						for (unordered_map<string, int>::iterator inMemItor=inMemHashTable.begin(); inMemItor!=inMemHashTable.end(); ++inMemItor){
							if (op != AVG){
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeVarChar;
								newResult.values[0].strValue = inMemItor->first;
								newResult.values[1].type = TypeInt;
								newResult.values[1].intValue = inMemItor->second;
							}else{
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeVarChar;
								newResult.values[0].strValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = (double)inMemItor->second / countTable[inMemItor->first];
							}
						}
					}else{
						unordered_map<string, float> inMemHashTable;
						unordered_map<string, int> countTable;
						buildInMemHashTable(rbfmItor, inMemHashTable, countTable);
						for (unordered_map<string, float>::iterator inMemItor=inMemHashTable.begin(); inMemItor!=inMemHashTable.end(); ++inMemItor){
							if (op != AVG){
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeVarChar;
								newResult.values[0].strValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = inMemItor->second;
							}else{
								bufferedResults.resize(bufferedResults.size() + 1);
								FormattedRecord & newResult = bufferedResults.back();
								newResult.values.resize(2);
								newResult.values[0].type = TypeVarChar;
								newResult.values[0].strValue = inMemItor->first;
								newResult.values[1].type = TypeReal;
								newResult.values[1].floatValue = inMemItor->second / countTable[inMemItor->first];
							}
						}
					}
				}
				RecordBasedFileManager::instance()->closeFile(thisFileHandle);
				RecordBasedFileManager::instance()->destroyFile(thisFileName);
				++ currentPartitionID;
				continue;
			}else{
				return QE_EOF;
			}
		}
		return QE_EOF;
	}

};

void Aggregate::getAttributes(vector<Attribute> &attrs) const{
	if (basic){
		//Basic version
		attrs.clear();
		attrs.resize(1);
		Attribute & thisAttribute = attrs[0];
		thisAttribute.type = TypeReal;
		thisAttribute.length = sizeof(float);
//		if (op == MIN){
//			thisAttribute.name = "MIN(" + aggAttr.name + ")";
//		}else if (op == MAX){
//			thisAttribute.name = "MAX(" + aggAttr.name + ")";
//		}else if (op == SUM){
//			thisAttribute.name = "SUM(" + aggAttr.name + ")";
//		}else if (op == AVG){	//First sum them together
//			thisAttribute.name = "AVG(" + aggAttr.name + ")";
//		}else if (op == COUNT){
//			thisAttribute.name = "COUNT(" + aggAttr.name + ")";
//		}
		if (op == MIN){
			thisAttribute.name = "MIN()";
			if (this->aggAttr.type == TypeInt){
				thisAttribute.type = TypeInt;
			}
		}else if (op == MAX){
			thisAttribute.name = "MAX()";
			if (this->aggAttr.type == TypeInt){
				thisAttribute.type = TypeInt;
			}
		}else if (op == SUM){
			thisAttribute.name = "SUM()";
			if (this->aggAttr.type == TypeInt){
				thisAttribute.type = TypeInt;
			}
		}else if (op == AVG){	//First sum them together
			thisAttribute.name = "AVG()";
		}else if (op == COUNT){
			thisAttribute.name = "COUNT()";
			thisAttribute.type = TypeInt;
		}

		//If the table is empty, corner cases
		if (!isInitialized && op != COUNT && op != SUM){
			thisAttribute.type = TypeReal;
		}
	}else{
		attrs.clear();
		attrs.resize(2);
		attrs[0] = groupAttr;
		Attribute & thisAttribute = attrs[1];
		thisAttribute.type = TypeReal;
		thisAttribute.length = sizeof(float);
		if (op == MIN){
			if (aggAttr.type == TypeInt){
				thisAttribute.type = TypeInt;
			}
			thisAttribute.name = "MIN(" + groupAttr.name + ")";
		}else if (op == MAX){
			if (aggAttr.type == TypeInt){
				thisAttribute.type = TypeInt;
			}
			thisAttribute.name = "MAX(" + groupAttr.name + ")";
		}else if (op == SUM){
			if (aggAttr.type == TypeInt){
				thisAttribute.type = TypeInt;
			}
			thisAttribute.name = "SUM(" + groupAttr.name + ")";
		}else if (op == AVG){	//First sum them together
			thisAttribute.name = "AVG(" + groupAttr.name + ")";
		}else if (op == COUNT){
			thisAttribute.type = TypeInt;
			thisAttribute.name = "COUNT(" + groupAttr.name + ")";
		}
	}
};
