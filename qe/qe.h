#ifndef _qe_h_
#define _qe_h_

#include <vector>
#include <sstream>
#include <type_traits>

#include "../rbf/rbfm.h"
#include "../rm/rm.h"
#include "../ix/ix.h"

# define QE_EOF (-1)  // end of the index scan
# define GRACE_HJ_LEFT 0
# define GRACE_HJ_RIGHT 1

using namespace std;

typedef enum{ MIN = 0, MAX, SUM, AVG, COUNT } AggregateOp;


// The following functions use the following
// format for the passed data.
//    For INT and REAL: use 4 bytes
//    For VARCHAR: use 4 bytes for the length followed by
//                 the characters

struct Value {
    AttrType type;          // type of value
    void     *data;         // value
};


struct Condition {
    string  lhsAttr;        // left-hand side attribute
    CompOp  op;             // comparison operator
    bool    bRhsIsAttr;     // TRUE if right-hand side is an attribute and not a value; FALSE, otherwise.
    string  rhsAttr;        // right-hand side attribute if bRhsIsAttr = TRUE
    Value   rhsValue;       // right-hand side value if bRhsIsAttr = FALSE
};

Condition * createAndCopyCondition(const Condition & condition);
void deleteCondition(Condition * pCondition);

class Iterator {
    // All the relational operators and access methods are iterators.
    public:
        virtual RC getNextTuple(void *data) = 0;
        virtual void getAttributes(vector<Attribute> &attrs) const = 0;
        virtual ~Iterator() {};
};

bool checkCondition(const MultiTypeValue &leftValue, const MultiTypeValue &rightValue, Condition * pCondition);
//{
//	bool result = false;
//	if (leftValue.type == TypeInt){
//		if (pCondition->op == EQ_OP){
//			if (leftValue.intValue == rightValue.intValue){
//				result = true;
//			}
//		}else if (pCondition->op == LT_OP){
//			if (leftValue.intValue < rightValue.intValue){
//				result = true;
//			}
//		}else if (pCondition->op == GT_OP){
//			if (leftValue.intValue > rightValue.intValue){
//				result = true;
//			}
//		}else if (pCondition->op == LE_OP){
//			if (leftValue.intValue <= rightValue.intValue){
//				result = true;
//			}
//		}else if (pCondition->op == GE_OP){
//			if (leftValue.intValue >= rightValue.intValue){
//				result = true;
//			}
//		}else if (pCondition->op == NE_OP){
//			if (leftValue.intValue != rightValue.intValue){
//				result = true;
//			}
//		}
//
//	}else if (leftValue.type == TypeReal){
//		if (pCondition->op == EQ_OP){
//			if (leftValue.floatValue == rightValue.floatValue){
//				result = true;
//			}
//		}else if (pCondition->op == LT_OP){
//			if (leftValue.floatValue < rightValue.floatValue){
//				result = true;
//			}
//		}else if (pCondition->op == GT_OP){
//			if (leftValue.floatValue > rightValue.floatValue){
//				result = true;
//			}
//		}else if (pCondition->op == LE_OP){
//			if (leftValue.floatValue <= rightValue.floatValue){
//				result = true;
//			}
//		}else if (pCondition->op == GE_OP){
//			if (leftValue.floatValue >= rightValue.floatValue){
//				result = true;
//			}
//		}else if (pCondition->op == NE_OP){
//			if (leftValue.floatValue != rightValue.floatValue){
//				result = true;
//			}
//		}
//	}else{
//		if (pCondition->op == EQ_OP){
//			if (leftValue.strValue == rightValue.strValue){
//				result = true;
//			}
//		}else if (pCondition->op == LT_OP){
//			if (leftValue.strValue < rightValue.strValue){
//				result = true;
//			}
//		}else if (pCondition->op == GT_OP){
//			if (leftValue.strValue > rightValue.strValue){
//				result = true;
//			}
//		}else if (pCondition->op == LE_OP){
//			if (leftValue.strValue <= rightValue.strValue){
//				result = true;
//			}
//		}else if (pCondition->op == GE_OP){
//			if (leftValue.strValue >= rightValue.strValue){
//				result = true;
//			}
//		}else if (pCondition->op == NE_OP){
//			if (leftValue.strValue != rightValue.strValue){
//				result = true;
//			}
//		}
//	}
//	return result;
//}

class TableScan : public Iterator
{
    // A wrapper inheriting Iterator over RM_ScanIterator
    public:
        RelationManager &rm;
        RM_ScanIterator *iter;
        string tableName;
        vector<Attribute> attrs;
        vector<string> attrNames;
        RID rid;

        TableScan(RelationManager &rm, const string &tableName, const char *alias = NULL):rm(rm)
        {
        	//Set members
        	this->tableName = tableName;

            // Get Attributes from RM
            rm.getAttributes(tableName, attrs);

            // Get Attribute Names from RM
            unsigned i;
            for(i = 0; i < attrs.size(); ++i)
            {
                // convert to char *
                attrNames.push_back(attrs[i].name);
            }

            // Call rm scan to get iterator
            iter = new RM_ScanIterator();
            rm.scan(tableName, "", NO_OP, NULL, attrNames, *iter);

            // Set alias
            if(alias) this->tableName = alias;
        };

        // Start a new iterator given the new compOp and value
        void setIterator()
        {
            iter->close();
            delete iter;
            iter = new RM_ScanIterator();
            rm.scan(tableName, "", NO_OP, NULL, attrNames, *iter);
        };

        RC getNextTuple(void *data)
        {
            return iter->getNextTuple(rid, data);
        };

        void getAttributes(vector<Attribute> &attrs) const
        {
            attrs.clear();
            attrs = this->attrs;
            unsigned i;

            // For attribute in vector<Attribute>, name it as rel.attr
            for(i = 0; i < attrs.size(); ++i)
            {
                string tmp = tableName;
                tmp += ".";
                tmp += attrs[i].name;
                attrs[i].name = tmp;
            }
        };

        ~TableScan()
        {
        	iter->close();
        };
};


class IndexScan : public Iterator
{
    // A wrapper inheriting Iterator over IX_IndexScan
    public:
        RelationManager &rm;
        RM_IndexScanIterator *iter;
        string tableName;
        string attrName;
        vector<Attribute> attrs;
        char key[PAGE_SIZE];
        RID rid;

        IndexScan(RelationManager &rm, const string &tableName, const string &attrName, const char *alias = NULL):rm(rm)
        {
        	// Set members
        	this->tableName = tableName;
        	this->attrName = attrName;


            // Get Attributes from RM
            rm.getAttributes(tableName, attrs);

            // Call rm indexScan to get iterator
            iter = new RM_IndexScanIterator();
            rm.indexScan(tableName, attrName, NULL, NULL, true, true, *iter);

            // Set alias
            if(alias) this->tableName = alias;
        };

        // Start a new iterator given the new key range
        void setIterator(void* lowKey,
                         void* highKey,
                         bool lowKeyInclusive,
                         bool highKeyInclusive)
        {
            iter->close();
            delete iter;
            iter = new RM_IndexScanIterator();
            rm.indexScan(tableName, attrName, lowKey, highKey, lowKeyInclusive,
                           highKeyInclusive, *iter);
        };

        RC getNextTuple(void *data)
        {
            int rc = iter->getNextEntry(rid, key);
            if(rc == 0)
            {
                rc = rm.readTuple(tableName.c_str(), rid, data);
            }
            return rc;
        };

        void getAttributes(vector<Attribute> &attrs) const
        {
            attrs.clear();
            attrs = this->attrs;
            unsigned i;

            // For attribute in vector<Attribute>, name it as rel.attr
            for(i = 0; i < attrs.size(); ++i)
            {
                string tmp = tableName;
                tmp += ".";
                tmp += attrs[i].name;
                attrs[i].name = tmp;
            }
        };

        ~IndexScan()
        {
            iter->close();
        };
};


class Filter : public Iterator {
    // Filter operator
    public:
		Iterator * pItor;
		Condition * pCondition;
		vector<Attribute> attrs;
		AttrType conditionType;

        Filter(Iterator *input,               // Iterator of input R
               const Condition &condition     // Selection condition
        );
        ~Filter(){
        	deleteCondition(pCondition);
        	pCondition = 0;
        };

        RC getNextTuple(void *data);

        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const{
        	pItor->getAttributes(attrs);
        };
};


class Project : public Iterator {
    // Projection operator
    public:
		Iterator * pItor;
		vector<Attribute> outputAttrs;			//The attributes which will be output
		vector<unsigned> originOrders;			//The orders of the attributes in the origin vector
		vector<Attribute> allAttrs;				//All the attributes from pItor
        Project(Iterator *input,                    // Iterator of input R
              const vector<string> &attrNames);   // vector containing attribute names
        ~Project(){};

        RC getNextTuple(void *data);
        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const{
        	attrs.clear();
        	attrs.insert(attrs.begin(), outputAttrs.begin(), outputAttrs.end());
        };
};


/*
 * This is a inner-join
 */
class GHJoin : public Iterator {
    // Grace hash join operator
    public:
	  unsigned numPartitions;
	  Condition * pCondition;
	  int GHJoin_ID;				//Join ID
	  unsigned currentPartitionID;
	  RBFM_ScanIterator R_Itor;
	  FileHandle R_FileHandle;
	  string R_name;
	  string S_name;
	  vector<Attribute> R_attrs;
	  vector<string> R_attrNames;
	  vector<Attribute> S_attrs;
	  vector<string> S_attrNames;
	  vector<Attribute> Output_attrs;
	  unsigned R_matchIndex;
	  unsigned S_matchIndex;
	  unordered_map<unsigned, vector<FormattedRecord>> S_HashTable;
	  list<FormattedRecord> bufferedOutput;	 //First-In First-Out

      GHJoin(Iterator *leftIn,               // Iterator of input R, outer
            Iterator *rightIn,               // Iterator of input S, inner
            const Condition &condition,      // Join condition (CompOp is always EQ)
            const unsigned numPartitions     // # of partitions for each relation (decided by the optimizer)
      );
      ~GHJoin(){

    	  deleteCondition(pCondition);
    	  pCondition = 0;
      };

      void getPartitionName(const string & attributeName,		//The original attribute name, we want the left part
//    		  const string & delim,
    		  string & partitionName
    		  ){
    	  partitionName.clear();
    	  partitionName = attributeName.substr(0, attributeName.find("."));
      }

      void getPartitionFileName(const int type, 	 // GRACE_HJ_LEFT (0), GRACE_HJ_RIGHT (1)
			  const int partitionIndex,			//The partition id
			  const string & partitionName,		 // Partition name
    		  string & fileName				 // The result
    		  ){
    	  stringstream ss;
    	  if (type == GRACE_HJ_LEFT){
    		  ss << "left_join";
    	  }else{
    		  ss << "right_join";
    	  }
    	  ss << GHJoin_ID;
    	  ss << "_";
    	  ss << partitionName;
    	  ss << "_";
    	  ss << partitionIndex;
    	  fileName.clear();
    	  fileName.append(ss.str());
      }

      void buildInMemoryHashTable(RBFM_ScanIterator & rbfm_Itor,
    		  unsigned matchAttrIndex,
    		  unordered_map<unsigned, vector<FormattedRecord>> & inMemMap){
    	  inMemMap.clear();
    	  char buffer[PAGE_SIZE];
    	  RID tempRID;
    	  while (rbfm_Itor.getNextRecord(tempRID, buffer) == 0){
    		  FormattedRecord thisRecord;
    		  readRecordFromRawData(buffer, rbfm_Itor.recordDescriptor, thisRecord);
    		  unsigned hashValue = 0;
    		  MultiTypeValue & matchValue = thisRecord.values[matchAttrIndex];
    		  if (matchValue.type == TypeInt){
    			  hashValue = hashInt_2(matchValue.intValue);
    		  }else if (matchValue.type == TypeReal){
    			  hashValue = hashFloat_2(matchValue.floatValue);
    		  }else{
    			  hashValue = hashString_2(matchValue.strValue);
    		  }
    		  inMemMap[hashValue].push_back(thisRecord);
    	  }
      }

      RC getNextTuple(void *data);
      // For attribute in vector<Attribute>, name it as rel.attr
      void getAttributes(vector<Attribute> &attrs) const{
    	  attrs.clear();
    	  attrs.insert(attrs.begin(), Output_attrs.begin(), Output_attrs.end());
      };
};

/*
 * This is a inner-join
 */
class BNLJoin : public Iterator {
    // Block nested-loop join operator
    public:
		Iterator * R_Itor;
		TableScan * S_Itor;
		Condition * pCondition;
		unsigned numRecords;
		vector<Attribute> R_attrs;
		vector<Attribute> S_attrs;
		unsigned R_matchIndex;
		unsigned S_matchIndex;
		bool R_finished;
		vector<Attribute> Output_attrs;
		unordered_map<unsigned, FormattedRecord> inMemHashTable;
		unordered_map<unsigned, FormattedRecord>::iterator cached_HashTable_Itor;
		FormattedRecord cached_S_Tuple;

        BNLJoin(Iterator *leftIn,            // Iterator of input R
               TableScan *rightIn,           // TableScan Iterator of input S
               const Condition &condition,   // Join condition
               const unsigned numRecords     // # of records can be loaded into memory, i.e., memory block size (decided by the optimizer)
        );
        ~BNLJoin(){
        	deleteCondition(pCondition);
        	pCondition = 0;
        };

        RC getNextTuple(void *data);
        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const{
        	attrs.clear();
        	attrs.insert(attrs.begin(), Output_attrs.begin(), Output_attrs.end());
        };
};

/*
 * This is a inner-join
 */
class INLJoin : public Iterator {
    // Index nested-loop join operator
    public:
		Iterator * R_Itor;
		IndexScan * S_Itor;
		Condition * pCondition;
		vector<Attribute> R_attrs;
		vector<Attribute> S_attrs;
		unsigned R_matchIndex;
		unsigned S_matchIndex;
		vector<Attribute> Output_attrs;
		FormattedRecord cached_R_Tuple;
		bool R_finished;
		bool S_valid;
		char key[PAGE_SIZE];

        INLJoin(Iterator *leftIn,           // Iterator of input R
               IndexScan *rightIn,          // IndexScan Iterator of input S
               const Condition &condition   // Join condition
        );
        ~INLJoin(){
        	deleteCondition(pCondition);
        	pCondition = 0;
        };

        RC getNextTuple(void *data);
        // For attribute in vector<Attribute>, name it as rel.attr
        void getAttributes(vector<Attribute> &attrs) const{
        	attrs.clear();
        	attrs.insert(attrs.begin(), Output_attrs.begin(), Output_attrs.end());
        };
};

/*
 * This is a inner-join
 */
class Aggregate : public Iterator {
    // Aggregation operator
    public:

		bool basic;
		vector<Attribute> attrs;		//TODO whether to keep this field
		Attribute aggAttr;
		AggregateOp op;
		unsigned aggIndex;

		//For basic aggregation
		MultiTypeValue basicValue;
		bool isInitialized;
		bool resultReturned;

		//For group-based hash aggregation
		unsigned currentPartitionID;
		unsigned numPartitions;
		Attribute groupAttr;
		vector<Attribute> partitionAttrs;
		vector<string> partitionAttrNames;
		unsigned groupIndex;
		unsigned GroupByAggregateID;
		list<FormattedRecord> bufferedResults;
		//TODO

        // Mandatory for graduate teams only
        // Basic aggregation
        Aggregate(Iterator *input,          // Iterator of input R
                  Attribute aggAttr,        // The attribute over which we are computing an aggregate
                  AggregateOp op            // Aggregate operation
        );

        // Optional for everyone. 5 extra-credit points
        // Group-based hash aggregation
        Aggregate(Iterator *input,             // Iterator of input R
                  Attribute aggAttr,           // The attribute over which we are computing an aggregate
                  Attribute groupAttr,         // The attribute over which we are grouping the tuples
                  AggregateOp op,              // Aggregate operation
                  const unsigned numPartitions // Number of partitions for input (decided by the optimizer)
        );
        ~Aggregate();

        //Used by Group-based hash aggregation
        void getPartitionFileName(int partitionIndex, 	   // The partition id
  			  const string & groupAttrName,	   // Group by attribute name
			  const string & aggAttrName,	   // Aggregate attribute name
      		  string & fileName			   // The result
      		  ){
      	  stringstream ss;
      	  ss << "GroupByAggregate_";
      	  ss << GroupByAggregateID;
      	  ss << "_";
      	  ss << groupAttrName;
      	  ss << "_";
      	  ss << aggAttrName;
      	  ss << "_";
      	  ss << partitionIndex;
      	  fileName.clear();
      	  fileName.append(ss.str());
        }

        void buildInMemHashTable(RBFM_ScanIterator & rbfmItor,
        		unordered_map<float, float> & inMemHashTable,		//May have precision problem, because the key is float type
        		unordered_map<float, int> & countTable 			//Only used by AVG
        		){
        	inMemHashTable.clear();
        	countTable.clear();
        	unordered_map<float, float>::iterator inMemItor;
        	char buffer[PAGE_SIZE];
//        	char temp[PAGE_SIZE];
			RID thisRID;
			while (rbfmItor.getNextRecord(thisRID, buffer) == 0){
				float thisKey;
				float thisValue;
				memcpy(&thisKey, buffer, sizeof(float));
				memcpy(&thisValue, buffer+sizeof(float), sizeof(float));

				inMemItor = inMemHashTable.find(thisKey);
				if (op == MIN){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue < inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == MAX){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue > inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == SUM){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						inMemItor->second += thisValue;
					}
				}else if (op == AVG){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
						countTable[thisKey] = 1;
					}else{
						inMemItor->second += thisValue;
						countTable[thisKey] = countTable[thisKey] + 1;
					}
				}else if (op == COUNT){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = 1;
					}else{
						inMemItor->second += 1;
					}
				}
			}
        };

        void buildInMemHashTable(RBFM_ScanIterator & rbfmItor,
        		unordered_map<float, int> & inMemHashTable,		//May have precision problem, because the key is float type
        		unordered_map<float, int> & countTable 			//Only used by AVG
        		){
        	inMemHashTable.clear();
        	countTable.clear();
        	unordered_map<float, int>::iterator inMemItor;
        	char buffer[PAGE_SIZE];
//        	char temp[PAGE_SIZE];
			RID thisRID;
			while (rbfmItor.getNextRecord(thisRID, buffer) == 0){
				float thisKey;
				int thisValue;
				memcpy(&thisKey, buffer, sizeof(float));
				memcpy(&thisValue, buffer+sizeof(float), sizeof(int));

				inMemItor = inMemHashTable.find(thisKey);
				if (op == MIN){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue < inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == MAX){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue > inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == SUM){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						inMemItor->second += thisValue;
					}
				}else if (op == AVG){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
						countTable[thisKey] = 1;
					}else{
						inMemItor->second += thisValue;
						countTable[thisKey] = countTable[thisKey] + 1;
					}
				}else if (op == COUNT){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = 1;
					}else{
						inMemItor->second += 1;
					}
				}
			}
        };

        void buildInMemHashTable(RBFM_ScanIterator & rbfmItor,
        		unordered_map<int, float> & inMemHashTable,		//May have precision problem, because the key is float type
        		unordered_map<int, int> & countTable 			//Only used by AVG
        		){
        	inMemHashTable.clear();
        	countTable.clear();
        	unordered_map<int, float>::iterator inMemItor;
        	char buffer[PAGE_SIZE];
//        	char temp[PAGE_SIZE];
			RID thisRID;
			while (rbfmItor.getNextRecord(thisRID, buffer) == 0){
				int thisKey;
				float thisValue;
				memcpy(&thisKey, buffer, sizeof(int));
				memcpy(&thisValue, buffer+sizeof(int), sizeof(float));

				inMemItor = inMemHashTable.find(thisKey);
				if (op == MIN){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue < inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == MAX){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue > inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == SUM){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						inMemItor->second += thisValue;
					}
				}else if (op == AVG){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
						countTable[thisKey] = 1;
					}else{
						inMemItor->second += thisValue;
						countTable[thisKey] = countTable[thisKey] + 1;
					}
				}else if (op == COUNT){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = 1;
					}else{
						inMemItor->second += 1;
					}
				}
			}
        };

        void buildInMemHashTable(RBFM_ScanIterator & rbfmItor,
        		unordered_map<int, int> & inMemHashTable,		//May have precision problem, because the key is float type
        		unordered_map<int, int> & countTable 			//Only used by AVG
        		){
        	inMemHashTable.clear();
        	countTable.clear();
        	unordered_map<int, int>::iterator inMemItor;
        	char buffer[PAGE_SIZE];
//        	char temp[PAGE_SIZE];
			RID thisRID;
			while (rbfmItor.getNextRecord(thisRID, buffer) == 0){
				int thisKey;
				int thisValue;
				memcpy(&thisKey, buffer, sizeof(int));
				memcpy(&thisValue, buffer+sizeof(int), sizeof(int));

				inMemItor = inMemHashTable.find(thisKey);
				if (op == MIN){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue < inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == MAX){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue > inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == SUM){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						inMemItor->second += thisValue;
					}
				}else if (op == AVG){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
						countTable[thisKey] = 1;
					}else{
						inMemItor->second += thisValue;
						countTable[thisKey] = countTable[thisKey] + 1;
					}
				}else if (op == COUNT){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = 1;
					}else{
						inMemItor->second += 1;
					}
				}
			}
        };

        void buildInMemHashTable(RBFM_ScanIterator & rbfmItor,
        		unordered_map<string, float> & inMemHashTable,		//May have precision problem, because the key is float type
        		unordered_map<string, int> & countTable 			//Only used by AVG
        		){
        	inMemHashTable.clear();
        	countTable.clear();
        	unordered_map<string, float>::iterator inMemItor;
        	char buffer[PAGE_SIZE];
        	char temp[PAGE_SIZE];
			RID thisRID;
			while (rbfmItor.getNextRecord(thisRID, buffer) == 0){
				string thisKey;
				float thisValue;
				unsigned charLength = 0;
				memcpy(&charLength, buffer, sizeof(unsigned));
				memcpy(temp, buffer+sizeof(unsigned), charLength);
				temp[charLength] = '\0';
				thisKey = temp;
				memcpy(&thisValue, buffer+sizeof(unsigned)+charLength, sizeof(float));

				inMemItor = inMemHashTable.find(thisKey);
				if (op == MIN){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue < inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == MAX){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue > inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == SUM){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						inMemItor->second += thisValue;
					}
				}else if (op == AVG){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
						countTable[thisKey] = 1;
					}else{
						inMemItor->second += thisValue;
						countTable[thisKey] = countTable[thisKey] + 1;
					}
				}else if (op == COUNT){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = 1;
					}else{
						inMemItor->second += 1;
					}
				}
			}
        };

        void buildInMemHashTable(RBFM_ScanIterator & rbfmItor,
        		unordered_map<string, int> & inMemHashTable,		//May have precision problem, because the key is float type
        		unordered_map<string, int> & countTable 			//Only used by AVG
        		){
        	inMemHashTable.clear();
        	countTable.clear();
        	unordered_map<string, int>::iterator inMemItor;
        	char buffer[PAGE_SIZE];
        	char temp[PAGE_SIZE];
			RID thisRID;
			while (rbfmItor.getNextRecord(thisRID, buffer) == 0){
				string thisKey;
				int thisValue;
				unsigned charLength = 0;
				memcpy(&charLength, buffer, sizeof(unsigned));
				memcpy(temp, buffer+sizeof(unsigned), charLength);
				temp[charLength] = '\0';
				thisKey = temp;
				memcpy(&thisValue, buffer+sizeof(unsigned)+charLength, sizeof(int));

				inMemItor = inMemHashTable.find(thisKey);
				if (op == MIN){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue < inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == MAX){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						if (thisValue > inMemItor->second){
							inMemItor->second = thisValue;
						}
					}
				}else if (op == SUM){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
					}else{
						inMemItor->second += thisValue;
					}
				}else if (op == AVG){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = thisValue;
						countTable[thisKey] = 1;
					}else{
						inMemItor->second += thisValue;
						countTable[thisKey] = countTable[thisKey] + 1;
					}
				}else if (op == COUNT){
					if (inMemItor == inMemHashTable.end()){
						inMemHashTable[thisKey] = 1;
					}else{
						inMemItor->second += 1;
					}
				}
			}
        };

        RC getNextTuple(void *data);
        // Please name the output attribute as aggregateOp(aggAttr)
        // E.g. Relation=rel, attribute=attr, aggregateOp=MAX
        // output attrname = "MAX(rel.attr)"
        void getAttributes(vector<Attribute> &attrs) const;
};

#endif
