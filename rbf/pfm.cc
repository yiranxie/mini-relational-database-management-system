#include "pfm.h"

#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;

PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance() {
	if (!_pf_manager)
		_pf_manager = new PagedFileManager();

	return _pf_manager;
}

PagedFileManager::PagedFileManager() {
}

PagedFileManager::~PagedFileManager() {
}

//This method creates a paged file called fileName.
//The file should not already exist.
RC PagedFileManager::createFile(const char *fileName) {
	ifstream ifs;
	ifs.open(fileName, ifstream::in);
	if (ifs) { //check whether the file exists, if so, return -1
		return -1;
	}
	ifs.close();

	ofstream ofs;
	ofs.open(fileName, ofstream::out);
	ofs.close();
	return 0;
}

//This method destroys the paged file whose name is fileName.
//The file should exist.
RC PagedFileManager::destroyFile(const char *fileName) {
	return remove(fileName);
}

//This method opens the paged file whose name is fileName.
//The file must already exist and it must have been created using the createFile method.
//It is an error if fileHandle is already a handle for an open file when it is passed to the openFile method.
RC PagedFileManager::openFile(const char *fileName, FileHandle &fileHandle) {
	if (fileHandle.fs.is_open()) // fileHandle is already a handle for an open file
		return -1;
	// test whether the file exists
	ifstream f(fileName);
	if (!f.good()) {
		f.close();
		return 1;
	} else {
		f.close();
	}
	fileHandle.fs.open(fileName, fstream::in | fstream::out | fstream::binary);
	if (!fileHandle.fs.good())
		return -1;
	return 0;
}

//This method closes the open file instance referred to by fileHandle.
//The file must have been opened using the openFile method.
//All of the file's pages are flushed to disk when the file is closed.
RC PagedFileManager::closeFile(FileHandle &fileHandle) {
	// test whether the file is open.
	if (!fileHandle.fs.is_open())
		return 2;
	fileHandle.fs.close();
	return 0;
}

FileHandle::FileHandle() {
}

FileHandle::~FileHandle() {
}

//This method reads the page into the memory block pointed by data.
//The page should exist. Note the page number starts from 0.
RC FileHandle::readPage(PageNum pageNum, void *data) {
	if (!fs.is_open()) // non-open file
		return 2;
	if (pageNum >= getNumberOfPages() || pageNum < 0) // invalid parameter
		return 3;
	int offset = pageNum * PAGE_SIZE;
	fs.seekg(offset, fs.beg);
	fs.read((char *) data, PAGE_SIZE);
	readPageCounter++; //increase readPageCounter whenever readPage is executed
	return 0;
}

//This method writes the data into a page specified by the pageNum.
//The page should exist. Note the page number starts from 0.
RC FileHandle::writePage(PageNum pageNum, const void *data) {
	if (!fs.is_open()) // non-open file
		return 2;
	if (pageNum >= getNumberOfPages() || pageNum < 0) // invalid parameter
		return 3;
	int offset = pageNum * PAGE_SIZE;
	fs.seekp(offset, fs.beg);
	fs.write((const char *) data, PAGE_SIZE);
	writePageCounter++; //increase writePageCounter whenever writePage is executed
	return 0;
}

//This method appends a new page to the file, and writes the data into the new allocated page.
RC FileHandle::appendPage(const void *data) {
	if (!fs.is_open()) // non-open file
		return 2;
	fs.seekp(0, fs.end);
	fs.write((const char *) data, PAGE_SIZE);
	appendPageCounter++; //increase appendPageCounter whenever appendPage is executed
	return 0;
}

//This method returns the total number of pages in the file.
unsigned FileHandle::getNumberOfPages() {
	fs.seekg(0, fs.end);
	int length = fs.tellg();
	return length / PAGE_SIZE;
}

// New method - put the current counter values into variables
RC FileHandle::collectCounterValues(unsigned &readPageCount,
		unsigned &writePageCount, unsigned &appendPageCount) {
	readPageCount = readPageCounter;
	writePageCount = writePageCounter;
	appendPageCount = appendPageCounter;
	return 0;
}
