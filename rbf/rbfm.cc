#include "rbfm.h"
#include <iostream>

RC generateDataInMemoryFormat(const char * data, unsigned maxSize,
		vector<MultiTypeValue> & values){

	char * ptr = (char *) data;
	unsigned currentSize = 0;
	for (unsigned i = 0; i < values.size(); ++i) {
		MultiTypeValue & thisValue = values[i];
		if (thisValue.type == TypeInt) {
			currentSize += 4;
			if (currentSize > maxSize) {
				return -1;
			}
			memcpy(ptr, &thisValue.intValue, sizeof(int));
			ptr += sizeof(int);
		} else if (thisValue.type == TypeReal) {
			currentSize += 4;
			if (currentSize > maxSize) {
				return -1;
			}
			memcpy(ptr, &thisValue.floatValue, sizeof(float));
			ptr += sizeof(float);
		} else if (thisValue.type == TypeVarChar) {
			currentSize += (4 + thisValue.strValue.length());
			if (currentSize > maxSize) {
				return -1;
			}
			unsigned charLength = thisValue.strValue.length();
			memcpy(ptr, &charLength, sizeof(unsigned));
			ptr += sizeof(unsigned);
			memcpy(ptr, thisValue.strValue.c_str(),
					thisValue.strValue.length());
			ptr += thisValue.strValue.length();
		}
	}
	return 0;
}

void readRecordFromRawData(const void * data, const vector<Attribute> attrs, FormattedRecord & record){
	record.values.clear();
	unsigned totalLength = 0;
	unsigned totalAttributes = attrs.size();
	record.values.resize(totalAttributes);
	char temp[PAGE_SIZE];
	const char * pdata = (const char *)data;
	for (unsigned i=0; i<attrs.size(); ++i){
		const Attribute & thisAttr = attrs[i];
		MultiTypeValue & thisValue = record.values[i];
		thisValue.type = thisAttr.type;
		if (thisAttr.type == TypeInt){
			memcpy(&thisValue.intValue, pdata+totalLength, sizeof(int));
			totalLength += sizeof(int);
		}else if (thisAttr.type == TypeReal){
			memcpy(&thisValue.floatValue, pdata+totalLength, sizeof(float));
			totalLength += sizeof(float);
		}else{
			unsigned charLength = 0;
			memcpy(&charLength, pdata+totalLength, sizeof(unsigned));
			totalLength += sizeof(unsigned);
			memcpy(temp, pdata+totalLength, charLength);
			temp[charLength] = '\0';
			thisValue.strValue = temp;
			totalLength += charLength;
		}
	}
}

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance() {
	if (!_rbf_manager)
		_rbf_manager = new RecordBasedFileManager();

	return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager() {
}

RecordBasedFileManager::~RecordBasedFileManager() {
}

//This method creates a record-based file called fileName.
//The file should not already exist.
//This method should internally use the method PagedFileManager::createFile (const char *fileName).
RC RecordBasedFileManager::createFile(const string &fileName) {
	const char * file = fileName.c_str();
	return PagedFileManager::instance()->createFile(file);
}

//This method destroys the record-based file whose name is fileName.
//The file should exist.
//This method should internally use the method PagedFileManager::destroyFile (const char *fileName).
RC RecordBasedFileManager::destroyFile(const string &fileName) {
	const char * file = fileName.c_str();
	return PagedFileManager::instance()->destroyFile(file);
}

//This method opens the record-based file whose name is fileName.
//The file must already exist and it must have been created using the RecordBasedFileManager::createFile method.
//This method should internally use the method PagedFileManager::openFile(const char *fileName, FileHandle &fileHandle).
RC RecordBasedFileManager::openFile(const string &fileName,
		FileHandle &fileHandle) {
	const char * file = fileName.c_str();
	return PagedFileManager::instance()->openFile(file, fileHandle);
}

//This method closes the open file instance referred to by fileHandle.
//The file must have been opened using the RecordBasedFileManager::openFile method.
//This method should internally use the method PagedFileManager::closeFile(FileHandle &fileHandle).
RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) {
	return PagedFileManager::instance()->closeFile(fileHandle);
}

//Given a record descriptor, insert a record into a given file identified by the provided handle.
//You do not need to check if the input record has the right number of attributes and if the attribute types match.
//Find the first page with free space large enough to store the record and store the record at that location.
//The insertRecord method accepts an RID object and fills it with the RID of the record that is target for insertion.
RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor, const void *data, RID &rid) {
	unsigned AttrNum = recordDescriptor.size(); // number of attributes in the vector
	char * record = new char[PAGE_SIZE]; // allocate a char array to store the record
	// copy the record into the allocated char array, in the variable length record format
	*(unsigned *) record = AttrNum; // the first 4 Bytes are used to store AttrNum
	int current = 4 * (AttrNum + 1); // initialize the starting Byte of "record" to store attributes' actual values
	int curData = 0; // initialize the staring Byte of "data" to be read into "record"
	for (unsigned i = 0; i < AttrNum; i++) {
		if (recordDescriptor[i].type == TypeInt) {
			*(int *) (record + current) = *(int *) ((char *) data + curData); //store the int value
			current += 4;
			*(int *) (record + 4 * (i + 1)) = current; // store the ending offset of this int attribute
			curData += 4;
		} else if (recordDescriptor[i].type == TypeReal) {
			*(float *) (record + current) =
					*(float *) ((char *) data + curData); //store the float value
			current += 4;
			*(int *) (record + 4 * (i + 1)) = current; // store the ending offset of this real number attribute
			curData += 4;
		} else if (recordDescriptor[i].type == TypeVarChar) {
			unsigned length = *(unsigned *) ((char *) data + curData); //length of the varchar
			curData += 4;
			memcpy(record + current, (char*)data + curData, length);
			current += length;
			curData += length;
			*(int *) (record + 4 * (i + 1)) = current; // store the ending offset of this varchar
		} else
			return -1;
	}
	// At this time, the variable "current" equals to the total length of this record
	if (current > PAGE_SIZE - 16) //a record so big that won't fit on an empty page
		return -1;
	//Find the first page with free space large enough to store the record and store the record at that location.
	char * page = new char[PAGE_SIZE]; // allocate a char array of PAGE_SIZE
	PageNum pageNum;
	for (pageNum = 0; pageNum < fileHandle.getNumberOfPages(); pageNum++) {
		fileHandle.readPage(pageNum, page);
		//The last four Bytes of the page stores the offset of the beginning of free space.
		int freeSpaceBeginOffset = *((int *) (page + PAGE_SIZE - 4));
		//The second last four Bytes stores the number of used slots.
		unsigned numOfSlots = *((unsigned *) (page + PAGE_SIZE - 8));
		//The offset of the end of free space
		int freeSpaceEndOffset = PAGE_SIZE - 8 * (numOfSlots + 1);
		//The length of free space
		int freeSpaceLength = freeSpaceEndOffset - freeSpaceBeginOffset;
		if (freeSpaceLength < 0)
			return -1;
		if (freeSpaceLength < current) // not enough free space on the page
			continue;
		// update metadata at the end of the page
		// if there exists an empty slot due to deletion:
		unsigned emptySlot;
		for (emptySlot = 1; emptySlot <= numOfSlots; emptySlot++) {
			if (*((int *) (page + PAGE_SIZE - 8 * (emptySlot + 1))) == -1)
				break;
		}
		if (emptySlot <= numOfSlots) {
			// copy the record to the page
			for (int i = 0; i < current; i++)
				*(page + freeSpaceBeginOffset + i) = *(record + i);
			*((int *) (page + PAGE_SIZE - 8 * (emptySlot + 1))) =
					freeSpaceBeginOffset; //store the offset of the record
			*((unsigned *) (page + PAGE_SIZE - 4 - 8 * emptySlot)) = current; //store the length of the record
			*((int *) (page + PAGE_SIZE - 4)) = freeSpaceBeginOffset + current; //update the beginning of free space
			rid.pageNum = pageNum;  //update rid
			rid.slotNum = emptySlot;
			fileHandle.writePage(pageNum, page);  // flush data back to the disk
		} else {   // no empty slot exists
			if (freeSpaceLength - 8 < current) // not enough free space on the page (need 8 Bytes to store the new slot info)
				continue;
			// copy the record to the page
			for (int i = 0; i < current; i++)
				*(page + freeSpaceBeginOffset + i) = *(record + i);
			*((unsigned *) (page + PAGE_SIZE - 8)) += 1; // update the number of used slots
			numOfSlots = *((unsigned *) (page + PAGE_SIZE - 8));
			*((int *) (page + PAGE_SIZE - 8 * (numOfSlots + 1))) =
					freeSpaceBeginOffset; //store the offset of the record
			*((unsigned *) (page + PAGE_SIZE - 4 - 8 * numOfSlots)) = current; //store the length of the record
			*((int *) (page + PAGE_SIZE - 4)) = freeSpaceBeginOffset + current; //update the beginning of free space
			rid.pageNum = pageNum;  //update rid
			rid.slotNum = numOfSlots;
			fileHandle.writePage(pageNum, page);  // flush data back to the disk
		}
		break;
	}
	// if no free space big enough on existing pages
	if (pageNum == fileHandle.getNumberOfPages()) {
		rid.pageNum = pageNum; //update rid
		rid.slotNum = 1;
		for (int i = 0; i < current; i++) //copy the record to the beginning of the page
			*(page + i) = *(record + i);
		// at the end of the new page, write metadata
		*((unsigned *) (page + PAGE_SIZE - 8)) = 1; //The number of slots is 1.
		*((int *) (page + PAGE_SIZE - 16)) = 0; //store the offset of the record
		*((unsigned *) (page + PAGE_SIZE - 12)) = current; //store the length of the record
		*((int *) (page + PAGE_SIZE - 4)) = current; //store the beginning of free space
		fileHandle.appendPage(page);  // append the new page
	}

	delete[] page;
	delete[] record;
	return 0;
}

//Given a record descriptor, read the record identified by the given rid.
RC RecordBasedFileManager::readRecord(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor, const RID &rid, void *data) {
	if (rid.pageNum >= fileHandle.getNumberOfPages())
		return -1;
	char * page = new char[PAGE_SIZE];
	unsigned pageNum = rid.pageNum;
	unsigned slotNum = rid.slotNum;
	while (1) {
		fileHandle.readPage(pageNum, page);
		if (slotNum > *((unsigned *) (page + PAGE_SIZE - 8)) || slotNum < 1) {
			delete[] page;
			return -1;
		}
		if (*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) == -1) { // the record has been deleted already
			delete[] page;
			return -1;
		} else if ((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) >> 12)
				& 1) { // if the 13th least significant bit is 1, then this int stores slotNum
			pageNum = *((unsigned *) (page + PAGE_SIZE - 8 * slotNum - 4)); // pageNum of the new location
			slotNum = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
					& EFFECTIVE_BITS; // the last 12 bits store the slotNum of the new location
			continue;
		} else {  // stores the offset
			int offset = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
					& EFFECTIVE_BITS;  // the last 12 bits store the offset
			// copy the record into "data", in required format
			unsigned AttrNum = recordDescriptor.size(); // number of attributes in the vector
			int current = 4 * (AttrNum + 1); // initialize the starting Byte of "record" to read attributes' actual values
			int curData = 0; // initialize the staring Byte of "data" to store attributes
			for (unsigned i = 0; i < AttrNum; i++) {
				if (recordDescriptor[i].type == TypeInt) {
					*(int *) ((char *) data + curData) = *(int *) (page + offset
							+ current); //store the int value
					current += 4;
					curData += 4;
				} else if (recordDescriptor[i].type == TypeReal) {
					*(float *) ((char *) data + curData) = *(float *) (page
							+ offset + current); //store the float value
					current += 4;
					curData += 4;
				} else if (recordDescriptor[i].type == TypeVarChar) {
					unsigned varCharLength;
					if (i != 0)
						varCharLength = *(int *) (page + offset + 4 * (i + 1))
								- *(int *) (page + offset + 4 * i); //length of the varchar
					else
						varCharLength = *(int *) (page + offset + 4) - current;
					*(unsigned *) ((char *) data + curData) = varCharLength;
					curData += 4;
					memcpy((char*)data + curData, page + offset + current,
							varCharLength);
					current += varCharLength;
					curData += varCharLength;
				} else
					return -1;
			}
			delete[] page;
			return 0;
		}
	}
}

//This is a utility method that will be mainly used for debugging/testing.
//It should be able to interpret the bytes of each record using the passed record descriptor and then print its content to the screen.
RC RecordBasedFileManager::printRecord(
		const vector<Attribute> &recordDescriptor, const void *data) {
	unsigned AttrNum = recordDescriptor.size(); // number of attributes
	int curData = 0; // initialize the staring Byte of "data" to be printed
	for (unsigned i = 0; i < AttrNum; i++) {
		cout << "attribute name: " << recordDescriptor[i].name << "; ";
		if (recordDescriptor[i].type == TypeInt) {
			cout << "attribute type: int; attribute value: ";
			cout << *(int *) ((char *) data + curData) << endl; //print the int value
			curData += 4;
		} else if (recordDescriptor[i].type == TypeReal) {
			cout << "attribute type: real; attribute value: ";
			cout << *(float *) ((char *) data + curData) << endl; //print the float value
			curData += 4;
		} else if (recordDescriptor[i].type == TypeVarChar) {
			cout << "attribute type: characters; attribute value: ";
			unsigned length = *(unsigned *) ((char *) data + curData); //length of the varchar
			curData += 4;
			for (unsigned j = 0; j < length; j++)
				cout << *((char *) data + curData + j); //print a character at a time
			cout << endl;
			curData += length;
		} else
			return -1;
	}
	return 0;
}

//Delete all records in the file.
RC RecordBasedFileManager::deleteRecords(FileHandle &fileHandle) {
	char * page = new char[PAGE_SIZE]; // allocate a char array of PAGE_SIZE
	for (PageNum pageNum = 0; pageNum < fileHandle.getNumberOfPages();
			pageNum++) {
		fileHandle.readPage(pageNum, page);
		*((unsigned *) (page + PAGE_SIZE - 8)) = 0; //The number of slots is 0.
		*((int *) (page + PAGE_SIZE - 4)) = 0; //store the beginning offset of free space
		fileHandle.writePage(pageNum, page);  // flush data back to the disk
	}
	delete[] page;
	return 0;
}

//Given a record descriptor, delete the record identified by the given rid.
RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor, const RID &rid) {
	if (rid.pageNum >= fileHandle.getNumberOfPages())
		return -1;
	char * page = new char[PAGE_SIZE]; // allocate a char array of PAGE_SIZE
	unsigned pageNum = rid.pageNum;
	unsigned slotNum = rid.slotNum;
	while (1) {
		fileHandle.readPage(pageNum, page);
		if (slotNum > *((unsigned *) (page + PAGE_SIZE - 8)) || slotNum < 1) {
			delete[] page;
			return -1;
		}
		if (*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) == -1) { // the record has been deleted already
			delete[] page;
			return -1;
		} else if ((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) >> 12)
				& 1) { // if the 13th least significant bit is 1, then this int stores slotNum
			pageNum = *((unsigned *) (page + PAGE_SIZE - 8 * slotNum - 4)); // pageNum of the new location
			slotNum = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
					& EFFECTIVE_BITS; // slotNum of the new location
			continue;
		} else {
			*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) = -1; // set the record offset to -1 to mark an empty slot no matter whether it is a forwarded address
			fileHandle.writePage(pageNum, page);
			delete[] page;
			return 0;
		}
	}
}

//Given a record descriptor, update the record identified by the given rid with the passed data.
//If the record grows and there is no space in the page to store the record, the record is migrated to a new page with enough free space.
//Records are identified by their rids and when they migrate, they leave a tombstone behind pointing to the new location of the record.
RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor, const void *data,
		const RID &rid) {
	unsigned AttrNum = recordDescriptor.size(); // number of attributes in the vector
	char * record = new char[PAGE_SIZE]; // allocate a char array to store the record
// copy the record into the allocated char array, in the variable-length record format
	*(unsigned *) record = AttrNum;	// the first 4 Bytes are used to store AttrNum
	int current = 4 * (AttrNum + 1);// initialize the starting Byte of "record" to store attributes' actual values
	int curData = 0;// initialize the staring Byte of "data" to be read into "record"
	for (unsigned i = 0; i < AttrNum; i++) {
		if (recordDescriptor[i].type == TypeInt) {
			*(int *) (record + current) = *(int *) ((char *) data + curData); //store the int value
			current += 4;
			*(int *) (record + 4 * (i + 1)) = current; // store the ending offset of this int attribute
			curData += 4;
		} else if (recordDescriptor[i].type == TypeReal) {
			*(float *) (record + current) =
					*(float *) ((char *) data + curData); //store the float value
			current += 4;
			*(int *) (record + 4 * (i + 1)) = current; // store the ending offset of this real number attribute
			curData += 4;
		} else if (recordDescriptor[i].type == TypeVarChar) {
			unsigned length = *(unsigned *) ((char *) data + curData); //length of the varchar
			curData += 4;
			memcpy(record + current, (char*)data + curData, length); //store the varchar
			current += length;
			curData += length;
			*(int *) (record + 4 * (i + 1)) = current; // store the ending offset of this varchar
		} else
			return -1;
	}
// At this time, the variable "current" equals to the total length of this new record
	unsigned length = current;

	if (rid.pageNum >= fileHandle.getNumberOfPages())
		return -1;
	char * page = new char[PAGE_SIZE]; // allocate a char array of PAGE_SIZE
	unsigned pageNum = rid.pageNum;
	unsigned slotNum = rid.slotNum;
	while (1) {
		fileHandle.readPage(pageNum, page);
		if (slotNum > *((unsigned *) (page + PAGE_SIZE - 8)) || slotNum < 1) {
			delete[] page;
			delete[] record;
			return -1;
		}
		if (*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) == -1) { // the record has been deleted already
			delete[] page;
			delete[] record;
			return -1;
		} else if ((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) >> 12)
				& 1) { // if the 13th least significant bit is 1, then this int stores slotNum
			pageNum = *((unsigned *) (page + PAGE_SIZE - 8 * slotNum - 4)); // pageNum of the new location
			slotNum = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
					& EFFECTIVE_BITS; // the last 12 bits store the slotNum of the new location
			continue;
		} else {  // stores the offset
			unsigned oldLength = *((unsigned *) (page + PAGE_SIZE - 4
					- 8 * slotNum));
			unsigned N = *((unsigned *) (page + PAGE_SIZE - 8));
			int freeSpaceEndOffset = PAGE_SIZE - 8 * (N + 1);
			int freeSpaceBeginOffset = *((int *) (page + PAGE_SIZE - 4));
			// The length of free space
			int freeSpaceLength = freeSpaceEndOffset - freeSpaceBeginOffset;
			if (freeSpaceLength < 0)
				return -1;
			if (length <= oldLength) { // update the record at the original location
				int offset = *((int *) (page + PAGE_SIZE - 8 - 8 * slotNum))
						& EFFECTIVE_BITS;
				memcpy(page + offset, record, length); // copy the record to the original location
				*((unsigned *) (page + PAGE_SIZE - 4 - 8 * slotNum)) = length; //update the record length
			} else if (length <= (unsigned) freeSpaceLength) { // move the record within page
				memcpy(page + freeSpaceBeginOffset, record, length); // copy the record to the free space
				if ((*((int *) (page + PAGE_SIZE - 8 - 8 * slotNum)) >> 13) & 1) // the slot has a forwarded record
					*((int *) (page + PAGE_SIZE - 8 - 8 * slotNum)) =
							freeSpaceBeginOffset + (1 << 13);
				else
					*((int *) (page + PAGE_SIZE - 8 - 8 * slotNum)) =
							freeSpaceBeginOffset; // update the record offset
				*((unsigned *) (page + PAGE_SIZE - 4 - 8 * slotNum)) = length; //update the record length
				*((int *) (page + PAGE_SIZE - 4)) = freeSpaceBeginOffset
						+ length;   // update the free space begin offset
			} else { // the record is migrated to a new page with enough free space.
				RID new_rid;
				if (insertRecord(fileHandle, recordDescriptor, data, new_rid)
						!= 0)
					return -1;
				// set the 14th least significant bit to 1 to mark this is a "forwarded" slot
				char * new_page = new char[PAGE_SIZE];
				fileHandle.readPage(new_rid.pageNum, new_page);
				*((int *) (new_page + PAGE_SIZE - 8 - 8 * new_rid.slotNum)) |= 1
						<< 13;
				fileHandle.writePage(new_rid.pageNum, new_page);
				delete[] new_page;
				// update the information at old location
				if ((*((int *) (page + PAGE_SIZE - 8 - 8 * slotNum)) >> 13) & 1) // if the old slot is both forwarding and forwarded
					*((int *) (page + PAGE_SIZE - 8 - 8 * slotNum)) =
							new_rid.slotNum + (3 << 12);
				else
					// if the old slot is just forwarding
					*((int *) (page + PAGE_SIZE - 8 - 8 * slotNum)) =
							new_rid.slotNum + (1 << 12);
				*((unsigned *) (page + PAGE_SIZE - 4 - 8 * slotNum)) =
						new_rid.pageNum; // store the new page number
			}
			fileHandle.writePage(pageNum, page);
			delete[] record;
			delete[] page;
			return 0;
		}
	}
}

//Given a record descriptor, read a specific attribute of a record identified by a given rid.
RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor, const RID &rid,
		const string attributeName, void *data) {
	if (rid.pageNum >= fileHandle.getNumberOfPages())
		return -1;
	char * page = new char[PAGE_SIZE];
	unsigned pageNum = rid.pageNum;
	unsigned slotNum = rid.slotNum;
	while (1) {
		fileHandle.readPage(pageNum, page);
		if (slotNum > *((unsigned *) (page + PAGE_SIZE - 8)) || slotNum < 1) {
			delete[] page;
			return -1;
		}
		if (*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) == -1) { // the record has been deleted already
			delete[] page;
			return -1;
		} else if ((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) >> 12)
				& 1) { // if the 13th least significant bit is 1, then this int stores slotNum
			pageNum = *((unsigned *) (page + PAGE_SIZE - 8 * slotNum - 4)); // pageNum of the new location
			slotNum = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
					& EFFECTIVE_BITS; // the last 12 bits store the slotNum of the new location
			continue;
		} else {  // stores the offset
			int offset = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
					& EFFECTIVE_BITS;  // the last 12 bits store the offset
			// copy the specific attribute into "data", in required format
			unsigned AttrNum = recordDescriptor.size();	// number of attributes in the vector
			for (unsigned i = 0; i < AttrNum; i++) {
				if (recordDescriptor[i].name.compare(attributeName) == 0) {
					int attrOffset; // the beginning offset of the attribute
					if (i == 0)
						attrOffset = 4 * (AttrNum + 1);
					else
						attrOffset = *(int *) (page + offset + 4 * i);
					if (recordDescriptor[i].type == TypeInt) {
						*(int *) ((char *) data) = *(int *) (page + offset
								+ attrOffset); // read the int value
					} else if (recordDescriptor[i].type == TypeReal) {
						*(float *) ((char *) data) = *(float *) (page + offset
								+ attrOffset); // read the real value
					} else if (recordDescriptor[i].type == TypeVarChar) {
						unsigned varCharLength;
						varCharLength = *(int *) (page + offset + 4 * (i + 1))
								- attrOffset; //length of the varchar
						*(unsigned *) ((char *) data) = varCharLength;
						memcpy((char *) data + 4, page + offset + attrOffset,
								varCharLength);
					} else {
						delete[] page;
						return -1;
					}
					delete[] page;
					return 0;
				}
			}
			delete[] page;
			return -1;
		}
	}
}

//Given a record descriptor, reorganize a page, i.e., push the free space towards the end of the page.
RC RecordBasedFileManager::reorganizePage(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor, const unsigned pageNumber) {
	if (pageNumber >= fileHandle.getNumberOfPages())
		return -1;
	char * oldPage = new char[PAGE_SIZE];
	char * newPage = new char[PAGE_SIZE];
	fileHandle.readPage(pageNumber, oldPage);
	unsigned N = *((unsigned *) (oldPage + PAGE_SIZE - 8));
	memcpy(newPage + PAGE_SIZE - 8, oldPage + PAGE_SIZE - 8, 4); //copy the number of slots to the new page
	int newFreeSpaceOffset = 0; // initialize the new free space offset
	for (unsigned i = 1; i <= N; i++) {
		int offset = *((int *) (oldPage + PAGE_SIZE - 8 * (i + 1)));
		if (offset == -1) {   // simply copy -1
			*((int *) (newPage + PAGE_SIZE - 8 * (i + 1))) = -1;
		} else if ((offset >> 12) & 1) {  // simply copy the tombstone
			*((int *) (newPage + PAGE_SIZE - 8 * (i + 1))) = *((int *) (oldPage
					+ PAGE_SIZE - 8 * (i + 1)));
			*((unsigned *) (newPage + PAGE_SIZE - 8 * i - 4)) =
					*((unsigned *) (oldPage + PAGE_SIZE - 8 * i - 4));
		} else if ((offset >> 13) & 1) { // the slot has a forwarded record
			offset &= EFFECTIVE_BITS; // only the last 12 bits store the actual offset
			unsigned length = *((unsigned *) (oldPage + PAGE_SIZE - 8 * i - 4));
			*((unsigned *) (newPage + PAGE_SIZE - 8 * i - 4)) = length; // copy the record length
			memcpy(newPage + newFreeSpaceOffset, oldPage + offset, length); // copy the record
			*((int *) (newPage + PAGE_SIZE - 8 * (i + 1))) = newFreeSpaceOffset
					+ (1 << 13); // copy the record offset, set the 14th least significant bit to 1 to mark a forwarded record.
			newFreeSpaceOffset += length; // update the free space offset of the new page
		} else {  // the slot has a normal record
			unsigned length = *((unsigned *) (oldPage + PAGE_SIZE - 8 * i - 4));
			*((unsigned *) (newPage + PAGE_SIZE - 8 * i - 4)) = length; // copy the record length
			memcpy(newPage + newFreeSpaceOffset, oldPage + offset, length); // copy the record
			*((int *) (newPage + PAGE_SIZE - 8 * (i + 1))) = newFreeSpaceOffset; // copy the record offset
			newFreeSpaceOffset += length; // update the free space offset of the new page
		}
	}
	*((int *) (newPage + PAGE_SIZE - 4)) = newFreeSpaceOffset; // store the free space offset to the new page
	fileHandle.writePage(pageNumber, newPage); // flush the reorganized page to the disk
	delete[] oldPage;
	delete[] newPage;
	return 0;
}

// scan returns an iterator to allow the caller to go through the results one by one.
RC RecordBasedFileManager::scan(FileHandle &fileHandle,
		const vector<Attribute> &recordDescriptor,
		const string &conditionAttribute, const CompOp compOp, // comparison type such as "<" and "="
		const void *value, // used in the comparison
		const vector<string> &attributeNames, // a list of projected attributes
		RBFM_ScanIterator &rbfm_ScanIterator) {
	rbfm_ScanIterator.currentRID.pageNum = 0;
	rbfm_ScanIterator.currentRID.slotNum = 1;
	rbfm_ScanIterator.pFileHandle = &fileHandle;
	rbfm_ScanIterator.recordDescriptor = recordDescriptor;
	rbfm_ScanIterator.conditionAttribute = conditionAttribute;
	rbfm_ScanIterator.compOp = compOp;
	rbfm_ScanIterator.value = value;
	rbfm_ScanIterator.attrNames = attributeNames;
	rbfm_ScanIterator.pageInMemory = -1;
	rbfm_ScanIterator.N = -1;
	return 0;
}
