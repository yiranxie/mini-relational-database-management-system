#ifndef _pfm_h_
#define _pfm_h_

#include <fstream>

typedef int RC;
typedef unsigned PageNum;

#define PAGE_SIZE 4096
#define EFFECTIVE_BITS 07777   // equals to 2^12-1: the last 12 bits of an int to store the actual slot number for tombstone

class FileHandle;

class PagedFileManager {
public:
	static PagedFileManager* instance();   // Access to the _pf_manager instance

	RC createFile(const char *fileName);                    // Create a new file
	RC destroyFile(const char *fileName);                      // Destroy a file
	RC openFile(const char *fileName, FileHandle &fileHandle); // Open a file
	RC closeFile(FileHandle &fileHandle);                       // Close a file

protected:
	PagedFileManager();                                   // Constructor
	~PagedFileManager();                                  // Destructor

private:
	static PagedFileManager *_pf_manager;
};

class FileHandle {
public:
	// variables to keep counter for each operation
	unsigned readPageCounter;
	unsigned writePageCounter;
	unsigned appendPageCounter;

public:
	FileHandle();                                         // Default constructor
	~FileHandle();                                                 // Destructor

	RC readPage(PageNum pageNum, void *data);             // Get a specific page
	RC writePage(PageNum pageNum, const void *data);    // Write a specific page
	RC appendPage(const void *data);                   // Append a specific page
	unsigned getNumberOfPages();          // Get the number of pages in the file

	RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount,
			unsigned &appendPageCount);

	std::fstream fs;
};

#endif
