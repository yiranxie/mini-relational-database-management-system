#ifndef _rbfm_h_
#define _rbfm_h_

#include <string>
#include <vector>
#include <string.h>
#include <iostream>
#include <assert.h>

#include "pfm.h"

using namespace std;

// Record ID
typedef struct {
	unsigned pageNum;
	unsigned slotNum;
} RID;

// Attribute
typedef enum {
	TypeInt = 0, TypeReal, TypeVarChar
} AttrType;

typedef unsigned AttrLength;

struct Attribute {
	string name;     // attribute name
	AttrType type;     // attribute type
	AttrLength length; // attribute length
};

// Comparison Operator (NOT needed for part 1 of the project)
typedef enum {
	EQ_OP = 0,  // =
	LT_OP,      // <
	GT_OP,      // >
	LE_OP,      // <=
	GE_OP,      // >=
	NE_OP,      // !=
	NO_OP       // no condition
} CompOp;

/****************************************************************************
 The scan iterator is NOT required to be implemented for part 1 of the project
 *****************************************************************************/

# define RBFM_EOF (-1)  // end of a scan operator

// RBFM_ScanIterator is an iterator to go through records
// The way to use it is like the following:
//  RBFM_ScanIterator rbfmScanIterator;
//  rbfm.scan(..., rbfmScanIterator);
//  while (rbfmScanIterator.getNextRecord(rid, data) != RBFM_EOF) {
//    process the data;
//  }
//  rbfmScanIterator.close();

class RBFM_ScanIterator {
public:
	RID currentRID;
	int pageInMemory;   // page number of the page in memory
	FileHandle * pFileHandle;
	vector<Attribute> recordDescriptor;
	vector<string> attrNames;
	string conditionAttribute;
	CompOp compOp;
	const void * value;
	char page[PAGE_SIZE];
	unsigned N;	// total number of slots on the page

	RBFM_ScanIterator() {
		currentRID.pageNum = 0;
		currentRID.slotNum = 1;
		pageInMemory = -1;
		pFileHandle = 0;
		value = 0;
		compOp = NO_OP;
		N = 0;
	}

	~RBFM_ScanIterator() {
	}

	// test whether the attribute meets the condition
	bool meetConditionInt(int * attribute, int * value, CompOp compOp) {
		switch (compOp) {
		case EQ_OP:
			if (*attribute == *value)
				return 1;
			else
				return 0;
		case LT_OP:
			if (*attribute < *value)
				return 1;
			else
				return 0;
		case GT_OP:
			if (*attribute > *value)
				return 1;
			else
				return 0;
		case LE_OP:
			if (*attribute <= *value)
				return 1;
			else
				return 0;
		case GE_OP:
			if (*attribute >= *value)
				return 1;
			else
				return 0;
		case NE_OP:
			if (*attribute != *value)
				return 1;
			else
				return 0;
		default:
			return 1;
		}
	}

	bool meetConditionFloat(float * attribute, float * value, CompOp compOp) {
		switch (compOp) {
		case EQ_OP:
			if (*attribute == *value)
				return 1;
			else
				return 0;
		case LT_OP:
			if (*attribute < *value)
				return 1;
			else
				return 0;
		case GT_OP:
			if (*attribute > *value)
				return 1;
			else
				return 0;
		case LE_OP:
			if (*attribute <= *value)
				return 1;
			else
				return 0;
		case GE_OP:
			if (*attribute >= *value)
				return 1;
			else
				return 0;
		case NE_OP:
			if (*attribute != *value)
				return 1;
			else
				return 0;
		default:
			return 1;
		}
	}

	bool meetConditionStr(string attribute, string value, CompOp compOp) {
		switch (compOp) {
		case EQ_OP:
			if (attribute.compare(value) == 0)
				return 1;
			else
				return 0;
		case LT_OP:
			if (attribute.compare(value) < 0)
				return 1;
			else
				return 0;
		case GT_OP:
			if (attribute.compare(value) > 0)
				return 1;
			else
				return 0;
		case LE_OP:
			if (attribute.compare(value) <= 0)
				return 1;
			else
				return 0;
		case GE_OP:
			if (attribute.compare(value) >= 0)
				return 1;
			else
				return 0;
		case NE_OP:
			if (attribute.compare(value))
				return 1;
			else
				return 0;
		default:
			return 1;
		}
	}

	// "data" follows the same format as RecordBasedFileManager::insertRecord()
	RC getNextRecord(RID &rid, void *data) {
		if (pFileHandle == 0) {
			return RBFM_EOF;
		}

		unsigned pageNum = currentRID.pageNum;
		unsigned slotNum = currentRID.slotNum;
		while (1) {
			if (pageNum >= pFileHandle->getNumberOfPages())   // end of file
				return RBFM_EOF;
			if (pageNum != (unsigned) pageInMemory) { // the desired page not in memory
				pFileHandle->readPage(pageNum, page);
				pageInMemory = pageNum;
				N = *((unsigned *) (page + PAGE_SIZE - 8));
			}
			if (slotNum > N) {  // read in the next page
				pageNum++;
				slotNum = 1;
				continue;
			} else if (*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) == -1
					|| ((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) >> 13)
							& 1)) { // empty slot or slot which is forwarded, do not need to scan them
				slotNum++;
				continue;
			} else if (((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1))) >> 12)
					& 1) == 0) { //  "normal" slot: neither forwarding nor forwarded
				int offset = *((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)));
				unsigned AttrNum = recordDescriptor.size(); // number of attributes in the vector
				// test whether the condition is met
				bool met = 1;
				for (unsigned i = 0; i < AttrNum; i++) {
					if (recordDescriptor[i].name.compare(conditionAttribute)
							== 0) {
						if (recordDescriptor[i].type == TypeInt) {
							int * attribute;
							if (i == 0)
								attribute = (int *) (page + offset
										+ 4 * (AttrNum + 1));
							else
								attribute = (int *) (page + offset
										+ (*(int*) (page + offset + 4 * i)));
							if (!meetConditionInt(attribute, (int *) value,
									compOp)) {
								met = 0;
								break;
							}
						} else if (recordDescriptor[i].type == TypeReal) {
							float * attribute;
							if (i == 0)
								attribute = (float *) (page + offset
										+ 4 * (AttrNum + 1));
							else
								attribute = (float *) (page + offset
										+ (*(int*) (page + offset + 4 * i)));
							if (!meetConditionFloat(attribute, (float *) value,
									compOp)) {
								met = 0;
								break;
							}
						} else if (recordDescriptor[i].type == TypeVarChar) {
							// create attribute string
							int start;
							if (i == 0) {
								start = 4 * (AttrNum + 1);
							} else {
								start = *(int*) (page + offset + 4 * i);
							}
							int end = *(int*) (page + offset + 4 * i + 4);
							unsigned length = end - start;
							char ptr[PAGE_SIZE];  // temp char array
							memcpy(ptr, page + offset + start, length);
							ptr[length] = '\0';
							string attribute = ptr;
							// create s_value string
							length = *(int *) value; // length of value varChar
							memcpy(ptr, (char*)value + 4, length);
							ptr[length] = '\0';
							string s_value = ptr;
							if (!meetConditionStr(attribute, s_value, compOp)) {
								met = 0;
								break;
							}
						}
					}
				}
				if (met == 0) { // if not meet the condition, go to next slot
					slotNum++;
					continue;
				}

				// copy the record into "data", in required format
				int current = 4 * (AttrNum + 1); // initialize the starting Byte of "record" to read attributes' actual values
				int curData = 0; // initialize the staring Byte of "data" to store attributes
				for (unsigned j = 0; j < attrNames.size(); j++) {
					current = 4 * (AttrNum + 1); // initialize the starting Byte of "record" to read attributes' actual values
					for (unsigned i = 0; i < AttrNum; i++) {
						if (recordDescriptor[i].type == TypeInt) {
							// if not the desired attribute
							if (recordDescriptor[i].name.compare(
									attrNames[j])) {
								current += 4;
								continue;
							}
							*(int *) ((char *) data + curData) = *(int *) (page
									+ offset + current); //store the int value
							current += 4;
							curData += 4;
						} else if (recordDescriptor[i].type == TypeReal) {
							// if not the desired attribute
							if (recordDescriptor[i].name.compare(
									attrNames[j])) {
								current += 4;
								continue;
							}
							*(float *) ((char *) data + curData) =
									*(float *) (page + offset + current); //store the float value
							current += 4;
							curData += 4;
						} else if (recordDescriptor[i].type == TypeVarChar) {
							unsigned varCharLength;
							if (i != 0)
								varCharLength = *(int *) (page + offset
										+ 4 * (i + 1))
										- *(int *) (page + offset + 4 * i); //length of the varchar
							else
								varCharLength = *(int *) (page + offset + 4)
										- current;
							// if not the desired attribute
							if (recordDescriptor[i].name.compare(
									attrNames[j])) {
								current += varCharLength;
								continue;
							}
							*(unsigned *) ((char *) data + curData) =
									varCharLength;
							curData += 4;
							memcpy((char*)data + curData, page + offset + current,
									varCharLength);
							current += varCharLength;
							curData += varCharLength;
						} else
							return -1;
						break;
					}
				}
				rid.pageNum = pageNum;  // update rid
				rid.slotNum = slotNum;
				currentRID.pageNum = pageNum;   // update currentRID
				currentRID.slotNum = slotNum + 1;
				return 0;
			} else {   // a forwarding slot
				// store the original rid
				unsigned originPageNum = pageNum;
				unsigned originSlotNum = slotNum;
				int flag = 0; // flag=1 means the record exists (not deleted)
				int whetherMet = 1;  // indicates whether the condition is met
				while (1) {
					if (pageNum >= pFileHandle->getNumberOfPages()) // end of file
						return RBFM_EOF;
					if (pageNum != (unsigned) pageInMemory) { // the desired page not in memory
						pFileHandle->readPage(pageNum, page);
						pageInMemory = pageNum;
						N = *((unsigned *) (page + PAGE_SIZE - 8));
					}
					if (slotNum > N || slotNum < 1) {
						return -1;
					} else if (*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
							== -1) { // empty slot
						// go to the next slot
						pageNum = originPageNum;
						slotNum = originSlotNum + 1;
						break;
					} else if ((*((int *) (page + PAGE_SIZE - 8 * (slotNum + 1)))
							>> 12) & 1) { //  forwarding slot
						pageNum = *((unsigned *) (page + PAGE_SIZE - 8 * slotNum
								- 4)); // pageNum of the new location
						slotNum = *((int *) (page + PAGE_SIZE
								- 8 * (slotNum + 1))) & EFFECTIVE_BITS; // the last 12 bits store the slotNum of the new location
						continue;
					} else {  // "normal" slot or forwarded slot
						flag = 1; // the record exist
						int offset = *((int *) (page + PAGE_SIZE
								- 8 * (slotNum + 1))) & EFFECTIVE_BITS; // the last 12 bits store the offset
						unsigned AttrNum = recordDescriptor.size(); // number of attributes in the vector
						// test whether the condition is met
						bool met = 1;
						for (unsigned i = 0; i < AttrNum; i++) {
							if (recordDescriptor[i].name.compare(
									conditionAttribute) == 0) {
								if (recordDescriptor[i].type == TypeInt) {
									int * attribute;
									if (i == 0)
										attribute = (int *) (page + offset
												+ 4 * (AttrNum + 1));
									else
										attribute = (int *) (page + offset
												+ (*(int*) (page + offset
														+ 4 * i)));
									if (!meetConditionInt(attribute,
											(int *) value, compOp)) {
										met = 0;
										break;
									}
								} else if (recordDescriptor[i].type
										== TypeReal) {
									float * attribute;
									if (i == 0)
										attribute = (float *) (page + offset
												+ 4 * (AttrNum + 1));
									else
										attribute = (float *) (page + offset
												+ (*(int*) (page + offset
														+ 4 * i)));
									if (!meetConditionFloat(attribute,
											(float *) value, compOp)) {
										met = 0;
										break;
									}
								} else if (recordDescriptor[i].type
										== TypeVarChar) {
									// make attribute string
									int start;
									if (i == 0) {
										start = 4 * (AttrNum + 1);
									} else {
										start = *(int*) (page + offset + 4 * i);
									}
									int end =
											*(int*) (page + offset + 4 * i + 4);
									unsigned length = end - start;
									char ptr[PAGE_SIZE];  // temp char array
									memcpy(ptr, page + offset + start, length);
									ptr[length] = '\0';
									string attribute = ptr;
									// make s_value string
									length = *(int *) value; // length of value varChar
									memcpy(ptr, (char*)value + 4, length);
									ptr[length] = '\0';
									string s_value = ptr;
									if (!meetConditionStr(attribute, s_value,
											compOp)) {
										met = 0;
										break;
									}
								}
							}
						}
						if (met == 0) {
							whetherMet = 0;
							break;
						}

						// copy the record into "data", in required format
						int current = 4 * (AttrNum + 1); // initialize the starting Byte of "record" to read attributes' actual values
						int curData = 0; // initialize the staring Byte of "data" to store attributes
						for (unsigned j = 0; j < attrNames.size(); j++) {
							current = 4 * (AttrNum + 1); // initialize the starting Byte of "record" to read attributes' actual values
							for (unsigned i = 0; i < AttrNum; i++) {
								if (recordDescriptor[i].type == TypeInt) {
									// if not the desired attribute
									if (recordDescriptor[i].name.compare(
											attrNames[j])) {
										current += 4;
										continue;
									}
									*(int *) ((char *) data + curData) =
											*(int *) (page + offset + current); //store the int value
									current += 4;
									curData += 4;
								} else if (recordDescriptor[i].type
										== TypeReal) {
									// if not the desired attribute
									if (recordDescriptor[i].name.compare(
											attrNames[j])) {
										current += 4;
										continue;
									}
									*(float *) ((char *) data + curData) =
											*(float *) (page + offset + current); //store the float value
									current += 4;
									curData += 4;
								} else if (recordDescriptor[i].type
										== TypeVarChar) {
									unsigned varCharLength;
									if (i != 0)
										varCharLength = *(int *) (page + offset
												+ 4 * (i + 1))
												- *(int *) (page + offset
														+ 4 * i); //length of the varchar
									else
										varCharLength = *(int *) (page + offset
												+ 4) - current;
									// if not the desired attribute
									if (recordDescriptor[i].name.compare(
											attrNames[j])) {
										current += varCharLength;
										continue;
									}
									*(unsigned *) ((char *) data + curData) =
											varCharLength;
									curData += 4;
									memcpy((char*)data + curData,
											page + offset + current,
											varCharLength);
									current += varCharLength;
									curData += varCharLength;
								} else
									return -1;
								break;
							}
						}
					}
					if (whetherMet == 0) { // if not meet the condition, go to the next record
						slotNum++;
						continue;
					}
					if (flag) { // if the record exists
						rid.pageNum = originPageNum;  // update rid
						rid.slotNum = originSlotNum;
						currentRID.pageNum = originPageNum; // update currentRID
						currentRID.slotNum = originSlotNum + 1;
						return 0;
					}
				}
			}
		}
	}

	RC close() {
		currentRID.pageNum = -1;
		currentRID.slotNum = -1;
		N = 0;
		pageInMemory = -1;
		pFileHandle = 0;
		recordDescriptor.clear();
		attrNames.clear();
		return 0;
	}

}
;

/**
 * This function will convert the data in in-memory format, from values
 */

struct MultiTypeValue {
	AttrType type;
	string strValue;
	int intValue;
	float floatValue;
};

/**
 * Make sure the type of values are defined
 */
RC generateDataInMemoryFormat(const char * data, unsigned maxSize,
		vector<MultiTypeValue> & values);
//{
//
//	char * ptr = (char *) data;
//	unsigned currentSize = 0;
//	for (unsigned i = 0; i < values.size(); ++i) {
//		MultiTypeValue & thisValue = values[i];
//		if (thisValue.type == TypeInt) {
//			currentSize += 4;
//			if (currentSize > maxSize) {
//				return -1;
//			}
//			*((int*) ptr) = thisValue.intValue;
//			ptr += 4;
//		} else if (thisValue.type == TypeReal) {
//			currentSize += 4;
//			if (currentSize > maxSize) {
//				return -1;
//			}
//			*((int*) ptr) = thisValue.floatValue;
//			ptr += 4;
//		} else if (thisValue.type == TypeVarChar) {
//			currentSize += (4 + thisValue.strValue.length());
//			if (currentSize > maxSize) {
//				return -1;
//			}
//			*((int *) ptr) = thisValue.strValue.length();
//			ptr += 4;
//			memcpy(ptr, thisValue.strValue.c_str(),
//					thisValue.strValue.length());
//			ptr += thisValue.strValue.length();
//		}
//	}
//	return 0;
//}

struct FormattedRecord{
	vector<MultiTypeValue> values;
};

void readRecordFromRawData(const void * data, const vector<Attribute> attrs, FormattedRecord & record);
//{
//	record.values.clear();
//	unsigned totalLength = 0;
//	unsigned totalAttributes = attrs.size();
//	record.values.resize(totalAttributes);
//	char temp[PAGE_SIZE];
//	const char * pdata = (const char *)data;
//	for (unsigned i=0; i<attrs.size(); ++i){
//		const Attribute & thisAttr = attrs[i];
//		MultiTypeValue & thisValue = record.values[i];
//		thisValue.type = thisAttr.type;
//		if (thisAttr.type == TypeInt){
//			memcpy(&thisValue.intValue, pdata+totalLength, sizeof(int));
//			totalLength += sizeof(int);
//		}else if (thisAttr.type == TypeReal){
//			memcpy(&thisValue.floatValue, pdata+totalLength, sizeof(float));
//			totalLength += sizeof(float);
//		}else{
//			unsigned charLength = 0;
//			memcpy(&charLength, pdata+totalLength, sizeof(unsigned));
//			totalLength += sizeof(unsigned);
//			memcpy(temp, pdata+totalLength, charLength);
//			temp[charLength] = '\0';
//			thisValue.strValue = temp;
//			totalLength += charLength;
//		}
//	}
//}

class RecordBasedFileManager {
public:
	static RecordBasedFileManager* instance();

	RC createFile(const string &fileName);

	RC destroyFile(const string &fileName);

	RC openFile(const string &fileName, FileHandle &fileHandle);

	RC closeFile(FileHandle &fileHandle);

	//  Format of the data passed into the function is the following:
	//  1) data is a concatenation of values of the attributes
	//  2) For int and real: use 4 bytes to store the value;
	//     For varchar: use 4 bytes to store the length of characters, then store the actual characters.
	//  !!!The same format is used for updateRecord(), the returned data of readRecord(), and readAttribute()

	RC insertRecord(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor, const void *data,
			RID &rid);

	RC readRecord(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor, const RID &rid,
			void *data);

	// This method will be mainly used for debugging/testing
	RC printRecord(const vector<Attribute> &recordDescriptor, const void *data);

	/**************************************************************************************************************************************************************
	 ***************************************************************************************************************************************************************
	 IMPORTANT, PLEASE READ: All methods below this comment (other than the constructor and destructor) are NOT required to be implemented for part 1 of the project
	 ***************************************************************************************************************************************************************
	 ***************************************************************************************************************************************************************/
	RC deleteRecords(FileHandle &fileHandle);

	RC deleteRecord(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor, const RID &rid);

	// Assume the rid does not change after update
	RC updateRecord(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor, const void *data,
			const RID &rid);

	RC readAttribute(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor, const RID &rid,
			const string attributeName, void *data);

	RC reorganizePage(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor,
			const unsigned pageNumber);

	// scan returns an iterator to allow the caller to go through the results one by one.
	RC scan(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor,
			const string &conditionAttribute, const CompOp compOp, // comparison type such as "<" and "="
			const void *value,                 // used in the comparison
			const vector<string> &attributeNames, // a list of projected attributes
			RBFM_ScanIterator &rbfm_ScanIterator);

// Extra credit for part 2 of the project, please ignore for part 1 of the project
public:

	RC reorganizeFile(FileHandle &fileHandle,
			const vector<Attribute> &recordDescriptor);

protected:
	RecordBasedFileManager();
	~RecordBasedFileManager();

private:
	static RecordBasedFileManager *_rbf_manager;
};

#endif
