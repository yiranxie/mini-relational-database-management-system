This is the project for a data management course.

There are four parts:

(1) file/record manager (rbf directory)
(2) index manager (ix directory)
(3) relation manager (rm directory)
(4) query engine (qe directory)
