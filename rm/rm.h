#ifndef _rm_h_
#define _rm_h_

#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "../rbf/rbfm.h"
#include "../ix/ix.h"

using namespace std;

/******************************************
 * Parameters Definition
 ******************************************/
#define INDEX_FILE_SEPARATOR '.'

/******************************************
 * Error Code Definition
 ******************************************/
#define RM_INDEX_CREATE_ERROR_TABLE_NOT_EXIST 1001
#define RM_INDEX_CREATE_ERROR_ATTRIBUTE_NOT_EXIST 1002
#define RM_INDEX_CREATE_ERROR_INDEX_ALREADY_EXIST 1003
#define RM_INDEX_DESTORY_ERROR_TABLE_NOT_EXIST 1011
#define RM_INDEX_DESTORY_ERROR_ATTRIBUTE_NOT_EXIST 1012
#define RM_INDEX_DESTORY_ERROR_INDEX_NOT_EXIST 1013
#define RM_INDEX_SCAN_ERROR_TABLE_NOT_EXIST 1021
#define RM_INDEX_SCAN_ERROR_ATTRIBUTE_NOT_EXIST 1022
#define RM_INDEX_SCAN_ERROR_INDEX_NOT_EXIST 1023

typedef enum {
	StatusNormal, StatusDeleted
} AttrStatus;

/*
 * Name the index file
 * Format:
 * 		tableName + INDEX_FILE_SEPARATOR + attributeName + INDEX_FILE_SEPARATOR + "index"
 */
void getIndexName(const string &tableName, const string &attributeName, string & indexName);
//{
//	indexName.clear();
//	indexName.append(tableName);
//	indexName.append(1, INDEX_FILE_SEPARATOR);
//	indexName.append(attributeName);
//	indexName.append(1, INDEX_FILE_SEPARATOR);
//	indexName.append("index");
//}

struct RMColumn {
	string cname;
	AttrType ctype;
	AttrLength clength;
	AttrStatus cstatus;
	int position;
	RID rid; // a record of Columns table
};

class RMIndex{
public:
	string iname;
	IXFileHandle ixFileHandle;
	RMIndex(){
	}
	RMIndex(string iname){
		this->iname = iname;
	}
//	RMIndex(string tableName, string attrName){
//		iname = tableName + "." + attrName;
//	}
	~RMIndex(){
		ixFileHandle.primaryHandle.fs.close();
		ixFileHandle.metadataHandle.fs.close();
	}
};

class RMTable {
public:
	int tid; // table id
	string tname; // table name
	vector<RMColumn> tcolumns; // Columns
	string tfilename; // file name
	FileHandle * tfileHandle;
	int status; // 1 means system table, 0 means user table
	RID rid; // a record of Tables table
	map<string, RMIndex> indices;	//name=>index name's format is tname.columnname (RMIndex)
	RMTable() {
		tid = -1;
		status = -1;
		tfileHandle = 0;
	}
	void detectIndices(){	//Only invoked when the table is initialized!
		unsigned columnNum = tcolumns.size();
		for (unsigned i=0; i<columnNum; ++i){
			RMColumn & thisColumn = tcolumns[i];
			string thisIndexName;
			getIndexName(tname, thisColumn.cname, thisIndexName);
			if (IndexManager::instance()->detectIndex(thisIndexName)){
				RMIndex & thisIndex = indices[thisIndexName];
				thisIndex.iname = thisIndexName;
				IndexManager::instance()->openFile(thisIndex.iname, thisIndex.ixFileHandle);
			}
		}
	}
	void print() {		//For testing
		cout << "**************Table " << tname << "***********" << endl;
		cout << "tid: " << tid << endl;
		cout << "tname: " << tname << endl;
		cout << "tfilename: " << tfilename << endl;
		cout << "status: " << status << endl;
		cout << "rid: " << rid.pageNum << "," << rid.slotNum << endl;
		if (tfileHandle == 0) {
			cout << "tfileHandle: NULL" << endl;
		} else {
			cout << "tfileHandle: NOT NULL" << endl;
		}
		for (unsigned i = 0; i < tcolumns.size(); ++i) {
			cout << "\tcolumn " << i << ": " << tcolumns[i].cname << ", type:"
					<< tcolumns[i].ctype << ", length:" << tcolumns[i].clength
					<< endl;
		}
		cout << "*****************************************" << endl;
	}
	~RMTable() {
		if (tfileHandle != 0){
			tfileHandle->fs.close();
			delete tfileHandle;
		}

		for (map<string, RMIndex>::iterator itor=indices.begin(); itor!=indices.end(); ++itor){
			IndexManager::instance()->closeFile(itor->second.ixFileHandle);
		}
	}
};

/**
 * In Catalog, we need store **TABLE** information
 * Each table contains
 * 1. table name
 * 2. columns (name, type and length)
 * 3. the corresponding record-based file (at least one file / table)
 */
class Catalog {
public:
	/**
	 * Catalog cached in memory
	 */
	map<string, RMTable> tableMap;

	string catalog_tablename;
	string catalog_columnname;

	vector<Attribute> tableAttrs;
	vector<Attribute> columnAttrs;

	FileHandle * tFH;
	FileHandle * cFH;

	unsigned tableMaxId;

	static const unsigned TableNameMaxLength = 1024;
	static const unsigned FileNameMaxLength = 1024;
	static const unsigned ColumnNameMaxLength = 1024;

	static const int RegularTable = 0;
	static const int SystemTable = 1;

	/**
	 * permanent level,
	 * Table table
	 * 		table id, table name, file name ==> record contains 3 fields (int, varchar, varchar)
	 * Column table
	 * 		table-id, column-name, column-type, column-length, column-status ==> record contains 4 fields (int, varchar, int, int, int)
	 *
	 */
	Catalog() {
		tableMaxId = 0;
		catalog_tablename = "Tables";
		catalog_columnname = "Columns";

		char * data = new char[PAGE_SIZE];

		/**
		 * table-id, column-name, column-type, column-length, column-status
		 */
		Attribute att_tableid;
		att_tableid.name = "table-id";
		att_tableid.length = 4;
		att_tableid.type = TypeInt;
		Attribute att_columnname;
		att_columnname.name = "column-name";
		att_columnname.length = ColumnNameMaxLength;
		att_columnname.type = TypeVarChar;
		Attribute att_columntype;
		att_columntype.name = "column-type";
		att_columntype.length = 4;
		att_columntype.type = TypeInt;
		Attribute att_columnlength;
		att_columnlength.name = "column-length";
		att_columnlength.length = 4;
		att_columnlength.type = TypeInt;
		Attribute att_columnstatus;
		att_columnstatus.name = "column-status";
		att_columnstatus.length = 4;
		att_columnstatus.type = TypeInt;
		Attribute att_columnposition;
		att_columnposition.name = "column-position";
		att_columnposition.length = 4;
		att_columnstatus.type = TypeInt;

		columnAttrs.push_back(att_tableid);
		columnAttrs.push_back(att_columnname);
		columnAttrs.push_back(att_columntype);
		columnAttrs.push_back(att_columnlength);
		columnAttrs.push_back(att_columnstatus);
		columnAttrs.push_back(att_columnposition);

		/**
		 * table-id, int
		 * table-name, varchar
		 * file-name, varchar
		 * table-status, int
		 */

		Attribute att_tablename;
		att_tablename.name = "table-name";
		att_tablename.length = TableNameMaxLength;
		att_tablename.type = TypeVarChar;
		Attribute att_filename;
		att_filename.name = "file-name";
		att_filename.length = FileNameMaxLength;
		att_filename.type = TypeVarChar;
		Attribute att_tablestatus;
		att_tablestatus.name = "table-status";
		att_tablestatus.length = 4;
		att_tablestatus.type = TypeInt;

		tableAttrs.push_back(att_tableid);
		tableAttrs.push_back(att_tablename);
		tableAttrs.push_back(att_filename);
		tableAttrs.push_back(att_tablestatus);

		string tableFilename = catalog_tablename + ".tab";
		string columnFilename = catalog_columnname + ".tab";
		tFH = new FileHandle();
		cFH = new FileHandle();

		if (RecordBasedFileManager::instance()->openFile(tableFilename, *tFH)
				!= 0
				|| RecordBasedFileManager::instance()->openFile(columnFilename,
						*cFH) != 0) {
			/**
			 * table file or column file doesn't exists
			 * so we rebuild the catalog
			 */
			RecordBasedFileManager::instance()->destroyFile(tableFilename);
			RecordBasedFileManager::instance()->destroyFile(columnFilename);
			RecordBasedFileManager::instance()->createFile(tableFilename);
			RecordBasedFileManager::instance()->createFile(columnFilename);
			RecordBasedFileManager::instance()->openFile(tableFilename, *tFH);
			RecordBasedFileManager::instance()->openFile(columnFilename, *cFH);

			/**
			 * Part 1. Build TABLE table
			 */
			vector<MultiTypeValue> values;
			MultiTypeValue value_tableTID;
			value_tableTID.type = TypeInt;
			int tableTableID = tableMaxId++;
			value_tableTID.intValue = tableTableID;
			MultiTypeValue value_tableName;
			value_tableName.type = TypeVarChar;
			value_tableName.strValue = catalog_tablename;
			MultiTypeValue value_tableFilename;
			value_tableFilename.type = TypeVarChar;
			value_tableFilename.strValue = tableFilename;
			MultiTypeValue value_tableStatus;
			value_tableStatus.type = TypeInt;
			value_tableStatus.intValue = SystemTable;

			values.push_back(value_tableTID);
			values.push_back(value_tableName);
			values.push_back(value_tableFilename);
			values.push_back(value_tableStatus);
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RID tableTableRID;
			RecordBasedFileManager::instance()->insertRecord(*tFH, tableAttrs,
					data, tableTableRID);

			int columnTableID = tableMaxId++;
			values[0].intValue = columnTableID;
			values[1].strValue = catalog_columnname;
			values[2].strValue = columnFilename;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RID columnTableRID;
			RecordBasedFileManager::instance()->insertRecord(*tFH, tableAttrs,
					data, columnTableRID);

			/**
			 * Part 2. Build COLUMN table
			 * 2.1 columns in TABLE table
			 */
			int columnPosition = 0;
			MultiTypeValue value_tableID;
			value_tableID.type = TypeInt;
			value_tableID.intValue = tableTableID;
			MultiTypeValue value_columnName;
			value_columnName.type = TypeVarChar;
			value_columnName.strValue = "table-id";
			MultiTypeValue value_columnType;
			value_columnType.type = TypeInt;
			value_columnType.intValue = 0;
			MultiTypeValue value_columnLength;
			value_columnLength.type = TypeInt;
			value_columnLength.intValue = 4;
			MultiTypeValue value_columnStatus;
			value_columnStatus.type = TypeInt;
			value_columnStatus.intValue = 0;
			MultiTypeValue value_columnPosition;
			value_columnPosition.type = TypeInt;
			value_columnPosition.intValue = columnPosition++;
			values.clear();
			values.push_back(value_tableID);
			values.push_back(value_columnName);
			values.push_back(value_columnType);
			values.push_back(value_columnLength);
			values.push_back(value_columnStatus);
			values.push_back(value_columnPosition);
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RID tempRID;
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "table-name";
			values[2].intValue = 2;
			values[3].intValue = TableNameMaxLength;
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "file-name";
			values[3].intValue = FileNameMaxLength;
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "table-status";
			values[2].intValue = 0;
			values[3].intValue = 4;
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			/**
			 * 2.2 columns in COLUMN tables
			 */
			columnPosition = 0;
			values[0].intValue = columnTableID;
			values[1].strValue = "table-id";
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "column-name";
			values[2].intValue = 2;
			values[3].intValue = ColumnNameMaxLength;
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "column-type";
			values[2].intValue = 0;
			values[3].intValue = 4;
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "column-length";
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "column-status";
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values[1].strValue = "column-position";
			values[5].intValue = columnPosition++;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			RecordBasedFileManager::instance()->insertRecord(*cFH, columnAttrs,
					data, tempRID);

			values.clear();

			RecordBasedFileManager::instance()->closeFile(*tFH);
			RecordBasedFileManager::instance()->closeFile(*cFH);
		}

		RecordBasedFileManager::instance()->openFile(tableFilename, *tFH);
		RecordBasedFileManager::instance()->openFile(columnFilename, *cFH);

		string condition = "";

		vector<string> attributeNames;
		for (unsigned i = 0; i < columnAttrs.size(); ++i) {
			attributeNames.push_back(columnAttrs[i].name);
		}
		RBFM_ScanIterator rbfm_ScanIterator;

		RecordBasedFileManager::instance()->scan(*cFH, columnAttrs, condition,
				NO_OP, 0, attributeNames, rbfm_ScanIterator);

		RID rid;
		map<int, vector<RMColumn> > tid_columns;

		while (rbfm_ScanIterator.getNextRecord(rid, data) != RBFM_EOF) {

			/**
			 * table-id int
			 * column-name varchar
			 * column-type int
			 * column-length int
			 * column-status int
			 * column-position int
			 */
			char * ptr = data;
			int tableId = *((int *) ptr);
			ptr += sizeof(int);
			unsigned cnameLength = *((unsigned *) ptr);
			ptr += sizeof(int);
			char cname[cnameLength + 1];
			memcpy(cname, ptr, cnameLength);
			cname[cnameLength] = '\0';
			ptr += cnameLength;
			int columnType = *((int *) ptr);
			ptr += sizeof(int);
			int columnLength = *((int *) ptr);
			ptr += sizeof(int);
			int columnStatus = *((int *) ptr);
			ptr += sizeof(int);
			int columnPosition = *((int *) ptr);

			RMColumn newColumn;
			newColumn.rid = rid;
			newColumn.cname = cname;
			if (columnStatus == 0) {
				newColumn.cstatus = StatusNormal;
			} else {
				newColumn.cstatus = StatusDeleted;
			}
			newColumn.clength = columnLength;
			newColumn.position = columnPosition;
			if (columnType == 0) {
				newColumn.ctype = TypeInt;
			} else if (columnType == 1) {
				newColumn.ctype = TypeReal;
			} else if (columnType == 2) {
				newColumn.ctype = TypeVarChar;
			} else {
				cout
						<< "Unknown type when we initialize the column tables in catalog!!!!!"
						<< endl;
			}
			tid_columns[tableId].push_back(newColumn);
		}
		rbfm_ScanIterator.close();

		attributeNames.clear();
		for (unsigned i = 0; i < tableAttrs.size(); ++i) {
			attributeNames.push_back(tableAttrs[i].name);
		}
		RecordBasedFileManager::instance()->scan(*tFH, tableAttrs, condition,
				NO_OP, 0, attributeNames, rbfm_ScanIterator);

		tableMaxId = 0;
		while (rbfm_ScanIterator.getNextRecord(rid, data) != RBFM_EOF) {

			/**
			 * table-id int
			 * table-name varchar
			 * file-name varchar
			 * table-status int
			 */
			char * ptr = data;
			int tableId = *((int *) ptr);
			ptr += sizeof(int);
			unsigned tnameLength = *((unsigned *) ptr);
			ptr += sizeof(int);
			char tname[tnameLength + 1];
			memcpy(tname, ptr, tnameLength);
			tname[tnameLength] = '\0';
			ptr += tnameLength;

			unsigned fnameLength = *((unsigned*) ptr);
			ptr += sizeof(int);
			char fname[fnameLength + 1];
			memcpy(fname, ptr, fnameLength);
			fname[fnameLength] = '\0';
			ptr += fnameLength;

			int tableStatus = *((int *) ptr);

			RMTable & table = tableMap[tname];
			table.tid = tableId;
			if ((unsigned) tableId > tableMaxId) {
				tableMaxId = tableId;
			}
			table.tname = tname;
			table.tfilename = fname;
			table.status = tableStatus;
			if (table.tname.compare(catalog_tablename) == 0) {
				table.tfileHandle = tFH;

			} else if (table.tname.compare(catalog_columnname) == 0) {
				table.tfileHandle = cFH;
			} else {
				table.tfileHandle = new FileHandle();
				RecordBasedFileManager::instance()->openFile(fname,
						*(table.tfileHandle));
			}

			table.tcolumns = tid_columns[tableId];
			table.rid = rid;
			table.detectIndices();		//detect the indices of this table
		}
		rbfm_ScanIterator.close();

		delete[] data;

	}

	~Catalog() {

	}

};

# define RM_EOF (-1)  // end of a scan operator


class RM_IndexScanIterator {
public:
	IX_ScanIterator ixItor;
	RMIndex * pIndex;

	RM_IndexScanIterator() {
		pIndex = 0;
	};  	// Constructor
	~RM_IndexScanIterator() {
	}; 	// Destructor

	// "key" follows the same format as in IndexManager::insertEntry()
	RC getNextEntry(RID &rid, void *key) {
		return ixItor.getNextEntry(rid, key);
	};  	// Get next matching entry
	RC close() {
		pIndex = 0;
		return ixItor.close();
	};             			// Terminate index scan
};

// RM_ScanIterator is an iterator to go through tuples
// The way to use it is like the following:
//  RM_ScanIterator rmScanIterator;
//  rm.open(..., rmScanIterator);
//  while (rmScanIterator(rid, data) != RM_EOF) {
//    process the data;
//  }
//  rmScanIterator.close();

class RM_ScanIterator {
public:
	bool exactMatch;

	//For Regular RM Iterator
	RBFM_ScanIterator baseIter;

	//For Exact Match RM Iterator
	RM_IndexScanIterator indexIter;
	string tableName;
	vector<Attribute> attrs;
	vector<unsigned> outputOrders;

	RM_ScanIterator() {
		exactMatch = false;
	};
	~RM_ScanIterator() {
	};

	// "data" follows the same format as RelationManager::insertTuple()
	RC getNextTuple(RID &rid, void *data);

	RC close() {
		if (exactMatch){
			return indexIter.close();
		}else{
			return baseIter.close();
		}
	}
	;
};


// Relation Manager
class RelationManager {
public:
	static RelationManager* instance();

	Catalog catalog;

	RC createTable(const string &tableName, const vector<Attribute> &attrs);

	RC deleteTable(const string &tableName);

	RC getAttributes(const string &tableName, vector<Attribute> &attrs);

	RC insertTuple(const string &tableName, const void *data, RID &rid);

	RC deleteTuples(const string &tableName);

	RC deleteTuple(const string &tableName, const RID &rid);

	// Assume the rid does not change after update
	RC updateTuple(const string &tableName, const void *data, const RID &rid);

	RC readTuple(const string &tableName, const RID &rid, void *data);

	RC readAttribute(const string &tableName, const RID &rid,
			const string &attributeName, void *data);

	RC reorganizePage(const string &tableName, const unsigned pageNumber);

	//TODO
	// scan returns an iterator to allow the caller to go through the results one by one.
	RC scan(const string &tableName, const string &conditionAttribute,
			const CompOp compOp,         // comparison type such as "<" and "="
			const void *value,                    // used in the comparison
			const vector<string> &attributeNames, // a list of projected attributes
			RM_ScanIterator &rm_ScanIterator);

	RC createIndex(const string &tableName, const string &attributeName);

	RC destroyIndex(const string &tableName, const string &attributeName);

	// indexScan returns an iterator to allow the caller to go through qualified entries in index
	RC indexScan(const string &tableName,
	      const string &attributeName,
	      const void *lowKey,
	      const void *highKey,
	      bool lowKeyInclusive,
	      bool highKeyInclusive,
	      RM_IndexScanIterator &rm_IndexScanIterator);

// Extra credit
public:
	RC dropAttribute(const string &tableName, const string &attributeName);

	RC addAttribute(const string &tableName, const Attribute &attr);

	RC reorganizeTable(const string &tableName);

protected:
	RelationManager();
	~RelationManager();

private:
	static RelationManager *_rm;
};

#endif
