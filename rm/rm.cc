
#include "rm.h"

void getIndexName(const string &tableName, const string &attributeName, string & indexName){
	indexName.clear();
	if (attributeName.find(".") == string::npos){
		indexName.append(tableName);
		indexName.append(1, INDEX_FILE_SEPARATOR);
	}
	indexName.append(attributeName);
	indexName.append(1, INDEX_FILE_SEPARATOR);
	indexName.append("index");
}

RelationManager* RelationManager::_rm = 0;

RC RM_ScanIterator::getNextTuple(RID &rid, void *data) {
	if (exactMatch){
		char temp[PAGE_SIZE];
		RC rc = indexIter.getNextEntry(rid, temp);
		if (rc != 0){
			return rc;
		}
		rc = RelationManager::instance()->readTuple(tableName, rid, temp);
		if (rc != 0){
			return rc;
		}
		FormattedRecord thisTuple;
		readRecordFromRawData(temp, attrs, thisTuple);
		unsigned totalOutputAttr = outputOrders.size();
		unsigned totalLength = 0;
		char * pdata = (char*)data;
		for (unsigned i=0; i<totalOutputAttr; ++i){
			Attribute & thisAttr = attrs[outputOrders[i]];
			if (thisAttr.type == TypeInt){
				memcpy(pdata+totalLength, &thisTuple.values[outputOrders[i]].intValue, sizeof(int));
				totalLength += sizeof(int);
			}else if (thisAttr.type == TypeReal){
				memcpy(pdata+totalLength, &thisTuple.values[outputOrders[i]].floatValue, sizeof(float));
				totalLength += sizeof(float);
			}else{
				unsigned charLength = thisTuple.values[outputOrders[i]].strValue.size();
				memcpy(pdata+totalLength, &charLength, sizeof(unsigned));
				totalLength += sizeof(unsigned);
				memcpy(pdata+totalLength, thisTuple.values[outputOrders[i]].strValue.c_str(), charLength);
				totalLength += charLength;
			}
		}
		return 0;
	}else{
		return baseIter.getNextRecord(rid, data);
	}
}

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}


RelationManager::RelationManager()
{
}

RelationManager::~RelationManager()
{
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
	if (catalog.tableMap.find(tableName) !=  catalog.tableMap.end()){
		/**
		 * Some table with the same name already exists
		 */
		return -1;
	}else{
		RMTable & thisTable = catalog.tableMap[tableName];
		thisTable.tid = ++(catalog.tableMaxId);
		thisTable.tname = tableName;
		thisTable.tfilename = tableName + ".tab";
		thisTable.tfileHandle = new FileHandle();
		thisTable.status = Catalog::RegularTable;
		int result = RecordBasedFileManager::instance()->createFile(thisTable.tfilename.c_str());
		result = RecordBasedFileManager::instance()->openFile(thisTable.tfilename.c_str(), *(thisTable.tfileHandle));
		if (result != 0){
			return result;
		}
		char data[PAGE_SIZE];
		vector<MultiTypeValue> values;
		values.resize(4);
		values[0].type = TypeInt;
		values[0].intValue = thisTable.tid;
		values[1].type = TypeVarChar;
		values[1].strValue = thisTable.tname;
		values[2].type = TypeVarChar;
		values[2].strValue = thisTable.tfilename;
		values[3].type = TypeInt;
		values[3].intValue = Catalog::RegularTable;
		generateDataInMemoryFormat(data, PAGE_SIZE, values);
		result = insertTuple(catalog.catalog_tablename, data, thisTable.rid);
		if (result != 0){
			return result;
		}

		values.clear();
		values.resize(6);

		vector<RMColumn> & columns = thisTable.tcolumns;
		columns.resize(attrs.size());

		for (unsigned i=0; i<attrs.size(); ++i){

			columns[i].clength = attrs[i].length;
			columns[i].cname = attrs[i].name;
			columns[i].cstatus = StatusNormal;
			columns[i].ctype = attrs[i].type;
			columns[i].position = i;

			values[0].type = TypeInt;
			values[0].intValue = thisTable.tid;
			values[1].type = TypeVarChar;
			values[1].strValue = attrs[i].name;
			values[2].type = TypeInt;
			values[2].intValue = attrs[i].type;
			values[3].type = TypeInt;
			values[3].intValue = attrs[i].length;
			values[4].type = TypeInt;
			values[4].intValue = 0;
			values[5].type = TypeInt;
			values[5].intValue = i;
			generateDataInMemoryFormat(data, PAGE_SIZE, values);
			result = insertTuple(catalog.catalog_columnname, data, columns[i].rid);
			if (result != 0){
				return result;
			}
		}
	}
    return 0;
}

RC RelationManager::deleteTable(const string &tableName)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
				tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;
		if (thisTable.status != Catalog::RegularTable){
			return -2;
		}

		//delete the indices
		map<string, RMIndex> & indices = thisTable.indices;
		for (map<string, RMIndex>::iterator itor=indices.begin(); itor!=indices.end(); ++itor){
			IndexManager::instance()->closeFile(itor->second.ixFileHandle);
			IndexManager::instance()->destroyFile(itor->second.iname);
		}

		vector<Attribute> attrs;
		getAttributes(catalog.catalog_tablename, attrs);
		RMTable & tableTable = catalog.tableMap[catalog.catalog_tablename];
		int result = RecordBasedFileManager::instance()->deleteRecord(*(tableTable.tfileHandle), attrs, thisTable.rid);
		if (result != 0){
			return result;
		}
		attrs.clear();
		getAttributes(catalog.catalog_columnname, attrs);
		RMTable & columnTable = catalog.tableMap[catalog.catalog_columnname];
		for (unsigned i=0; i<thisTable.tcolumns.size(); ++i){
			result = RecordBasedFileManager::instance()->deleteRecord(*(columnTable.tfileHandle), attrs, thisTable.tcolumns[i].rid);
			if (result != 0){
				return result;
			}
		}
		RecordBasedFileManager::instance()->closeFile(*thisTable.tfileHandle);
		RecordBasedFileManager::instance()->destroyFile(thisTable.tfilename);
		catalog.tableMap.erase(tableName);
		return 0;
	}
}

RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
			tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;
		for (unsigned i = 0; i < thisTable.tcolumns.size(); ++i) {
			Attribute thisAttr;
			thisAttr.length = thisTable.tcolumns[i].clength;
			thisAttr.name = thisTable.tcolumns[i].cname;
			thisAttr.type = thisTable.tcolumns[i].ctype;
			attrs.push_back(thisAttr);
		}
		return 0;
	}
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(tableName);
	if (iter == catalog.tableMap.end()){
		return -1;
	}else{
		RMTable & thisTable = iter->second;
		vector<Attribute> attrs;
		getAttributes(tableName, attrs);
		RC rc = RecordBasedFileManager::instance()->insertRecord(*(thisTable.tfileHandle), attrs, data,rid);
		if (rc != 0){
			return rc;
		}

		//insert indices
		FormattedRecord thisTuple;
		readRecordFromRawData(data, attrs, thisTuple);
		char temp[PAGE_SIZE];
		for (unsigned i=0; i<attrs.size(); ++i){
			Attribute & thisAttr = attrs[i];
			string thisIndexName;
			getIndexName(tableName, thisAttr.name, thisIndexName);
			map<string, RMIndex>::iterator indexItor = thisTable.indices.find(thisIndexName);
			if (indexItor != thisTable.indices.end()){
				RMIndex & thisIndex = indexItor->second;
				MultiTypeValue & thisValue = thisTuple.values[i];
				if (thisAttr.type == TypeInt){
					memcpy(temp, &thisValue.intValue, sizeof(int));
				}else if (thisAttr.type == TypeReal){
					memcpy(temp, &thisValue.floatValue, sizeof(float));
				}else{
					unsigned charLength = thisValue.strValue.size();
					memcpy(temp, &charLength, sizeof(unsigned));
					memcpy(temp+sizeof(unsigned), thisValue.strValue.c_str(), charLength);
				}
				rc = IndexManager::instance()->insertEntry(thisIndex.ixFileHandle, thisAttr, temp, rid);
				if (rc != 0){
					return rc;
				}
			}
		}
		return 0;
	}
}

RC RelationManager::deleteTuples(const string &tableName)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;
		if (thisTable.status == Catalog::RegularTable){
			RC rc = RecordBasedFileManager::instance()->deleteRecords(
					*(thisTable.tfileHandle));
			if (rc != 0){
				return rc;
			}

			//delete all the entries in indices
			map<string, RMIndex>::iterator indexItor = thisTable.indices.begin();
			for (; indexItor != thisTable.indices.end(); ++indexItor){
				RMIndex & thisIndex = indexItor->second;
				IndexManager::instance()->closeFile(thisIndex.ixFileHandle);
				IndexManager::instance()->destroyFile(thisIndex.iname);
				IndexManager::instance()->createFile(thisIndex.iname, 1);
				IndexManager::instance()->openFile(thisIndex.iname, thisIndex.ixFileHandle);
			}

			return rc;
		}else {
			return -2;
		}
	}


}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
			tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;

		if (thisTable.status == Catalog::RegularTable) {
			vector<Attribute> attrs;
			getAttributes(tableName, attrs);

			//delete indices
			char temp[PAGE_SIZE];
			RC rc = RecordBasedFileManager::instance()->readRecord(*(thisTable.tfileHandle), attrs, rid, temp);
			if (rc != 0){
				return rc;
			}
			FormattedRecord thisTuple;
			readRecordFromRawData(temp, attrs, thisTuple);
			for (unsigned i=0; i<attrs.size(); ++i){
				Attribute & thisAttr = attrs[i];
				string thisIndexName;
				getIndexName(tableName, thisAttr.name, thisIndexName);
				map<string, RMIndex>::iterator indexItor = thisTable.indices.find(thisIndexName);
				if (indexItor != thisTable.indices.end()){
					RMIndex & thisIndex = indexItor->second;
					MultiTypeValue & thisValue = thisTuple.values[i];
					if (thisAttr.type == TypeInt){
						memcpy(temp, &thisValue.intValue, sizeof(int));
					}else if (thisAttr.type == TypeReal){
						memcpy(temp, &thisValue.floatValue, sizeof(float));
					}else{
						unsigned charLength = thisValue.strValue.size();
						memcpy(temp, &charLength, sizeof(unsigned));
						memcpy(temp+sizeof(unsigned), thisValue.strValue.c_str(), charLength);
					}
					rc = IndexManager::instance()->deleteEntry(thisIndex.ixFileHandle, thisAttr, temp, rid);
					if (rc != 0){
						return rc;
					}
				}
			}

			return RecordBasedFileManager::instance()->deleteRecord(
					*(thisTable.tfileHandle), attrs, rid);
		} else {
			return -2;
		}
	}
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
			tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;
		if (thisTable.status == Catalog::RegularTable) {
			vector<Attribute> attrs;
			getAttributes(tableName, attrs);

			//update indices
			char temp[PAGE_SIZE];
			RC rc = RecordBasedFileManager::instance()->readRecord(*(thisTable.tfileHandle), attrs, rid, temp);
			if (rc != 0){
				return rc;
			}
			FormattedRecord oldTuple;
			readRecordFromRawData(temp, attrs, oldTuple);
			FormattedRecord newTuple;
			readRecordFromRawData(data, attrs, newTuple);
			for (unsigned i=0; i<attrs.size(); ++i){
				Attribute & thisAttr = attrs[i];
				string thisIndexName;
				getIndexName(tableName, thisAttr.name, thisIndexName);
				map<string, RMIndex>::iterator indexItor = thisTable.indices.find(thisIndexName);
				if (indexItor != thisTable.indices.end()){
					RMIndex & thisIndex = indexItor->second;
					MultiTypeValue & oldValue = oldTuple.values[i];
					if (thisAttr.type == TypeInt){
						memcpy(temp, &oldValue.intValue, sizeof(int));
					}else if (thisAttr.type == TypeReal){
						memcpy(temp, &oldValue.floatValue, sizeof(float));
					}else{
						unsigned charLength = oldValue.strValue.size();
						memcpy(temp, &charLength, sizeof(unsigned));
						memcpy(temp+sizeof(unsigned), oldValue.strValue.c_str(), charLength);
					}
					//delete old index entry
					rc = IndexManager::instance()->deleteEntry(thisIndex.ixFileHandle, thisAttr, temp, rid);
					if (rc != 0){
						return rc;
					}
					//insert new index entry
					MultiTypeValue & newValue = newTuple.values[i];
					if (thisAttr.type == TypeInt){
						memcpy(temp, &newValue.intValue, sizeof(int));
					}else if (thisAttr.type == TypeReal){
						memcpy(temp, &newValue.floatValue, sizeof(float));
					}else{
						unsigned charLength = newValue.strValue.size();
						memcpy(temp, &charLength, sizeof(unsigned));
						memcpy(temp+sizeof(unsigned), newValue.strValue.c_str(), charLength);
					}
					rc = IndexManager::instance()->insertEntry(thisIndex.ixFileHandle, thisAttr, temp, rid);
					if (rc != 0){
						return rc;
					}
				}
			}

			return RecordBasedFileManager::instance()->updateRecord(
					*(thisTable.tfileHandle), attrs, data, rid);
		} else {
			return -2;
		}
	}
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
			tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;

		vector<Attribute> attrs;
		getAttributes(tableName, attrs);
		return RecordBasedFileManager::instance()->readRecord(
				*(thisTable.tfileHandle), attrs, rid, data);
	}
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
			tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;

		vector<Attribute> attrs;
		getAttributes(tableName, attrs);
		int index = -1;
		for (unsigned i=0; i<attrs.size(); ++i){
			if (attrs[i].name.compare(attributeName) == 0){
				index = i;
				break;
			}
		}
		if (index == -1){
			return -2;
		}
		char temp[PAGE_SIZE];
		RC result = RecordBasedFileManager::instance()->readRecord(
				*(thisTable.tfileHandle), attrs, rid, temp);
		if (result != 0){
			return result;
		}
		char * ptr = temp;
		for (int i=0; i<index; ++i){
			if (attrs[i].type == TypeInt || attrs[i].type == TypeReal){
				ptr += 4;
			}else{
				int length = *((int *) ptr);
				ptr += 4;
				ptr += length;
			}
		}
		if (attrs[index].type == TypeInt || attrs[index].type == TypeReal){
			memcpy(data, ptr, 4);
		}else{
			*((int *)data) = *((int *)ptr);
			int size = *((int*)ptr);
			ptr += 4;
			memcpy((char*)data + 4, ptr, size);
		}
		return 0;
	}
}

RC RelationManager::reorganizePage(const string &tableName, const unsigned pageNumber)
{
	map<string, RMTable>::iterator iter = catalog.tableMap.find(
			tableName);
	if (iter == catalog.tableMap.end()) {
		return -1;
	} else {
		RMTable & thisTable = iter->second;
		vector<Attribute> attrs;
		getAttributes(tableName, attrs);
		if (thisTable.status == Catalog::RegularTable) {
			return RecordBasedFileManager::instance()->reorganizePage(*(thisTable.tfileHandle), attrs, pageNumber);
		} else {
			return -2;
		}
	}
    return -1;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  
      const void *value,                    
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
	map<string, RMTable>::iterator itor = catalog.tableMap.find(tableName);
	if ( itor == catalog.tableMap.end()){
		return -1;
	}
	RMTable & thisTable = itor->second;
	vector<Attribute> attrs;
	getAttributes(tableName, attrs);

	rm_ScanIterator.tableName = tableName;

	string thisIndexName;
	getIndexName(tableName, conditionAttribute, thisIndexName);

	if (compOp == EQ_OP && thisTable.indices.find(thisIndexName) != thisTable.indices.end()){
		//Support index exact match
		//Exact Match, we use indexIterator to speed up
		rm_ScanIterator.exactMatch = true;
		rm_ScanIterator.attrs.clear();
		rm_ScanIterator.outputOrders.clear();
		rm_ScanIterator.attrs = attrs;
		for (unsigned i=0; i<attributeNames.size(); ++i){
			for (unsigned j=0; j<attrs.size(); ++j){
				if (attributeNames[i] == attrs[j].name){
					rm_ScanIterator.outputOrders.push_back(j);
					break;
				}
			}
		}
		return indexScan(tableName, conditionAttribute, value, value, true, true, rm_ScanIterator.indexIter);
	}else{
		//Regular Iterate
		rm_ScanIterator.exactMatch = false;
		return RecordBasedFileManager::instance()->scan(*(thisTable.tfileHandle), attrs, conditionAttribute, compOp, value, attributeNames,
					rm_ScanIterator.baseIter);
	}
}

/*
 * This method creates an index on a given attribute of a given table.
 * The number of primary pages should be 1 when you create an index.
 */
RC RelationManager::createIndex(const string &tableName, const string &attributeName)
{
	/*
	 * First check whether the table and attribute exist
	 */
	map<string, RMTable>::iterator catalogItor = catalog.tableMap.find(tableName);
	if (catalogItor == catalog.tableMap.end()){
		return RM_INDEX_CREATE_ERROR_TABLE_NOT_EXIST;
	}
	RMTable & thisTable = catalogItor->second;
	vector<RMColumn> & columnVector = thisTable.tcolumns;
	Attribute thisAttribute;
	bool attributeExist = false;
	for (vector<RMColumn>::iterator itor=columnVector.begin(); itor!=columnVector.end(); ++itor){
		string thisName = "";
		if (attributeName.find(".") != string::npos){
			thisName.append(tableName);
			thisName.append(".");
			thisName.append(itor->cname);
		}else{
			thisName.append(itor->cname);
		}
		if (thisName.compare(attributeName) == 0){
			attributeExist = true;
			thisAttribute.type = itor->ctype;
			thisAttribute.length = itor->clength;
			thisAttribute.name = itor->cname;
			break;
		}
	}
	if (!attributeExist){
		return RM_INDEX_CREATE_ERROR_ATTRIBUTE_NOT_EXIST;
	}

	//check whether exist in the catalog
	string indexFilename;
	getIndexName(tableName, attributeName, indexFilename);
	map<string, RMIndex>::iterator indexItor = thisTable.indices.find(indexFilename);
	if (indexItor != thisTable.indices.end()){
		return RM_INDEX_CREATE_ERROR_INDEX_ALREADY_EXIST;
	}

	//delete useless files
	IndexManager::instance()->destroyFile(indexFilename);
	RC rc = IndexManager::instance()->createFile(indexFilename, 1);
	if (rc == 0){
		//insert to catalog
		RMIndex & thisIndex = thisTable.indices[indexFilename];
		thisIndex.iname = indexFilename;
		rc = IndexManager::instance()->openFile(indexFilename, thisIndex.ixFileHandle);
		if (rc != 0){
			return rc;
		}
		//create the indices for the existing tuples
		RM_ScanIterator scanItor;
		vector<string> projectedAttrNames;
		projectedAttrNames.push_back(attributeName);
		char buffer[PAGE_SIZE];
		RID thisRID;
		rc = scan(tableName, "", NO_OP, NULL, projectedAttrNames, scanItor);
	    if (rc != 0){
	    	return rc;
	    }
	    while(scanItor.getNextTuple(thisRID, buffer) != RM_EOF){
	    	rc = IndexManager::instance()->insertEntry(thisIndex.ixFileHandle, thisAttribute, buffer, thisRID);
	    	if (rc != 0){
	    		return rc;
	    	}
	    }
	    return scanItor.close();
	}else{
		return rc;
	}
}

/*
 * This method destroys an index on a given attribute of a given table.
 */
RC RelationManager::destroyIndex(const string &tableName, const string &attributeName)
{
	/*
	 * First check whether the table and attribute exist
	 */
	map<string, RMTable>::iterator catalogItor = catalog.tableMap.find(tableName);
	if (catalogItor== catalog.tableMap.end()){
		return RM_INDEX_DESTORY_ERROR_TABLE_NOT_EXIST;
	}
	RMTable & thisTable = catalogItor->second;
	vector<RMColumn> & columnVector = thisTable.tcolumns;
	bool attributeExist = false;
	for (vector<RMColumn>::iterator itor=columnVector.begin(); itor!=columnVector.end(); ++itor){
		if (itor->cname.compare(attributeName) == 0){
			attributeExist = true;
			break;
		}
	}
	if (!attributeExist){
		return RM_INDEX_DESTORY_ERROR_ATTRIBUTE_NOT_EXIST;
	}

	//check whether exist in the catalog
	string indexFilename;
	getIndexName(tableName, attributeName, indexFilename);
	map<string, RMIndex>::iterator indexItor = thisTable.indices.find(indexFilename);
	if (indexItor == thisTable.indices.end()){
		return RM_INDEX_DESTORY_ERROR_INDEX_NOT_EXIST;
	}

	RC rc = IndexManager::instance()->destroyFile(indexFilename);
	if (rc == 0){
		//delete from catalog
		thisTable.indices.erase(indexItor);
		return 0;
	}else{
		return rc;
	}
}

// indexScan returns an iterator to allow the caller to go through qualified entries in index
RC RelationManager::indexScan(const string &tableName,
      const string &attributeName,
      const void *lowKey,
      const void *highKey,
      bool lowKeyInclusive,
      bool highKeyInclusive,
      RM_IndexScanIterator &rm_IndexScanIterator)
{
	/*
	 * First check whether the table and attribute exist
	 */
	map<string, RMTable>::iterator catalogItor = catalog.tableMap.find(tableName);
	if (catalogItor == catalog.tableMap.end()){
		return RM_INDEX_SCAN_ERROR_TABLE_NOT_EXIST;
	}
	RMTable & thisTable = catalogItor->second;
	vector<RMColumn> & columnVector = thisTable.tcolumns;
	bool attributeExist = false;
	Attribute thisAttribute;
	for (vector<RMColumn>::iterator itor=columnVector.begin(); itor!=columnVector.end(); ++itor){
		string thisName = "";
		if (attributeName.find(".") != string::npos){
			thisName.append(tableName);
			thisName.append(".");
			thisName.append(itor->cname);
		}else{
			thisName.append(itor->cname);
		}
		if (thisName.compare(attributeName) == 0){
			attributeExist = true;
			thisAttribute.length = itor->clength;
			thisAttribute.name = itor->cname;
			thisAttribute.type = itor->ctype;
			break;
		}
	}
	if (!attributeExist){
		return RM_INDEX_SCAN_ERROR_ATTRIBUTE_NOT_EXIST;
	}

	//check from the catalog
	string indexFilename;
	getIndexName(tableName, thisAttribute.name, indexFilename);
	map<string, RMIndex>::iterator indexItor = thisTable.indices.find(indexFilename);
	if (indexItor == thisTable.indices.end()){
		return RM_INDEX_SCAN_ERROR_INDEX_NOT_EXIST;
	}
	rm_IndexScanIterator.pIndex = &(indexItor->second);
	return IndexManager::instance()->scan(rm_IndexScanIterator.pIndex->ixFileHandle,
			thisAttribute, lowKey, highKey,
			lowKeyInclusive, highKeyInclusive, rm_IndexScanIterator.ixItor);
}

// Extra credit
RC RelationManager::dropAttribute(const string &tableName, const string &attributeName)
{
	/**
	 * This method adds a new attribute (attr) to a table called tableName.
	 * Note: This operation will update the catalogs but should not involve touching the data itself.
	 */

    return -1;
}

// Extra credit
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr)
{
	/**
	 *	This method drops an attribute called attributeName from a table called tableName.
	 *	Note: This operation will update the catalogs but should not involve touching the data itself.
	 *	HINT: The above two methods will affect the way the operations that access the fields of
	 *	the records work when accessing a tuple that was created before such a schema change.
	 */
    return -1;
}

// Extra credit
RC RelationManager::reorganizeTable(const string &tableName)
{
	/**
	 * This method reorganizes a table that causes reorganization of the tuples such that the tuples are collected towards
	 * the beginning of the file. Also, tuple redirection is eliminated.
	 * Note: In this case, and only this case, you are allowed to change the records rids
	 * since they might need to move to other pages.
	 */
    return -1;
}
